#!/usr/bin/perl -w
##
###
### Author: eqsvimp    Eric Yin Z
### $Id: itkreport 9360 2014-11-04 05:06:20Z eqsvimp $
##
### GSC CHINA Tool & Data Centre Service
###
#

use FindBin qw($Bin);
use lib "../../../lib/";
use lib "$Bin/../lib/";
use Itk::Utils::LoginSession qw(eca_login node_login);



my @date = ("2016-07-15","2016-07-16","2016-07-19","2016-07-20");
@date = split "@" , $ARGV[0];

my @shell = `cat /home/eqsvimp/msc_bc/msc_bc.sh`;
my $count = -1;

if(defined $date[0] and defined $date[1] and defined $date[2] and defined $date[3])
{
    foreach my $item (@shell)
    {
        $count++;
        if(defined $item and $item =~ /datefrom1/) {$shell[$count] = "datefrom1=".$date[0]."\n";next}
        if(defined $item and $item =~ /datetill1/) {$shell[$count] = "datetill1=".$date[1]."\n";next}
        if(defined $item and $item =~ /datefrom2/) {$shell[$count] = "datefrom2=".$date[2]."\n";next}
        if(defined $item and $item =~ /datetill2/) {$shell[$count] = "datetill2=".$date[3]."\n";last;}
        
    }
}

open(FH,">/home/eqsvimp/msc_bc/msc_bc.sh");
foreach my $item (@shell) { print FH $item; }




etl_oss_files("/home/eqsvimp/msc_bc/","/home/etool/echglig/","msc_bc.sh");
my $session = oss_login();
$session->commandCommit("cd /home/etool/echglig/; /home/etool/echglig/msc_bc.sh;zip -r msc_bc.zip *.txt");
oss_etl_files("/home/eqsvimp/msc_bc/","/home/etool/echglig/","msc_bc.zip");
`chmod 775 -R /home/eqsvimp/msc_bc`;
`cd /home/eqsvimp/msc_bc/ ; unzip -o /home/eqsvimp/msc_bc/msc_bc.zip; rm -f /home/eqsvimp/msc_bc/msc_bc.zip`;


sub etl_oss_files
{
    my($src,$dist,$filename) = @_;
    my $session = new Itk::Utils::LoginSession();
    $tmp = "/var/tmp/";        
    $session->{command} = "scp -P 30023  $src$filename root\@localhost:\/$tmp;echo finish";
    $session->{password} = "rootstm";
    $session->{cliPrompt} = "finish";
    $session->ecaLogin();
    my %ecainfo = (  'user'      => "root",
                     'password'  => "rootstm",
                     'port'      => 30023,
                     'cliPrompt' => "eca.*#",
                     'host'      => "localhost"
                 
    );
    $session = new Itk::Utils::LoginSession();
    $session->setField(%ecainfo);
    $session->ecaLogin();
    #######get OSS password
    $session->{cliPrompt}= "eca.*#";
    $read = $session->commandCommit('ecaadmin config -get server.hostTable -nwid stmsg | grep -A 10 oss_wran');
    #eca ~ \$ \e[0mecaadmin config -get server.hostTable -nwid stmsg | grep -A 10 oss_w \cMran | grep password\cM\cJ\cI       
    # password => 'CNYfeb\@16',\cM\cJ\e[1;31meca\e[33m
    my ($username,$password,$hostaddress);
    if($read =~ /.*username.*\>\s'(.*)'/) { $username = $1;}  #'CNYfeb@16',
    if($read =~ /.*password.*\>\s'(.*)'/) { $password = $1;}
    if($read =~ /.*hostaddress.*\>\s'(.*)'/) { $hostaddress = $1;}
    $session->{cliPrompt} = "Password";
    $session->commandCommit("scp $tmp$filename $username\@$hostaddress:\/$dist");
    $session->{cliPrompt} = "eca.*#";
    $session->commandCommit("$password");
    $session->commandCommit("rm -f $tmp$filename");
    


    return $session;
}

sub oss_etl_files
{
    my($src,$dist,$filename) = @_;
    $tmp = "/var/tmp/";
    my $session = new Itk::Utils::LoginSession();
    my %ecainfo = (  'user'      => "root",
                     'password'  => "rootstm",
                     'port'      => 30023,
                     'cliPrompt' => "eca.*#",
                     'host'      => "localhost",
 		     'echo'      => "enabled",
			
                 
    );
    print "data handling process.... waiting for a while";
    $session = new Itk::Utils::LoginSession();
    $session->setField(%ecainfo);
    $session->ecaLogin();
    #######get OSS password
    $session->{cliPrompt}= "eca.*#";
    $read = $session->commandCommit('ecaadmin config -get server.hostTable -nwid stmsg | grep -A 10 oss_wran');
    #eca ~ \$ \e[0mecaadmin config -get server.hostTable -nwid stmsg | grep -A 10 oss_w \cMran | grep password\cM\cJ\cI       
    # password => 'CNYfeb\@16',\cM\cJ\e[1;31meca\e[33m
    my ($username,$password,$hostaddress);
    if($read =~ /.*username.*\>\s'(.*)'/) { $username = $1;}  #'CNYfeb@16',
    if($read =~ /.*password.*\>\s'(.*)'/) { $password = $1;}
    if($read =~ /.*hostaddress.*\>\s'(.*)'/) { $hostaddress = $1;}
    $session->{cliPrompt} = "Password";
    $session->commandCommit("scp  $username\@$hostaddress:\/$dist$filename $tmp");
    $session->{cliPrompt} = "\@eca";
    $session->commandCommit("$password");
    
    $session2 = new Itk::Utils::LoginSession();
    $session2->setField(%ecainfo);            
    $session2->{command} = "scp -P 30023  root\@localhost:\/$tmp$filename $src;echo finish";
    $session2->{password} = "rootstm";
    $session2->{cliPrompt} = "finish";
     $session->{echo} = "enabled"; 
    $session2->ecaLogin();
    $session->commandCommit("rm -f $tmp$filename");
    return $session;
}	
 

sub oss_login
{
    my %ecainfo = (  'user'      => "root",
                     'password'  => "rootstm",
                     'port'      => 30023,
                     'cliPrompt' => "eca.*#",
                     'host'      => "localhost"
                 
    );
    $session = new Itk::Utils::LoginSession();
    $session->setField(%ecainfo);
    $session->ecaLogin();
    #######get OSS password
    $session->{cliPrompt}= "eca.*#";
    $read = $session->commandCommit('ecaadmin config -get server.hostTable -nwid stmsg | grep -A 10 oss_wran');
    #eca ~ \$ \e[0mecaadmin config -get server.hostTable -nwid stmsg | grep -A 10 oss_w \cMran | grep password\cM\cJ\cI       
    # password => 'CNYfeb\@16',\cM\cJ\e[1;31meca\e[33m
    my ($username,$password,$hostaddress);
    if($read =~ /.*username.*\>\s'(.*)'/) { $username = $1;}  #'CNYfeb@16',
    if($read =~ /.*password.*\>\s'(.*)'/) { $password = $1;}
    if($read =~ /.*hostaddress.*\>\s'(.*)'/) { $hostaddress = $1;}

    ########Login OSS
    my %ossinfo = (  
                 'user'      => "$username",
                 'password'  => "$password",
                 'cliPrompt' => "ocsuas3>",
                 'timeout' => 1000,
                 'host'      => "$hostaddress",
                 'interact'  => "disabled"
   );

    $session->setField(%ossinfo);
    $session->nodeLogin();
    return $session;

}
