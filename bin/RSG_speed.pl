#!/usr/bin/perl
##
###
### Author: eqsvimp    Eric Yin Z
### $Id: itkreport 9360 2014-11-04 05:06:20Z eqsvimp $
##
### GSC CHINA Tool & Data Centre Service
### 
#

use FindBin qw($Bin);
use lib "../../../lib/";
use lib "$Bin/../lib/";
use Itk::Utils::LoginSession qw(eca_login node_login);
use warnings;
use Expect;

RSG();

sync_html();








sub RSG
{

($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdat) = localtime;

      $mon+=1; $year+=1900;
      if($mon <= 9) {$mon="0".$mon;}
      if($mday <= 9) {$mday="0".$mday;}
      if($hour <= 9) {$hour="0".$hour;}
      if($min <= 9) {$min="0".$min;}
      if($sec <= 9) {$sec="0".$sec;}


my $time = "$year-$mon-$mday $hour:$min:$sec";

$command = 'scp -P 20023  root@localhost:///ueh_mon/old_ueh/Rnc18/2015-12-22/2015-12-22_0015_Rnc18_AllMod_204427.log.gz  ./';
my %selfinfo = ( 'user'      => "root",
	         'timeout'   => 10000,
                 'password'  => "ensalty",
                 'cliPrompt' => "#",
                 'host'      => "127.0.0.1",
	         'command'   => "ssh root\@127.0.0.1"

);


my %rsginfo = (  'user'      => "root",
	         'timeout'   => 10000,
                 'password'  => "rootstm",
                 'cliPrompt' => "#",
	         'host'      => "127.0.0.1",
                 'command'   => $command
);



my $session = new Itk::Utils::LoginSession();

$session->setField(%selfinfo);
$session->ecaLogin();
$session->setField(%rsginfo);
$session->nodeLogin();

my $output = $session->{read};
if( $output =~ /.*\s(\d+\.\d+)KB.*/)
{
   
open FH,">> /var/opt/ericsson/itk/log/rsg/rsg.log";
print FH $time.",".$1."\n";
close FH;
print $time.",".$1." \n";
}
else
{
open FH,">> /var/opt/ericsson/itk/log/rsg/rsg.log";
print FH $time.",0\n";
close FH;

}

;

}


#$read = $session->commandCommit($command);
#print $read;
#print "here";



sub sync_html

{

my $t1 = 100;
my $rsg_prompt ="#";
my $command = "scp /var/opt/ericsson/itk/log/rsg/rsg.log root\@146.11.90.14://var/www/html/demos/sampledata/";
my $ssh = Expect->spawn($command);
my $rsg_pwd="Ensalty2012";

$ssh->expect($t1,

[ qr/Are you sure you want to continue connecting \(yes\/no\)\?/, sub {
                my $self=shift;
                $self->send("yes\n");
                print STDERR "EGW added to known hosts\n" unless $quiet;
                exp_continue;
                } ],

        [ qr/word:|CODE:/, sub {
                my $self=shift;
                $self->send("$rsg_pwd\n");
                exp_continue;
                } ],


        -re, $rsg_prompt
) or die "end";


}







sub getParentInfo
{
  my ($read,$fromKey,$targetKey) = @_;
  my @rows =  split '\cM\cJ', $read;
  my $count = 0;
  foreach my $row (@rows)
   {
  if($row eq $fromKey)
        {
          last;
        }
    $count++;
   }

  for( 0 .. $count) 
  {
    if($rows[$count-$_] =~ /$targetKey/)
    {
       return $rows[$count-$_];
    }
  }  
}



sub captureFromTo
{
    my ($read,$from,$to) = @_;
    my @result;
    my $count = 0;
    my @rows =  split '\cM\cJ', $read;
    my ($start,$end) = (0,$#rows);
    foreach my $row (@rows)
   {
	if($row =~ /$from/)
        {
          $start = $count;
        }
        if($row =~ /$to/)
        {
          $end = $count;
          last;
        }
        $count++;
   }    
    for ($start..$end){ push @result, $rows[$_];} 
    return @result;
}
