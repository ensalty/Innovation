package Check;

use HTML::TokeParser;
use Time::Local;
use Expect;
use MIME::Lite;
use Time::gmtime;
use FindBin qw($Bin);
use lib "../../../lib/";
use lib "$Bin/../lib/";
use Itk::Utils::SmsAlertSession;



($eniq_status,$etlc_load,$data_load,$connection_status)=("OK","OK","OK","OK");
my $file = "/var/www/html/ENIQ/ShowLoadStatus.html";
my $connection_status = CheckConnectionStatus($file);
my $file = "/var/www/html/ENIQ/LoaderStatusServlet.html";
my $eniq_status = CheckEniqStatus($file);
if($connection_status ne "OK")
{
SendAlert($connection_status,$eniq_status,$etlc_load,$data_load);
}





sub SendAlert
{
my ($conenction_status,$eniq_status,$etlc_load,$data_load) = @_;
my $localtime = localtime;
my $message = "Testing ENIQ SERVER Monitoring 
                <CONNECT STATUS>: $connection_status
                <ENIQ  STATUS>: $eniq_status
                <ETLC LOADING>: $etlc_load
                <DATA LOADING>: $data_load
                <TIME>: $localtime
                <Author>: GSC Tool & Data Center Developer Eric Yin Z ";
my %smsinfo = (  

                 'from'        => 'PDLGSCCPRO@pdl.internal.ericsson.com',
                 'smtp'        => 'smtp.eapac.ericsson.se',
#                'contactlist' => '+6583073925@sms.ericsson.com;+6597806544@sms.ericsson.com;+6597207335@sms.ericsson.com',
                 'contactlist' => '+6567045751@sms.ericsson.com;eric.z.yin@ericsson.com',
                 'subject'     => $message,
                 'message'     => "$message",
);

my $sms = new Itk::Utils::SmsAlertSession();
$sms->setField(%smsinfo);
$sms->sendWorker();

}

sub CheckEniqStatus
{
   my $file = shift;
   my @res = `cat $file | grep -A 2 'font color='`;
   my $eniq_status = "";
   my $count = 0;
   my $flag = 0;
   foreach my $item (@res)
   {
     if($item =~/.*font color="(\w+)".*/)
     {
       if($1 ne "green" and $1 ne "gray"){
#        \cI\cI\cI\cI\cI\cI\cI\cI\cI<img src=\"../img/green_bulp.gif\" width=\"10px\" height=\"10px\" border=\"0\" alt=\"On\">&nbsp;dwhdb\cJ
	 if($res[$count+1]=~ /.*;(\w+).*/)
         { 
           $flag = 1;
           $eniq_status = $eniq_status."$1 is NOT OK"; 
         }       

       }
       
     }
    $count++;
   }

   if($flag == 0){return "OK";}
   if($flag == 1){return $eniq_status;}
   



}

sub CheckConnectionStatus
{
    my $file = shift;
    my $parser = HTML::TokeParser->new($file)
    or die "Can't open $file: $!\n";
    my $connection_status = "OK";

    while (my $token = $parser->get_token( )) 
    {

        my $type = $token->[0];
        if ( $type eq 'T' ) 
        {
            if ( $token->[1] =~/.*loadstatus.*is :\s(\d+)-(.*)-(.*)\s(.*):(.*):(.*)/ ) {
            #2016-02-12 18:05:58
            my($sec, $min, $hours, $mday, $mon, $year)=($6,$5,$4,$3,$2,$1);
            $sec+=0; $min+=0; $hours+=0; $mday+=0; $mon-=1; $year-=1900;
            my $pagetime = timegm($sec, $min, $hours, $mday, $mon, $year);
            $pagetime-=8*60*60;
                if((time-$pagetime)>3600)
                {
                    print "ENIQ cannot be logged since $1-$2-$3 $4:$5:$6\n";
                    $connection_status = "ENIQ cannot be logged since $1-$2-$3 $4:$5:$6";
                }
            }
        }
    }
  return $connection_status;
}


