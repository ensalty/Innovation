#!/bin/bash

logrootdir=<ENTER_LOG_DIR_PATH>

USAGE=" Usuage: `basename $0` rnclist
                Where rnclist is a list of RNC names seperated by spaces
                i.e: `basename $0` rnc1 rnc2 rnc3"

#----------------------------------------
# Test we have received some RNC names
#----------------------------------------
if [ $# -eq 0 ]; then
   echo $"${USAGE}" >&2
   exit 1
fi

rnclist="$1"
echo "Attempting to start monitors towards the following RNCs '$rnclist'"

for rnc in $rnclist; do
        echo -n "attaching to monitor on $rnc: "
        if [ ! -d $logrootdir/$rnc ]; then
                echo "Creating directory $logrootdir/$rnc"
                mkdir -p $logrootdir/$rnc
        fi

        #-------------------------------------------------------------------------------------------------------------------------
        # Activate UEH traces on all modules on the target RNC, and get the command to start the monitor on the local workstation
        #-------------------------------------------------------------------------------------------------------------------------
        cd $logrootdir/$rnc
        moncmd=`<MOSHELL_PATH>/moshell $rnc 'lh mod te e all UEH_EXCEPTION; mon modact' | egrep  '\\$moncommand' | cut -d '=' -f 2`
        if  echo $moncmd | grep -q monitor ; then
                nohup $moncmd | <CONCAT SCRIPT PATH> concat_segmented_rlib_traces.pl | <LOGSPLIT_PATH>/logsplit -c "cp <ITK EXCEPTIONS DIR PATH>" -date -gzip 10000 ${rnc}_AllMod.log &
                echo "Successfully started logging Ueh Exceptions for RNC $rnc"
        else
                echo "Failed starting monitor for RNC $rnc . You may need to log onto the node with moshell and disconnect any existing monitors (tm -disconnect)"
        fi
done

