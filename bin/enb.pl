#!/usr/bin/perl
##
###
### Author: eqsvimp    Eric Yin Z
### $Id: itkreport 9360 2014-11-04 05:06:20Z eqsvimp $
##
### GSC CHINA Tool & Data Centre Service
### 
#
use FindBin qw($Bin);
use lib "../../../lib/";
use lib "$Bin/../lib/";
use Itk::Utils::LoginSession qw(eca_login node_login);
use Parallel::ForkManager;
use Itk::NitsPackServer;
use DBI;

use warnings;

#global variable
our $srv;
our %info;
my $dir = "/home/eqsvimp/healthcheck/report/spool/";
$maxProc = 2;
# multiple nodes
@keynodelist = ("enb_730041","enb_730181","enb_730071","enb_730241","enb_731213","enb_731871");

	


$configFile ||= "$Bin/../etc/itk.conf";
$srv=new Itk::NitsPackServer(config => $configFile);


use constant TIMEOUT => {
        'PMXML'         =>      {MAX_PROC_TIME  => 600,  TIMEOUT_POLL_INTERVAL=>3},
        'GPEH'          =>      {MAX_PROC_TIME  => 1200,TIMEOUT_POLL_INTERVAL=>60},
        'OSS'           =>      {MAX_PROC_TIME  => 2400,TIMEOUT_POLL_INTERVAL=>60},
};


my %ecainfo = (  'user'      => "eca",  
                 'password'  => "ecastm",  
                 'port'      => 30023,
                 'cliPrompt' => "\@",
    		         'host'      => "localhost",
  	       			 
);

my %ossinfo = (  'user'      => "",  
                 'password'  => "",  
                 'cliPrompt' => "\@ocsuas3>",
                 'timeout'   => 1000,                 
		             'host'      => ""
  
);

#@keynodelist = getNodelist();




sub getNodelist
{
    my %node;
    my $session = new Itk::Utils::LoginSession();
	  $session->setField(%ecainfo);
	  $session->ecaLogin();
	#######get OSS password
	  $session->{cliPrompt}= "\@eca";
	  $read = $session->commandCommit('ecaadmin config -get server.hostTable -nwid stmsg | grep -A 10 oss_lte');
#eca ~ \$ \e[0mecaadmin config -get server.hostTable -nwid stmsg | grep -A 10 oss_w \cMran | grep password\cM\cJ\cI       
# password => 'CNYfeb\@16',\cM\cJ\e[1;31meca\e[33m
    %ossinfo = getOSSinfo($read);
    $ossinfo{cliPrompt} = "\@.*>";
  	$session->setField(%ossinfo);
    $session->nodeLogin();
	  $read = $session->commandCommit('/opt/ericsson/ddc/util/bin/listme | grep "SubNetwork=ENB"');
    @res = captureFromTo($read,"HEAD%^&","TAIL%^&");
    #SubNetwork=ONRM_ROOT_MO_R,SubNetwork=ENB_PIONEER,MeContext=ENB_959313_SDV_Logistics@10.245.72.113@vF.1.107@4@OFF@2@3@ERBS_NODE_MODEL@1465366766527@1465189200831
    map{ $_ =~ s/.*MeContext=(ENB\_\d+)\_.*/$1/g } grep {/MeContext/} @res; 
    pop @res; shift @res; #remove the first and the last element


    $session->{cliPrompt}= "\@eca";
	  $session->commandCommit('exit');
	  $session->{cliPrompt}= "\@";
	  $session->commandCommit('exit');
	  $session->close();
	  return @res;

}


########Mutiple processes start here
	task_start($nodename,$dir,%ossinfo);
#	$pm->finish;



sub task_start
{
	my ($nodename,$dir,%ossinfo) = @_;



my $session = new Itk::Utils::LoginSession();
$session->setField(%ecainfo);
$session->ecaLogin();

#######get OSS password
$session->{cliPrompt}= "\@eca";
$read = $session->commandCommit('ecaadmin config -get server.hostTable -nwid stmsg | grep -A 10 oss_wran');
#eca ~ \$ \e[0mecaadmin config -get server.hostTable -nwid stmsg | grep -A 10 oss_w \cMran | grep password\cM\cJ\cI
# password => 'CNYfeb\@16',\cM\cJ\e[1;31meca\e[33m
my ($username,$password,$hostaddress);

if($read =~ /.*username.*\>\s'(.*)'/) { $username = $1;}  #'CNYfeb@16',
if($read =~ /.*password.*\>\s'(.*)'/) { $password = $1;}
if($read =~ /.*hostaddress.*\>\s'(.*)'/) { $hostaddress = $1;}


########Login OSS
%ossinfo = (  'user'      => "$username",
                 'password'  => "$password",
                 'cliPrompt' => "\@ocsuas3>",
                 'timeout' => 1000,
                         'host'      => "$hostaddress"

);

$session->setField(%ossinfo);
$session->nodeLogin();















#######login the node
	$command = "lga -m 7d";
  $session->{cliPrompt}= ">";
	$session->commandCommit("amos $nodename");
	$session->{cliPrompt}= "Password";
	$session->commandCommit($command);
	$session->{cliPrompt}= ">";
	$read = $session->commandCommit("rbs\r");
	

##########command 
	$description = "Alarm & Event History";
	@res = captureFromTo($read,"HEAD","TAIL");
	my($critical,$major,$minor,$warnings) = (scalar(grep {/.*\s+C\s+.*/} @res),scalar(grep {/.*\s+M\s+.*/} @res),scalar(grep {/.*\s+m\s+.*/} @res),scalar(grep {/.*\s+w\s+.*/} @res));
	#write $info{$nodename}{comments} and $info{$nodename}{status}
	$info{$nodename}{$description}{comments} = "Critical:- $critical,Major:- $major,Minor:- $minor,Warning:- $warnings";
	if($critical >= 0){$info{$nodename}{$description}{status} = "Critical";}
	elsif($major >= 0){$info{$nodename}{$description}{status} = "Major";}
	elsif($minor >= 0){$info{$nodename}{$description}{status} = "Minor";}
	elsif($warnings >= 0){$info{$nodename}{$description}{status} = "Warning";}
#push @tmp , grep {/.*\s+M\s+.*w/} @res;
	writeCSV($dir,$nodename,$description,%info);


##########command 







##########command 
















####
###exit session####
	close FH;
	$session->{cliPrompt} = $ossinfo{cliPrompt};
	$session->commandCommit("exit");
	$session->{cliPrompt} = $ecainfo{cliPrompt};
	$session->{timeout}= 1;
	$session->commandCommit("exit");
	$session->{cliPrompt} = "localhost";
	$session->commandCommit("exit");
  $session->close();
	$srv->logInfo("$nodename => $command => comments :".$info{$nodename}{$description}{comments});
	$srv->logInfo("$nodename collection finished "); 


}





















sub writeCSV
{
	my ($dir,$nodename,$description,%info) = @_;
	$srv->logInfo("write file >> $dir$nodename"); 
	open(FH, ">>$dir$nodename");
	print FH "$nodename,$description,$info{$nodename}{$description}{status},$info{$nodename}{$description}{comments}\n";

}



sub getOSSinfo
{
    my $read = shift;
    my ($username,$password,$hostaddress,%ossinfo);
    if($read =~ /.*username.*\>\s'(.*)'/) { $username = $1;}  #'CNYfeb@16',
	if($read =~ /.*password.*\>\s'(.*)'/) { $password = $1;}
	if($read =~ /.*hostaddress.*\>\s'(.*)'/) { $hostaddress = $1;}
	($ossinfo{user},$ossinfo{password},$ossinfo{host}) = ($username,$password,$hostaddress);
    return %ossinfo;
}





sub captureFromTo
{
    my ($read,$from,$to) = @_;
    my @result;
    my $count = 0;
    my @rows =  split '\cM\cJ', $read;
    my ($start,$end) = (0,$#rows);
    foreach my $row (@rows)
   {
  if($row =~ /$from/)
        {
          $start = $count;
          $from = "&$^&";
          next;
        }
        if($row =~ /$to/)
        {
          $end = $count;
          last;
        }
        $count++;
   }    
    for ($start..$end){ push @result, $rows[$_];} 
    return @result;
}


