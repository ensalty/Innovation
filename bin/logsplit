#!/usr/bin/perl
#------------------------------------------------------------------------------
#
# logsplit : Split STDIN into files based on time or size
#
# � Ericsson AB 2006 - All Rights Reserved
#
# No part of this material may be reproduced in any form without the written
# permission of the copyright owner. The contents are subject to revision without
# notice due to continued progress in methodology, design and manufacturing.
# Ericsson shall have no liability for any error or damage of any kind resulting
# from the use of these documents.
#
# Ericsson is the trademark or registered trademark of Telefonaktiebolaget LM
# Ericsson. All other trademarks mentioned herein are the property of their
# respective owners.
#------------------------------------------------------------------------------
# From original logsplit by Dave Markham
# $Id: logsplit 5332 2011-10-11 05:05:40Z eharmic $

use strict;

my $version = join '',(
	'$Date: 2011-10-11 16:05:40 +1100 (Tue, 11 Oct 2011) $'=~/\$Date: (\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} [+\d]+)/,' ', 
	'$URL$'=~/\$URL: .+\/itk\/(trunk|branches\/[^\/]+|tags\/[^\/]+)/,'@r',
	'$Revision: 5332 $'=~/\$Revision: (\d+)/);

#Prints script usage format and exits
sub usageAndDie {
  if (@_) {print STDERR "@_\n";}  #print extra argument passed to routine, if any
  else {print STDERR "Splits STDIN into files of specified size\n";}  #otherwise give description 
  print STDERR "    Usage: logsplit (-alt|-gzip) [-stop <string> <numlines>] [-f] [-echo] [-t <time>] <size> <output_file_name>\n";
  print STDERR " Switches: <size> = approx size of each log file in kbytes ('0' disables)\n";
  print STDERR "             -alt = alternate between two log files (named *_A/B)\n";
  print STDERR "            -gzip = increment logfiles, compressing after file is full\n";
  print STDERR "               -f = force overwriting of existing files\n";
  print STDERR "            -echo = echoes STDIN to STDOUT\n";
  print STDERR "            -stop = stop logging <numlines> after finding <string> in output\n";
  print STDERR "            -date = Date/Time prepended to filename\n";
  print STDERR "               -c = additional command to be called when target file has been closed and compressed\n";
  print STDERR "                    The macro \%file\% will be replaced with the actual file being processed\n";
  print STDERR "                    example: -c \"ecaimport -nwid xxxx -type telog -jobopts 'noderegex=_(\w{4})_AllMod' \%file\%\"\n";
  print STDERR "               -t = File rotation timeout in minutes, files will be rotated after this time even if max size not reached\n";
  
  print STDERR "\n";
  exit;
}

my ($ALT, $INC) = (1, 2);  #file splitting methods

sub nextFilename($$$$) {
	my ($template, $method, $filenum, $usedate) = @_;
	my ($base, $extension, $nextname);
	my $ts="";
	if ($template =~ /(.+)(\..+)/) {
		($base, $extension) = ($1, $2);
	}  else {
		#if template has an extension
		($base, $extension) = ($template, '');
	}
	if ($method == $ALT) {
		$nextname = $base.($filenum % 2? '_A' : '_B').$extension;
	} elsif ($method == $INC) {
		if ($usedate) {
			my ($ss,$mm,$hh,$dd,$mo,$yy) = localtime(time);
			$ts = sprintf "%04d-%02d-%02d_%02d%02d_",($yy+1900,$mo+1,$dd,$hh,$mm);
		}
		$nextname = $ts.$base.'_'.$filenum.$extension;
  	} else {
		$nextname = $base.$extension;
	}  #default
	return $nextname;
}

sub next_timeout {
	my ($timeout)=@_;
	return int(time/$timeout)*$timeout+$timeout;
}
  
my %options;
#read optional switches in any order
while (@ARGV > 0) { #while there are arguments left
  if ($ARGV[0] !~ /^-/) {last}  #stop looking for switches if argument doesn't start with '-'
  $_ = shift(@ARGV);
  tr/A-Z/a-z/;  #convert to lower case
  if ($_ eq '-v')               {&usageAndDie('Version '.$version);}  #report version info and stop
  if ($_ eq '-h' || $_ eq '-?') {&usageAndDie()}  #give help and stop
  if ($_ eq '-stop')            {$options{stop} = shift(@ARGV); $options{stoplines} = shift(@ARGV); next;}
  if ($_ eq '-alt')             {$options{alt} = 1; next;}
  if ($_ eq '-echo')            {$options{echo} = 1; next;}
  if ($_ eq '-gzip')            {$options{gzip} = 1; $options{inc} = 1;next;}
  if ($_ eq '-date')            {$options{date} = 1; next; }
  if ($_ eq '-f')               {$options{overwrite} = 1; next;}
  if ($_ eq '-c')               {$options{command} = shift(@ARGV); $options{command} =~ s/\s*-c\s*(\.*)/$1/; next;}
  if ($_ eq '-t')		{$options{timeout} = shift @ARGV; next; }
  # been through all valid switches, reject anything else
  &usageAndDie("ERROR: Unknown switch: $_\n");
}

#TEST: 

if (@ARGV==0) {&usageAndDie();}
if (@ARGV!=2) {&usageAndDie("ERROR: Incorrect number of arguments");};
my $size = shift(@ARGV);
my $template = shift(@ARGV);

my $currbytes = 0;
my $filenum = 1;
if (defined $options{command} and $options{command} !~ /%file%/) {
	# Assume old school -c option
	warn "Deprecated usage of -c option: this option should contain the token '\%file%' which will be replaced with the log file name\n";
	if ($options{command} !~ /^(\s+)?(cp|mv)\s+.*/) {
		die "Deprecated usage of -c option requires that the command must be 'cp' or 'mv'\n";
	}
	$options{command} =~ s/^(?:\s+)?(.*)\s+(.*)(?:\s+)?$/$1 %file% $2/;
}


my ($finished, $existingfile, $method);
if ($options{alt}) {
	$method = $ALT;
} else {
	$method = $INC;
}

if (!$options{overwrite}) {
	if ($method == $ALT) {  #check both log filenames
		foreach my $tempfile (&nextFilename($template, $method, 1,0), &nextFilename($template, $method, 2,0)) {
			if (stat($tempfile)) {$existingfile = $tempfile;}
		}
	} elsif ($method == $INC) {  #check existing files
		my $filter = &nextFilename($template, $method, "*", $options{date});  #make a filename filter
		if (`ls $filter 2>/dev/null`) {$existingfile = $filter;}
		elsif ($options{gzip}) {
			if (`ls $filter.gz 2>/dev/null`) {$existingfile = $filter.'.gz';}
		}
	}
	if ($existingfile) {
		print STDERR "logsplit: File '$existingfile' already exists - aborting...\n";
		exit;
	}
}

my ($select_timeout, $timeout, $t_next_rotate);
if ($options{timeout}) {
	$select_timeout=5; # This determines the accuracy of the timeout checks
	$timeout=$options{timeout} * 60;
	$t_next_rotate=next_timeout($timeout);
}

my $maxbytes = $size * 1024;
my $linecountdown = -1;  #not counting down
my $currlogfile = &nextFilename($template, $method, $filenum++, $options{date});
my $stopstring = $options{stop};  #NULL if not specified
open(CURRLOG, "> $currlogfile") || die "Can't open $currlogfile for writing";

# Prepare arguments for select
my ($rin, $rout, $ein, $eout);
$rin = $ein = '';
vec($rin,fileno(STDIN),1) = 1;
$ein = $rin;
my ($buff, @bufflines, $remainder, $numread, $dataready);
do {
	
	# Use select to wait for either some data to arrive, or a timeout to occur
	# We cannot mix buffered reads such as <STDIN> with select calls since the
	# buffered reads might block waiting for a carriage return. Instead we have to
	# ready using low level sysread call, and split into lines ourselves
	my $nfound = select($rout=$rin, undef, $eout=$ein, $select_timeout);
	
	$dataready = vec($rout,fileno(STDIN),1);
	#print ">> rdy $dataready\n";
	if ($dataready) {
		$numread = sysread STDIN, $buff, 1024;
		#print ">> Read: $numread\n";
		
		@bufflines = split(/\n/,$buff);
		if ($remainder) {
			$bufflines[0]=$remainder.$bufflines[0];
			$remainder=undef;
		}
		
		if (substr($buff,-1) ne "\n") {
			# buffer did not end in a newline, means last
			# line must be partial. Save to use with the next bit of data
			$remainder = pop @bufflines;
		}
	#	print ">> Bufflines:\n\t",join("\n\t", @bufflines),"\nRem: '$remainder'\n";

	}
	
	# Process any whole lines we have
	while (@bufflines) {
		$_ = shift @bufflines;
		
	
		print CURRLOG "$_\n";
		if ($options{echo}) {
			print "$_\n";
		}
		
		if ($linecountdown >= 0) {
			if ($linecountdown-- <=0) {last;}
		}
		
		if ($stopstring && /$stopstring/) {
			$linecountdown = $options{stoplines};  #start counting down
		}
		
		$currbytes += length($_);
	}
	
	#printf ">> Check file rotations: ($currbytes >= $maxbytes || ($t_next_rotate && %s >= $t_next_rotate)\n", time;

	if (($maxbytes > 0 and $currbytes >= $maxbytes) || ($t_next_rotate && (time >= $t_next_rotate))) {
		#print ">> Rotating\n";
		close(CURRLOG);
		if ($options{gzip}) {
			`gzip -f $currlogfile`;
		}
		if ($options{command}) {
			my $finalFilename = defined $options{gzip} ? $currlogfile . '.gz' : $currlogfile;
			my $command = $options{command};
			$command =~ s/%file%/$finalFilename/g;
			
			if (-e $finalFilename) {
				system($command);
			} else {
				warn "Could not execute command '$command' on file '$finalFilename', as it does not exist\n";
			}
		}
		$currlogfile = &nextFilename($template, $method, $filenum++, $options{date});
		open(CURRLOG, "> $currlogfile") || die "Can't open $currlogfile for writing";
		$currbytes = 0;
		if ($timeout) { 
			$t_next_rotate=next_timeout($timeout);
		}
		
	}
# We want to continue until either
# 1. EOF on input
# 2. one of the other conditions above has resulted in "last" being executed
# To detect EOF: an fd at EOF is always considered ready by select, but should
# return 0 bytes on a read
} until ($dataready && $numread == 0);

close(CURRLOG);
print STDERR "logsplit: Last file written to: $currlogfile\n";
