#!/usr/bin/perl
##
###
### Author: eqsvimp    Eric Yin Z
### $Id: itkreport 9360 2014-11-04 05:06:20Z eqsvimp $
##
### GSC CHINA Tool & Data Centre Service
### 
#

use FindBin qw($Bin);
use lib "../../../lib/";
use lib "$Bin/../lib/";
use Itk::Utils::LoginSession qw(eca_login node_login);
use Itk::Utils::SendMailSession qw(sendCUDBEmail);
use warnings;


my (@ERROR,@PRINT, @OK);

my %ecainfo = (  'user'      => "eca",  
                 'password'  => "ecastm",  
                 'port'      => 30023,
                 'cliPrompt' => "\@",
                 'host'      => "localhost"
  
);


my %nodeinfo = ( 'user'      => "root",
                 'password'  => "ericsson",
                 'cliPrompt' => "CUDB.*SC.*#",
                 'host'      => "10.252.55.83"

);

my %bladeinfo = ( 'user'      => "expert",
                 'password'  => "expert",
                 'cliPrompt' => ".*\@blade.*",
                 'host'      => "10.252.55.86 -p 2024 -c 3des-cbc"

);

my %nwiinfo = (  'user'      =>"admin",
                 'password'  => "\r",
                 'cliPrompt' => "CUDB.*NWI.*#",
                 'host'      => "192.168.0.253"

);

my %comhash;



$NOT_REGEX ='.*\W(not|no|nok)\W.*';
$LOCK_REGEX = '.*\W(Locked|Disabled).*';

my $session = new Itk::Utils::LoginSession();

$session->setField(%ecainfo);
$session->ecaLogin();
$session->setField(%nodeinfo);
$session->nodeLogin();


my @res;

$read = $session->commandCommit("pmreadcounter | grep failed");
@pmfailed_first = captureFromTo($read,"pmreadcounter","#$%");
$read = $session->commandCommit("pmreadcounter | grep dropped");
@pmdropped_first = captureFromTo($read,"pmreadcounter","#$%");
print "\nwaiting for 60 seconds \n";








$cm = "cudbSystemStatus";
$read = $session->commandCommit($cm);
@res = captureFromTo($read,"Checking BC clusters:","Checking System Monitor BC status in local node:");
push @ERROR, map{ $_ = "WARNING:  ".$_." => ".getParentInfo($read,$_ ,'Site')} grep {/not|no|notok|nok/i} grep {/running/i} @res;

@res=();@res = captureFromTo($read,"Checking System Monitor BC status in local node:","Checking Clusters status:");
push @ERROR, map{ $_ = "WARNING:  ".$_} grep {/$NOT_REGEX/i} grep {/running/i} @res;

@res=();@res = captureFromTo($read,'Checking Clusters status:','Checking NDB status:');
push @ERROR, map{ $_ = "WARNING:  ".$_." => ".getParentInfo($read,$_ ,'Node')}  grep {/nok/i} @res;

@res=();@res = captureFromTo($read,'Checking NDB status:','Checking Replication Channels in the System:');
push @ERROR, map{ $_ = "WARNING:  ".$_} grep {/$NOT_REGEX/i} @res;

push @PRINT,"replication channels  =>\n";
@res=();@res = captureFromTo($read,'Checking Replication Channels in the System:','Printing Alarms');
#push @PRINT, map{ $_=~ s/\e\[\d+m|\cI//g; } @res;  # remove paticular symbol"\e[0"
map{ $_=~ s/\e\[\d+m|\cI//g; } @res;  # remove paticular symbol"\e[0"
push @PRINT, @res;
#push @PRINT,  @res;
splice (@PRINT, -1); # remove last row

@res=();@res = captureFromTo($read,'Checking Process:','!@#LastRow$%');
#@res=("\cIStoragenotification Engine process (ndbd).................Not Running");
push @ERROR, map{ $_ = "WARNING:".$_} grep {/$NOT_REGEX/i} grep {/running/i}@res;
unless(@ERROR){ establishDB($cm,"OK",$read);}
else{ establishDB($cm,"ERROR $cm", $read);}



$read = $session->commandCommit("cudbHaState");
@res=();@res = captureFromTo($read,"AMF cluster state:","CoreMW HA state:");
push @ERROR, map{ $_ = "WARNING:  ".$_ } grep {/$LOCK_REGEX/} grep {/Amf/i} @res;

$cm = "CoreHW HA state";
@res = captureFromTo($read,"CoreMW HA state:","COM state:");
@tmp =();push @tmp, grep {!/CoreMW HA state:/} grep {!/----/} @res;
if(!($tmp[0] =~ /ACTIVE/i && $tmp[1] =~ /STANDBY/i || $tmp[0] =~ /STANDBY/i && $tmp[1] =~ /ACTIVE/i))
{
  push @ERROR,"CoreMW HA state =>\n";
  push @ERROR, "ERROR: One is Active, another should be Standby!";
  push @ERROR, @res;
  establishDB($cm,"ERROR", $read);    
}
else
{
 establishDB($cm,"OK",$read);  
}


$cm = "COM state";
@res = captureFromTo($read,"COM state:","SI HA state:");
@tmp =();push @tmp, grep {!/COM state:/} grep {!/----/} @res;
if(!($tmp[0] =~ /ACTIVE/i && $tmp[1] =~ /STANDBY/i || $tmp[0] =~ /STANDBY/i && $tmp[1] =~ /ACTIVE/i))
{
  push @ERROR,"COM state =>\n";
  push @ERROR, "ERROR: One is Active, another should be Standby!";
  push @ERROR, @res;
  establishDB($cm,"ERROR", $read);
}
else
{
 establishDB($cm,"OK",$read);  
}






#push @PRINT,"fmactivealarms =>\n";
$cm = "fmactivealarms";
$read = $session->commandCommit("fmactivealarms");
@res=();@res = captureFromTo($read,"Active alarms:","#$%");
foreach my $item(@res)
{
if($item =~ /Severity.*: (.*)/)
{
  if($1 =~ /warning|critical|major/i)
  {
    push @ERROR, "fmactivealarms -n =>".$item;
    @tmp =();push @tmp, map{ if($_ =~/Severity/) {$_ = "ERROR:  ".$_ ;} else{ $_=$_;}} grep {!/Active alarms:/} @res;
    push @ERROR, @tmp;
    $stat = join "\cM\cJ",@tmp;
    establishDB($cm,"ERROR", $stat);
  }
}

}
unless(@tmp){establishDB($cm,"OK",$read); }
#if(@tmp) {push @PRINT,"fmactivealarms -n =>\n"; push @ERROR, @tmp;} @tmp =();


#push @PRINT,"tipc-config -n =>\n";
$cm = "tipc_config";
$read = $session->commandCommit("tipc-config -n");
@res=();@res = captureFromTo($read,"Neighbors:","#$%");
@tmp =();push @tmp, map{ $_ = "WARNING:  ".$_ } grep {/up/} grep {!/Neighbors/} @res; # grep down tipc
if(@tmp) {push @ERROR,"tipc-config -n =>\n"; push @ERROR, @tmp;$stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR: ".$stat, $read);} 
else{establishDB($cm,"OK",$read); }


$cm = "ntpq -p";
$read = $session->commandCommit("ntpq -p");
@res=();@res = captureFromTo($read,"remote","#$%");
@tmp =();push @tmp,  @res;
if(@tmp) {push @PRINT,"ntpq -p =>\n"; push @PRINT, @tmp; establishDB($cm,"PRINT",$read);} @tmp =();


$cm = "filesystem HDD space";
$read = $session->commandCommit('for a in `awk \'/^node/ { print $4 }\' /cluster/etc/cluster.conf`;do echo $a; ssh $a df -h; done;');
@res=();@res = captureFromTo($read,"#$%^","#$%");
@tmp =();push @tmp, grep {!/echo/} @res;
my $flag = 0;
foreach my $item(@res)
{
  
  if($item =~ /.*\s(\d+)%.*/)
  {
    if($1 >= 75) 
    {
       
       if($flag == 0) {push @t_ERROR,"filesystem status HDD SPACE > 80% -p =>"; $flag = 1;}
       push @t_ERROR, getParentInfo($read,$item ,'\w+_\d+_\d').$item;
    }
  }


}
push @ERROR, @t_ERROR;
if(@t_ERROR){$stat = join "\cM\cJ",@t_ERROR; establishDB($cm,"ERROR", $stat); }
else{establishDB($cm,"OK",$read);}

#if(@tmp) {push @PRINT,"filesystem status -p =>\n"; push @PRINT, @tmp;} @tmp =();

$cm = "filesystem status";
$read = $session->commandCommit('for a in `awk \'/^node/ { print $4 }\' /cluster/etc/cluster.conf`;do   echo $a; ssh $a netstat -i; done;');
@res=();@res = captureFromTo($read,"#$%^","#$%");
@tmp =();push @tmp, grep {!/echo/} @res;
if(@tmp) {push @PRINT,"filesystem status -p =>\n"; push @PRINT, @tmp;establishDB($cm,"PRINT",$read);} @tmp =();

#cudbMpstat
#$session->{'cliPrompt'} = "Next";

$session->{'timeout'} = 7;
$cm = "cudbMpstat";
$read = $session->commandCommit("cudbMpstat;");
@res=();@res = captureFromTo($read,"Next","Next");
@tmp =();push @tmp, @res;
if(@tmp) {push @PRINT,"cudbMpstat =>\n"; push @PRINT, @tmp;shift @tmp;shift @tmp;$stat = join "\cM\cJ",@tmp; establishDB($cm,"PRINT", $stat);} 



$cm = "dhcpd status";
$session->commandCommit("\cC");
$read = $session->commandCommit("/etc/init.d/dhcpd status");
print $read; # read : /etc/init.d/dhcpd status\cM\cJChecking DHCP daemon \e7\e[?25l\cM\e[80C\e[10D\e[1;32mrunning\e[m\cO\e8\e[?25h\cM\cJ
if($read =~ /.*daemon.*\e.*m(.*)\e\[m.*/)
{
  if($1 ne "running")
  {
    push @ERROR,"dhcpd status =>NOT running\n"; 
    establishDB($cm,"ERROR",$read);
  }
  
}


$cm = "Alarm Raise"; 
$read = $session->commandCommit("cat /var/log/ESA/alarms.log | grep -c \"Alarm Raise\"");
if($read =~ /.*\cM\cJ(.*)\cM\cJ/)
{
    push @PRINT,"Alarm Raise => $1\n";
    establishDB($cm,"PRINT",$read);
}
print $read; # read :  cat /var/log/ESA/alarms.log | grep -c \"Alarm Raise\"\cM\cJ573\cM\cJ

$cm = "Alarm Clear";
$read = $session->commandCommit("cat /var/log/ESA/alarms.log | grep -c \"Alarm Clear\"");
if($read =~ /.*\cM\cJ(.*)\cM\cJ/)
{
    push @PRINT,"Alarm Clear => $1\n";
    establishDB($cm,"PRINT",$read);
}
print $read; # read :  cat /var/log/ESA/alarms.log | grep -c \"Alarm Raise\"\cM\cJ573\cM\cJ




#for a in `awk '/^node/ { if (substr($4,1,2) == "PL") {print $4} }' /cluster/etc/cluster.conf`;do ssh $a "echo $a ;/etc/init.d/cudbLDAPFrontEnd status";
#done;
$cm = "slapd process";
$read = $session->commandCommit('for a in `awk \'/^node/ { if (substr($4,1,2) == "PL") {print $4} }\' /cluster/etc/cluster.conf`;do ssh $a "echo $a ;/etc/init.d/cudbLDAPFrontEnd status";
done');
@res=();@res = captureFromTo($read,"#$%^","#$%");
@tmp =();push @tmp, grep {!/UP/} grep {/is/} @res;
if(@tmp) {push @ERROR,"slapd process =>\n"; push @ERROR, @tmp;$stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR", $stat);}
else{establishDB($cm,"OK",$read);}


$session->{'timeout'} = 600;
$cm = "esaclusterstatus";
$read = $session->commandCommit('esaclusterstatus');
@res=();@res = captureFromTo($read,"#$%^","#$%");
#@tmp =();push @tmp, map{ $_ =~ s/\cI/ /g;  } grep {!/cluster|Cluster/}  @res;
@tmp =();push @tmp, grep {!/cluster|Cluster/}  @res;
if(@tmp) {push @PRINT,"esaclusterstatus =>\n"; push @PRINT, @tmp;$stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR", $stat);}
else{establishDB($cm,"OK",$read);}

$cm = "esa status";
$read = $session->commandCommit('esa status');
@res=();@res = captureFromTo($read,"#$%^","#$%");
@tmp =();push @tmp,  grep {!/status/} grep {!/running/} @res;
if(@tmp) {push @ERROR,"esa status =>\n"; push @PRINT, @tmp;$stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR", $stat);} 
else{establishDB($cm,"OK",$read);}



$cm = "Memory status";
$read = $session->commandCommit('for i in `cat /cluster/etc/cluster.conf | grep node | grep -v \'^#\' | awk \'{ print $4 }\'`; do echo "$i:"; ssh $i "free -m"; done');
@res=();@res = captureFromTo($read,"#$%^","#$%");
@tmp =();push @tmp,  grep {!/cluster/} @res;
if(@tmp) {push @PRINT,"Memory status =>"; push @PRINT, @tmp;}
$stat = join "\cM\cJ",@tmp; establishDB($cm,"PRINT", $stat);
#establishDB($cm,"PRINT",$read);


#monitor last 2
$cm ="Crontab -l "; # name crontab -l  SC_2_1  SC_2_2
$read = $session->commandCommit('crontab -l | tail -n 2');
@res=();@res = captureFromTo($read,"#$%^","#$%");
@tmp =();push @tmp, map{ $_ = "PRINT:  ".$_ } grep {!/crontab/} @res;
if(@tmp) {push @PRINT,"crontab -l =>\n"; push @PRINT, @tmp;} @tmp =();
establishDB($cm,"PRINT",$read);

$session->commandCommit('ssh SC_2_2');#verify different situation email

$cm = "cudbAnalyser --auto-check";
$read = $session->commandCommit('cudbAnalyser --auto-check');#verify 
@res=();@res = captureFromTo($read,"#$%^","#$%");
@tmp =();push @tmp, map{ $_ = "PRINT:  ".$_ } grep {!/cudbAnalyser/} @res;
if(@tmp) {push @PRINT,"cudbAnalyser --auto-check =>\n"; push @PRINT, @tmp;} @tmp =();
establishDB($cm,"PRINT",$read);

#? verify this ???? Summary: slaves checked: 2 -> PASSED: 2, FAILED: 0, UNKNOWN: 0 
$cm = "cudbCheckConsistency";
$read = $session->commandCommit('cudbCheckConsistency'); #failed 0 unknown 0 all OK
@res=();@res = captureFromTo($read,"#$%^","#$%"); 
@tmp =();push @tmp,  grep {!/OK/} grep {/.*Node.*consistency.*/} @res;
foreach my $item(@res)
{
  if($item =~ /.*FAILED: (\d+), UNKNOWN: (\d+).*/)
  {
    if($1 != 0){push @tmp , "FAILED: $1" };
    if($2 != 0){push @tmp , "UNKNOWN: $2" };
  }
}

if(@tmp) {push @ERROR,"cudbCheckConsistency =>\n"; push @ERROR, @tmp;$stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR", $stat);} 
else{establishDB($cm,"OK",$read);}


$cm = "cudbCheckReplication -b 5";
$read = $session->commandCommit('cudbCheckReplication -b 5');#failed 0 unknown 0 all OK
@res=();@res = captureFromTo($read,"#$%^","#$%");
@tmp =();push @tmp,  grep {!/OK/} grep {/.*Node.*replication.*/} @res;
if(@tmp) {push @ERROR,"cudbCheckReplication =>\n"; push @ERROR, @tmp;$stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR", $stat);} 
else{establishDB($cm,"OK",$read);}

$cm = "ldapAccess slapd";
$read = $session->commandCommit('for port in `cat /cluster/home/cudb/dataAccess/ldapAccess/ldapFe/config/slapd.conf | grep -v \'^#\'| grep -v \'T_\'| grep -v site | egrep \'(datastore)\' | awk \'{ print $5 }\' | cut -d ":" -f2`; do echo "$host"; ndb_mgm -c localhost:$port -e "all status";echo"";done');
@res=();@res = captureFromTo($read,"#$%^","#$%");
@tmp =();push @tmp, grep {/started/} grep {/.*Node.*mysql.*/} @res;
if(@tmp) {push @ERROR,"ldapAccess slapd =>\n"; push @ERROR, @tmp;$stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR", $stat);} 
else{establishDB($cm,"OK",$read);}





@tmp = ();
$read = $session->commandCommit("pmreadcounter | grep failed");
@pmfailed_second = captureFromTo($read,"pmreadcounter","#$%");
$read = $session->commandCommit("pmreadcounter | grep dropped");
@pmdropped_second = captureFromTo($read,"pmreadcounter","#$%");

@tmp = output_delta(\@pmfailed_first,\@pmfailed_second);
$cm = "pmreadcounter failed";
push @PRINT,"pmreadcounter failed =>\n";
push @PRINT, @tmp; 
$stat = join "\cM\cJ",@tmp; establishDB($cm,"PRINT", $stat);
@tmp = ();

$cm = "pmreadcounter dropped";
@tmp = output_delta(\@pmdropped_first,\@pmdropped_second);
push @PRINT,"pmreadcounter dropped =>\n";
push @PRINT, @tmp; 
$stat = join "\cM\cJ",@tmp; establishDB($cm,"PRINT", $stat);
@tmp = ();




#testing
$session->commandCommit('ssh SC_2_1');
#testing

$session->setField(%bladeinfo);
$session->nodeLogin();
#notice the enter number may exceed the expected number so that the next expect string will be the same with blade* which cause it cannot get the result
$cm ="show ManagedElement 1 DmxFunctions 1 DmxSysM 1 SystemInformation";
$read = $session->commandCommit("show ManagedElement 1 DmxFunctions 1 DmxSysM 1 SystemInformation\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r");
@res=();@res = captureFromTo($read,"#$%^","#$%");
@tmp =();@t_ERROR=();push @tmp, grep {/bladeRole/} @res;
for my $i (0..1) { if($tmp[$i] =~ /.*(active)/ || $tmp[$i] =~ /.*(standby)/) { $tmp[$i] = $1; } }
unless( $tmp[0] eq 'active' && $tmp[1] eq 'standby' || $tmp[1] eq 'active' && $tmp[0] eq 'standby')
{
  push @t_ERROR , $cm." =>";
  push @t_ERROR , "Dmxc 1 is $tmp[0]  and Dmxc 2 is $tmp[1]";
  
}
push @ERROR, @t_ERROR;
if(@t_ERROR){$stat = join "\cM\cJ",@t_ERROR; establishDB($cm,"ERROR", $stat);}
else{establishDB($cm,"OK",$read);}



$cm = "show all ManagedElement 1 Equipment 1 Shelf 0 Slot 0 Blade 1 administrativeState";
$read = $session->commandCommit('show all ManagedElement 1 Equipment 1 Shelf 0 Slot 0 Blade 1 administrativeState');
@res=();@res = captureFromTo($read,"#$%^","#$%");
@tmp =();push @tmp, grep {!/show all/}  grep {!/unlocked/} grep {!/ok/} @res;
if(@tmp) {push @ERROR,"Dmxc Show administrativeState Slot 0 =>\n" ;push @ERROR,@tmp; $stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR", $stat);} 
else{establishDB($cm,"OK",$read);}

$cm = "show all ManagedElement 1 Equipment 1 Shelf 0 Slot 25 Blade 1 administrativeState";
$read = $session->commandCommit('show all ManagedElement 1 Equipment 1 Shelf 0 Slot 25 Blade 1 administrativeState');
@res=();@res = captureFromTo($read,"#$%^","#$%");
@tmp =();push @tmp, grep {!/show all/} grep {!/ok/} grep {!/unlocked/} @res;
if(@tmp) {push @ERROR,"Dmxc Show administrativeState Slot 25 =>\n" ;push @ERROR,@tmp; $stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR", $stat);} 
else{establishDB($cm,"OK",$read);}

$cm = "show all ManagedElement 1 Equipment 1 Shelf 0 Slot 0 Blade 1 operationalState";
$read = $session->commandCommit('show all ManagedElement 1 Equipment 1 Shelf 0 Slot 0 Blade 1 operationalState');
@res=();@res = captureFromTo($read,"#$%^","#$%");
@tmp =();push @tmp, grep {!/show all/} grep {!/ok/} grep {!/enabled/} @res;
if(@tmp) {push @ERROR,"Dmxc Show operationalState Slot 0 =>\n" ;push @ERROR,@tmp;$stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR", $stat);}
else{establishDB($cm,"OK",$read);}

$cm = "show all ManagedElement 1 Equipment 1 Shelf 0 Slot 25 Blade 1 operationalState";
$read = $session->commandCommit('show all ManagedElement 1 Equipment 1 Shelf 0 Slot 25 Blade 1 operationalState ');
@res=();@res = captureFromTo($read,"#$%^","#$%");
@tmp =();push @tmp, grep {!/show all/} grep {!/ok/} grep {!/enabled/} @res;
if(@tmp) {push @ERROR,"Dmxc Show operationalState Slot 25 =>\n" ;push @ERROR,@tmp; $stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR", $stat);}
else{establishDB($cm,"OK",$read);}



my $enter = "\r";
for my $i (0..100){ $enter = $enter."\r";}
$cm = "show status ManagedElement 1 SystemFunctions 1 Fm 1 FmAlarm";
$read = $session->commandCommit("show status ManagedElement 1 SystemFunctions 1 Fm 1 FmAlarm".$enter);
@res=();
@res = captureFromTo($read,"#$%^","#$%");
=pod
if ($res[-1] =~ /.*\[(\w+)\]\[(.*)\]/) #\e[7m--More--\e[27m\cH\cH\cH\cH\cH\cH\cH\cH        \cH\cH\cH\cH\cH\cH\cH\cH[ok][2016-05-09 08:20:05] 
{ 
   $result = $1;
}
=cut
push @PRINT,"Dmxc show status FmAlarm =>\n "; push @PRINT, @res; @tmp =(); #PRINT
establishDB($cm,"PRINT",$read);


$cm = "ntp";
$session = eraseBladeEnter($session);
$read = $session->commandCommit("ntp");
@res=();@res = captureFromTo($read,"#$%^","#$%");
@tmp =();push @tmp, grep {!/ntp/} @res;
if(@tmp) {push @PRINT,"NTP =>\n"; push @PRINT,@tmp;}
establishDB($cm,"PRINT",$read);


$session->{'cliPrompt'} = ".*expert\@blade.*";
$cm = "show log alarm";
$read = $session->commandCommit("show log alarm");
@res=();@res = captureFromTo($read,"#$%^","#$%");
if(@res) {
  @res = splice (@res, -20, 20); # get last 20 items from the array
  push @PRINT,"show log alarm =>\n"; push @PRINT, @res;
}
$stat = join "\cJ",@res; establishDB($cm,"PRINT", $stat);



#NWI health check
$session->{'cliPrompt'} = "CUDB.*SC.*#";
$session->commandCommit("exit");
$session->setField(%nwiinfo);
$session->nodeLogin();

$cm = "show temperature";
$read = $session->commandCommit("show temperature");
@res=();@res = captureFromTo($read,"-","#$%");
@tmp =();push @tmp, grep {!/-----/} grep {!/Normal/}@res; 
if(@tmp) {
push @ERROR,"NWI show temperature =>\n"; push @ERROR,@tmp; push @PRINT,"NWI show temperature =>\n"; 
push @PRINT,@tmp; $stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR", $stat);}
else{establishDB($cm,"OK",$read);}

###handle the previous \s problem which need exit first and enter into the system again.
#$session->commandCommit("\cC");
#$session->{'cliPrompt'} = "CUDB.*SC.*#";
#$session->commandCommit("exit");
#$session->setField(%nwiinfo);
#$session->nodeLogin();
#$read = $session->commandCommit("show temperature");
$cm = "show log severity critical";
$read = $session->commandCommit("show log severity critical");
@res=();@res = captureFromTo($read,"$%^","#$%");
@tmp =();push @tmp, grep {!/show log severity critical/} grep {!/No log messages were displayed/} @res; 
if(@tmp) {push @PRINT,"NWI severity critical =>\n"; push @PRINT,@tmp;}
establishDB($cm,"PRINT",$read);

$cm = "show log severity error";
$read = $session->commandCommit("show log severity error");
@res=();@res = captureFromTo($read,"$%^","#$%");
@tmp =();push @tmp, grep {!/show log severity error/} grep {!/No log messages were displayed/} @res; 
if(@tmp) {push @PRINT,"NWI severity error =>\n"; push @PRINT,@tmp;}
establishDB($cm,"PRINT",$read);



$cm = "show ports no-refresh";
$read = $session->commandCommit("show ports no-refresh");
@res=();@res = captureFromTo($read,"===","===");
foreach my $item (@res)
{
  if($item =~ /.*\s+(\w)\s+(\w)\s+.*/)
  {
    if($1 ne 'E') {push @tmp, $item};
    if($2 ne 'A' and $2 ne 'R') {push @tmp, $item};
  }

} 
if(@tmp) {push @ERROR,"NWI severity critical =>\n"; push @ERROR,@tmp; $stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR", $stat);}
else{establishDB($cm,"OK",$read);}


#$session->{'timeout'} = 20;
#$session->commandCommit("\cC");
#$session->commandCommit("\cC");
#$session->commandCommit("\cC");
$cm = "show lacp";
$read = $session->commandCommit("show lacp");
@res=();@res = captureFromTo($read,"$%^","#$%");
@tmp =();push @tmp, grep {/LACP/} grep {/LACP/} grep {!/ports/}grep {!/Yes/}@res; 
if(@tmp) {push @ERROR,"NWI show lacp =>\n"; push @ERROR,@tmp;$stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR", $stat);}
else{establishDB($cm,"OK",$read);}



$session->{'timeout'} = 2;
$cm = "show switch";
$read = $session->commandCommit("show switch");
@res=();@res = captureFromTo($read,"SysHealth","Image Selected");
@tmp =();push @tmp, grep {/SysHealth|Watchdog|Current\sState/} grep {!/Enabled/} grep {!/OPERATIONAL/} @res;
if(@tmp) {push @ERROR,"NWI show swith =>\n"; push @ERROR,@tmp; $stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR", $stat);}
else{establishDB($cm,"OK",$read);}

$session->commandCommit("\cC");

$cm = "show memory";
$session->commandCommit("show memory");
$session->commandCommit("\s");
$read = $session->commandCommit("\s");
@res=();@res = captureFromTo($read,"$%^","#$%");
@tmp =();push @tmp, grep {!/SPACE/} grep {!/\cM/} grep {!/show memory/} grep {!/^s$/}@res; 
if(@tmp) {push @PRINT,"NWI show memory =>\n"; push @PRINT,@tmp; shift @tmp;$stat = join "\cM\cJ",@tmp; establishDB($cm,"PRINT", $stat);}


$session->commandCommit("\cC");
$cm = "show vrrp detail";
$session->commandCommit("show vrrp detail\r");
$read = $session->commandCommit("\s");
@res=();@res = captureFromTo($read,"$%^","#$%");
@tmp =();push @tmp, grep {!/show vrrp detail/} grep {/VRRP:/} grep {!/Enabled/} @res; 
if(@tmp) {push @ERROR,"NWI vrrp detail =>\n"; push @ERROR,@tmp; $stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR", $stat);}
else{establishDB($cm,"OK",$read);}

$cm = "show ospf neighbor detail";
$session->commandCommit("show ospf neighbor detail");
$session->commandCommit("\s");
$session->commandCommit("\s");
$session->commandCommit("\s");
$read = $session->commandCommit("\s");

@res=();@res = captureFromTo($read,"$%^","#$%");
@tmp =();push @tmp, grep {!/show ospf neighbor detail/} grep {/Neighbor is /} grep {!/up/} @res; 
if(@tmp) {push @ERROR,"NWI ospf neighbor =>\n"; push @ERROR,@tmp;$stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR", $stat);}
else{establishDB($cm,"OK",$read);}



$cm = "show vlan";
$session->commandCommit("show vlan");
$read = $session->commandCommit("\s");
@res=();@res = captureFromTo($read,"$%^","#$%");
@tmp =();push @tmp, grep {!/show vlan/}  @res; 
if(@tmp) {push @PRINT,"NWI show vlan =>\n"; push @PRINT,@tmp;}
else{establishDB($cm,"PRINT",$read);}

$cm = "show bfd vlan";
$session->commandCommit("show bfd vlan");
$session->commandCommit("\s");
$read = $session->commandCommit("\s");
@res=();@res = captureFromTo($read,"$%^","#$%");
@tmp =();push @tmp, grep {!/show bdf vlan/} grep {/BFD                  :/} grep {!/Enabled/}@res; 
if(@tmp) {push @ERROR,"NWI show bdf vlan =>\n"; push @ERROR,@tmp; $stat = join "\cM\cJ",@tmp; establishDB($cm,"ERROR", $stat);}
else{establishDB($cm,"OK", $read);}




my $data = "";
while(<DATA>)
{
  $data = $data.$_;
}


$data = $data."<h1>CUDB Health Check Error Checkpoints</h1>";
my $logfile = "/home/eqsvimp/healthcheck/cudb/print.log";
my $scriptlog = "/home/eqsvimp/healthcheck/cudb/script.log";

###write the @PTINT to print file;
system("rm -f $logfile");
open(FH, ">>$logfile");
foreach my $item(@PRINT){ print FH $item."\n";}
close FH;


foreach my $item(@ERROR)
{
  if($item =~ /=\>/) {$data = $data.'<h2>'.$item.'</h2>';}
  else{$data = $data.'<br>'.$item.'</br>'}
}

foreach $key (keys %comhash) 
{   
    if( $comhash{$key}{status} eq "OK")
    {
      $data = $data.'<br>'.'<h3>'.$key.'</h3>'.' OK'.'</br>';
      
       
    }
    #"$key=>$food{$key}\n";     #使用keys %food 遍历%food的每一个关键字
}      
$data = $data.'</body></html>';




##### Write Html

my $source_html = "/var/www/html/singtel/CUDBhealthcheck.htm";
my $des_html = "/var/www/html/singtel/cudb.htm";
$JSON = write_html(%comhash);
$json_file = "/var/www/html/singtel/cudbjson.txt";
open(FILE, ">$json_file");
print FILE "[".$JSON."]";
#var JSON=[];
#`sed -i 's/jsonranklists/jsonranklist/g'  ./CUDBhealthcheck.htm`;
#$JSON=`cat /var/www/html/singtel/cudbjson.txt | tr -d "\033"`;
#$string = "sed -i 's/\x1b//g'  /var/www/html/singtel/cudb.htm";
open(HTML, "$source_html");
@content = <HTML>;close HTML;
`rm -f $des_html`;
open(HTML, ">>$des_html");
foreach my $item(@content)
{
  if($item =~ /JSONERIC/)
  {
    print HTML "var jsonranklist=[$JSON]; //JSONERIC"; next;
  }
print HTML $item;  
}

close HTML;


#### Send Email

#sendCUDBEmail("testing",$data,"eqsvimp",$logfile,$scriptlog);

print "";









sub eraseBladeEnter
{ 
  my $session =shift;
  $cliPrompt = $session->{'cliPrompt'};
  $session->{'cliPrompt'} = "ok";
  $session->commandCommit("ntp");
  $session->{'cliPrompt'} = $cliPrompt;
  $session->commandCommit("ntp");
  return $session;
}




sub getParentInfo
{
  my ($read,$fromKey,$targetKey) = @_;
  my @rows =  split '\cM\cJ', $read;
  my $count = 0;
  foreach my $row (@rows)
   {
  if($row eq $fromKey)
        {
          last;
        }
    $count++;
   }

  for( 0 .. $count) 
  {
    if($rows[$count-$_] =~ /$targetKey/)
    {
       return $rows[$count-$_];
    }
  }  
}



sub captureFromTo
{
    my ($read,$from,$to) = @_;
    my @result;
    my $count = 0;
    my @rows =  split '\cM\cJ', $read;
    my ($start,$end) = (0,$#rows);
    foreach my $row (@rows)
   {
  if($row =~ /$from/)
        {
          $start = $count;
          $from = "&$^&";
          next;
        }
        if($row =~ /$to/)
        {
          $end = $count;
          last;
        }
        $count++;
   }    
    for ($start..$end){ push @result, $rows[$_];} 
    return @result;
}



sub output_delta
{
   my ($first, $second) = @_;
   my @final;
   for my $i (1..(scalar(@$first)-1)) 
   {
      @first_tmp = split ';' , $$first[$i];
      @second_tmp = split ';' , $$second[$i];
      $delta = abs($first_tmp[3]-$second_tmp[3]);
      $final[$i] = $$second[$i]."; Delta = ".$delta;
   }
   shift @final;
   return @final;


}


sub establishDB
{
  my ($command,$status,$read ) = @_;
  $comhash{$command}{status} = $status;
  my @rows =  split '\cM\cJ', $read;
  my $res = "";
  foreach my $item (@rows)
  {
    $item =~ s/"//g;
    $item =~ s/'//g;
    $item =~ s/\cM//g;
    #$item =~ s/\\cJ//g;
    $item =~ s/\cH//g;
    $item =~ s/\cJ//g;
#    $item =~ s/\cH//g;
$item =~ s/\x1b//g;
$item =~ s/\[7m--More--\[27m//g;
#$item =~ s/\e\[.*m//g;
#$item =~ s/\e\[.*K//g;    
$item =~ s/\cI/ /g;    
    $res = $res.'<p>'.$item.'</p>';
  }
  $comhash{$command}{output} = $res;
}



sub write_html
{
   my (%comhash) = @_;
   my %data;
   my ($data_error, $data_print, $data_ok) = "";
   #$pre = "\n"."<div id=\"jqxNotification\" style=\"width: 100%;\"><div>";
   #$next = "<\/b><\/div><div style=\"font-size: smaller; text-align: center;\">Click to view.<\/div><\/div>"; 
   my %json = (  'ERROR'      =>  "",  
                 'OK'         =>  "",  
                 'PRINT'      =>  "",
                 
    );
   my $JSON = "";
   foreach $key (keys %comhash) 
   {

    # write JSON
    #var jsonranklist = ['{"command_name":"cudb_system","status":"OK","output":"1"}', '{"command_name":"cudb_monitor","status":"ERROR","output":"2"}', '{"command_name":"cudb_abc","status":"PRINT","output":"3"}'];
      if($comhash{$key}{status} eq "ERROR")
      {
        $item_error = '{"command_name":"'.$key.'","status":"'.$comhash{$key}{status}.'","output":"'.$comhash{$key}{output}.'"}';
        $json{ERROR} = $json{ERROR}."\'$item_error\', ";
    #    $data{data_error} = $data{data_error}.$pre.$key.'<b>'.$comhash{$key}{status}.$next;
      }
      if($comhash{$key}{status} eq "PRINT")
      {
        $item_print = '{"command_name":"'.$key.'","status":"'.$comhash{$key}{status}.'","output":"'.$comhash{$key}{output}.'"}';
        $json{PRINT} = $json{PRINT}."\'$item_print\', ";
    #    $data{data_print} = $data{data_print}.$pre.$key.'<b>'.$comhash{$key}{status}.$next;
      }       
      if($comhash{$key}{status} eq "OK")
      {
        $item_ok = '{"command_name":"'.$key.'","status":"'.$comhash{$key}{status}.'","output":"'.$comhash{$key}{output}.'"}';
        $json{OK} = $json{OK}."\'$item_ok\', ";
    #    $data{data_ok} = $data{data_ok}. $pre.$key.'<b>'.$comhash{$key}{status}.$next;
        
      }

    }
    
      $JSON = $json{ERROR}.$json{PRINT}.$json{OK};
    $JSON=~s/, $//g; #reomove the , for the last character
    return $JSON;
}











__DATA__
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
<head>
<title>MTAS Health Check</title>
<style type="text/css">
<!--/* <![CDATA[ */
body
{
    background-color: FloralWhite;
    font-family: Calibri,Verdana,serif;
    color: black;
}

h1, h2, h3 
{
    color: FloralWhite;
    font-family: Ericsson Capital, sans-serif;
    font-weight: bold;
    margin: 3ex 0 1ex 0;
}

h1
{   
    background: RoyalBlue; width:50px; 
    font-family: Ericsson Capital TT, sans-serif;
    font-size: large;
}

h2
{   
    background: FireBrick; width:100px; 
    font-family: Rockwell, sans-serif;
    font-size: medium;
}

h3
{   
    background: ForestGreen; width:300px; 
    font-family: Rockwell, sans-serif;
    font-size: medium;
}



table
{
    border-collapse: collapse;
}

.checks, .verdict
{
    margin-left: 1.5em;
}

tr.check td
{
    display: inline-block;
    padding: 0.25ex 0 0.25ex 0.75em;
}

tr.check td.symbol
{
    padding-left: 0;
}

.value
{
    font-weight: bold;
}

.key:after
{
    content: ": ";
}

.comment:before
{
    content: "-- ";
}

.comment
{
    font-style: italic;
    color: DarkSlateGray;
}

.symbol
{
    width: 0.5em;
    text-align: center;
    overflow: hidden;
}

span.symbol
{
    display: inline-block;
    height: 1.5ex;
}

.symbol.ok
{
    background-color: Green;
    color:  Green;
}

.symbol.verify
{
    background-color: #FF7E00;
    color: #FF7E00;
}

.symbol.fail
{
    background-color: FireBrick;
    color: FireBrick;
}

.symbol.error
{
    background-color: DarkRed;
    color: DarkRed;
}

.symbol.info
{
    background-color: SlateGray;
    color: SlateGray;
}

.sign
{
    color: Ivory;
    text-transform: uppercase;
    padding: 1px 7px;
}

.sign.ok
{
    background-color: Green;
}

.sign.verify
{
    background-color: #FF7E00;
}

.sign.fail
{
    background-color: FireBrick;
}

.sign.error
{
    background-color: DarkRed;
}

.sign.info
{
    background-color: SlateGrey;
}

.counters th
{
    text-align: left;
    font-weight: bold;
}
.counters td.text
{
    text-align: left;
}
.counters td.numeric
{
    text-align: right;
}
.counters th:first-child
{
    padding-left: 0;
}
.counters td.numeric.zero
{
    visibility: hidden;
}
.counters td.numeric.zero:before
{
    content: "-";
    visibility: visible;
    margin-right: -0.6em;
}
.counters td, .counters th
{
    padding-left: 10px;
}


/* ]]> */-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body>


