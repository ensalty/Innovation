use Expect;
use MIME::Lite;
use Time::gmtime;
use FindBin qw($Bin);
use lib "../../../lib/";
use lib "$Bin/../lib/";
use Itk::Utils::SmsAlertSession;


my $localtime = localtime;
my $message = "ENIQ SERVER Monitoring
                <ENIQ  STATUS>: OK
                <ETLC LOADING>: OK
                <DATA LOADING>: OK
                <TIME>: $localtime
                <Author>: GSC Tool & Data Center Developer Eric Yin Z ";



my %smsinfo = (  

		 'from'        => 'PDLGSCCPRO@pdl.internal.ericsson.com',
                 'to'        => 'eric.z.yin@ericsson.com',
                 'smtp'        => 'smtp.eapac.ericsson.se',
#                 'contactlist' => '+6583073925@sms.ericsson.com;+6597806544@sms.ericsson.com;+6597207335@sms.ericsson.com',
                 'contactlist' => '+6583073925@sms.ericsson.com;+6597748745@sms.ericsson.com;eric.z.yin@ericsson.com',
                 'subject'     => $message,
                 'message'     => "email message",
);


my $sms = new Itk::Utils::SmsAlertSession();
$sms->setField(%smsinfo);
$sms->sendWorker();





