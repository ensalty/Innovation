#!/usr/bin/perl
#------------------------------------------------------------------------------
#
# ISP Tool Kit - Trace Control Program
#
# (c) Ericsson AB 2006 - All Rights Reserved
#
# No part of this material may be reproduced in any form
# without the written permission of the copyright owner.
# The contents are subject to revision without notice due 
# to continued progress in methodology, design and manufacturing. 
# Ericsson shall have no liability for any error or damage of any
# kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of
# Telefonaktiebolaget LM Ericsson. All other trademarks mentioned
# herein are the property of their respective owners. 
#------------------------------------------------------------------------------
#
# $Id: itktrace 7937 2012-12-10 04:20:35Z ebreoli $

use strict;
use Getopt::Long;
use File::Find;
use FindBin qw($Bin $Script);

my ($help, $setup, @enable, @disable, $status, $stacktrace, $syslog, $verbose, $reset);

my @itk_libs = ( "$Bin/../lib" );
if ($ENV{PERL5LIB}) {
	push @itk_libs, grep $_, split ':',$ENV{PERL5LIB};
}

# Determine running environment, then find config file
my $configFile;
if ($ENV{ECAHOME}) {
	if ($Bin eq '/opt/ericsson/eca/bin') {
		# RPM Installation
		$configFile = '/etc/eca/trace.conf';
	} else {
		$configFile = "$ENV{ECAHOME}/etc/trace.conf";
	}
} else {
	# Old nits pack
	if (-e "$Bin/../lib/Itk/NitsPackServer.pm") {
		$configFile = "$Bin/../etc/trace.conf";
	} else {
		$configFile = '/etc/itk/trace.conf';
	}
}

my @trace_groups=qw(DEBUG1 DEBUG2 DEBUG3 DEBUG4 DEBUG5 DEBUG6 DEBUG7 DEBUG8 DEBUG9 ENTER );
my $n=1;
my %trace_group_index=map { $_ => $n++ } @trace_groups;

# Flag: set if there were any changes and the constants need to be re-written
my $changed=0;

# Hash which will contain a key for each package existing
# The values are arrays containing the trace setting for each level
my %packages;

# The state of stack backtrace printing: this becomes a constant Itk::Server::STACKTRACE
my $stacktrace_state=0;

# The state of syslog output: this becomes a constant Itk::Server::SYSLOG
my $syslog_state=0;


# Loop through all ITK modules. Look for declaration of packages
# and make sure that %packages contains an entry for them. Also
# delete any entries from %packages that do not refer to actual
# packages
sub find_packages {
	my @itk_libs=@_;
	my %existing_packages = map { $_ => 1 } keys %packages;
	
	# Make sure there is a 'main' debug entry
	delete $existing_packages{'main::'} if exists $existing_packages{'main::'};
	$packages{'main::'}=[] unless exists $packages{'main::'};
	
	# callback sub for file::find
	sub process_file {
		return unless /.pm$/; # Skip all except perl mods
		my $file="$_";
		# Need to localise $_ since File::Find will break otherwise..
		{
			local $_;
			my $fh;
			open $fh, $file or die "$Script: Error opening $file: $!\n";
			while (<$fh>) {
				if (/^\s*package\s*(Itk::[\w:]+)\s*;\s*$/) {
					#print "Found package $1 in file $file line $.\n";
					next if $1 eq 'DB';
					my $package="$1::";
					$packages{$package}=[] unless exists $packages{$package};
					delete $existing_packages{$package} if exists $existing_packages{$package};
				}
			}
			close $fh;
		}
	}
	
	find(\&process_file, @itk_libs);
	
	foreach my $p (keys %existing_packages) {
		delete $packages{$p};
	}
}

# Load the current debug constants from the config file
sub load_constants {
	my ($fh, $package, $group, $mask);
	unless (open $fh,$configFile) {
		return if $setup;
		die "$Script: Could not open $configFile for reading: $!\nUse '$Script -setup' to create this file\n";
	}
	while (<$fh>) {
		chomp;
		if (($package,$group,$mask) = (/^\s*sub\s*([\w:]+::)(\w+)\s*\(\s*\)\s*{\s*(\d+)\s*}\s*;\s*$/)) {
			if ($group eq 'STACKTRACE') {
				$stacktrace_state = $mask;
				next;
			}
			next unless exists $trace_group_index{$group};
			$packages{$package}[$trace_group_index{$group}]=$mask;
		}
	}
	close $fh;
}

# save the debug constants into a config file
sub save_constants {
	set_debug();
	my ($fh);
	open $fh,">$configFile" or die "$Script: Could not open $configFile for writing: $!\n";
	print $fh "# ITK Debug Constants\n";
	print $fh "# Auto Generated: Do not hand edit this file\n";
	print $fh "# Use $Script to set the constants\n";
	foreach my $package (sort keys %packages) {
		print $fh "sub ${package}DEBUG () { $packages{$package}[0] };\n";
		foreach my $group (@trace_groups) {
			my $v = $packages{$package}[$trace_group_index{$group}] ? $packages{$package}[$trace_group_index{$group}] : '0';
			print $fh "sub $package$group () { $v };\n";
		}
	}
	print $fh "sub Itk::Server::STACKTRACE () { $stacktrace_state };\n";
	print $fh "sub Itk::Server::SYSLOG () { $syslog_state };\n";
	print $fh "1;";
	close $fh;
}

# The main DEBUG flag is set if any of the other groups are enabled
# this is for legacy code which does not yet use DEBUGn
sub set_debug {
	foreach my $package (keys %packages) {
		my $debug=0;
		for (my $i=$#trace_groups; $i>0; $i--) {
			$debug = ($debug << 1) + ($packages{$package}[$i] ? 1 : 0);
		}
		$packages{$package}[0]=$debug << 3;
	}
}

sub print_usage {
	print <<_EOT_;
Trace Administration Utilitiy

Usage:

   $Script [-enable [package_match=]group[,group...]] [-disable [package_match=]group[,group...]]
            [-status [package_match]] [-stacktrace [on|off]] [-reset] [-setup]
            [-c file] [-h] [-v]

Options:
-enable | -e [package_match=]group[,group...]
        Enable one or more groups. If the package_match expression is supplied
        then traces are only enabled for packages that match the expression.
        One or more groups can be specified. Available groups are:
        @trace_groups
        In addition, if 'all' is specified then all groups are enabled.
        
-disable | -d [package_match=]group[,group...]
        Disable one or more groups. Parameters are the same as for -enable.
        
-status | -s [package_match]
        Show current status of debug groups. If package_match is supplied then
        only matching packages are shown.

-stacktrace | -t [on|enable|true|1|off|disable|false|0|eval]
        Enable or disable the printing of stack backtraces on fatal errors. If
        no value is given then the current stack trace printing state is inverted.
        If the special value 'eval' is used then stack trace is printed even if
        the error is trapped with an 'eval'.

-syslog |-y [on|enable|true|1|off|disable|false|0|all]
        Enable or disable the logging of info, warning, error or debug messages
        via the system logger (ie. syslog). If no value is given, then the
        current syslog state is inverted.  Regardless of this setting, all
        messages, of any type, will be directed to STDERR.  However, the 
        syslog setting has the following additional effect:
        If syslog is OFF: ERROR/WARN/INFO also go to messages.log.
        If syslog is ON : ERROR/WARN/INFO also go to syslog (ie. any other
                          message types only go to STDERR).
        If the special value 'all' is used: then all messages (ERROR/WARN/INFO
                          and ALL other types) go to syslog.

-reset  Resets all debug trace groups to off.

-setup  Used internally when itk is upgraded or installed. Initially sets up the
        trace constant file.
        
-c		Over-ride location of trace.conf file

-v      Enable verbose printouts

-h      Print this help.

Note that all matches are case insensitive.

_EOT_
	exit;
}

#-------------------------------------------------------------------------------
# MAIN
#-------------------------------------------------------------------------------

GetOptions(
	'h|help' 	=> \$help,
	'setup' 	=> \$setup,
	'reset'		=> \$reset,
	'enable|e=s'	=> \@enable,
	'disable|d=s'	=> \@disable,
	'status|s:s'	=> \$status,
	'stacktrace|t:s' => \$stacktrace,
	'c=s'	         => \$configFile,
	'syslog|y:s'     => \$syslog,
	'v'		 => \$verbose,
);
print_usage if $help;
die "$Script: Nothing to do\n" unless ($setup or $reset or @enable or @disable or defined $status or defined $stacktrace or defined $syslog);

print "Libs:\n\t",join("\n\t",@itk_libs),"\n\nConfig:\n\t$configFile\n\n" if $verbose;

load_constants() unless $reset;
find_packages(@itk_libs);

if ($setup) {
	save_constants();
	exit;
}

if (@enable) {
	foreach my $spec (@enable) {
		# [<class_spec>=]group(s)
		my ($pkg_match,$groups) = ($spec =~ /(?:([^=]+)=)?(\w+(?:,\w+)*)/);
		die "$Script: Invalid trace specifier: $spec\n" unless defined $groups;
		my @groups = ($groups eq 'all') ? @trace_groups : split ',',uc $groups;
		foreach (@groups) {
			$_="DEBUG$_" if /^[\d]$/; # Allow shorthand: 1 = DEBUG1, etc
			die "$Script: Invalid trace group: $_\n" unless exists $trace_group_index{$_};
		} 
		my $pkg_match_re = qr/$pkg_match/i if defined $pkg_match;
		foreach my $pkg (sort keys %packages) {
			next if (defined $pkg_match && $pkg !~ $pkg_match_re);
			$changed=1;
			foreach my $grp (@groups) {
				$packages{$pkg}[$trace_group_index{$grp}]=1;
			}
			if ($verbose) {
				my $enabled = join ',',@groups;
				printf "ENABLED: %-55s %s\n",$pkg, $enabled;
			}
		}
	}
}

if (@disable) {
	foreach my $spec (@disable) {
		# [<class_spec>=]group(s)
		my ($pkg_match,$groups) = ($spec =~ /(?:([^=]+)=)?(\w+(?:,\w+)*)/);
		die "$Script: Invalid trace specifier: $spec\n" unless defined $groups;
		my @groups = ($groups eq 'all') ? @trace_groups : split ',',uc $groups;
		foreach (@groups) { 
			$_="DEBUG$_" if /^[\d]$/; # Allow shorthand: 1 = DEBUG1, etc
			die "$Script: Invalid trace group: $_\n" unless exists $trace_group_index{$_};
		} 
		my $pkg_match_re = qr/$pkg_match/i if defined $pkg_match;
		foreach my $pkg (sort keys %packages) {
			next if (defined $pkg_match && $pkg !~ $pkg_match_re);
			$changed=1;
			foreach my $grp (@groups) {
				$packages{$pkg}[$trace_group_index{$grp}]=0;
			}
			if ($verbose) {
				my $disabled = join ',',@groups;
				printf "DISABLED: %-55s %s\n",$pkg, $disabled;
			}
		}
	}
}

if (defined $stacktrace) {
	if ($stacktrace =~ /^on|enabled|true|1$/i) {
		$stacktrace_state=1;
	} elsif ($stacktrace =~ /^off|disabled|false|0$/i) {
		$stacktrace_state=0;
	} elsif ($stacktrace =~ /^eval$/i) {
		$stacktrace_state=2;
	} elsif ($stacktrace eq '') {
		# Toggle current state
		$stacktrace_state = $stacktrace_state>0 ? 0 : 1;
	} else {
		die "$Script: Invalid stack trace specifier: $stacktrace\n";
	}
	$changed=1;
	print "Stack traces are: ",('DISABLED','ENABLED','ENABLED_EVAL')[$stacktrace_state],"\n" if $verbose;
}

if (defined $syslog) {
	if ($syslog =~ /^on|enabled|true|1$/i) {
		$syslog_state=1;
	} elsif ($syslog =~ /^off|disabled|false|0$/i) {
		$syslog_state=0;
	} elsif ($syslog =~ /^all$/i) {
		$syslog_state=2;
	} elsif ($syslog eq '') {
	# Toggle current state
		$syslog_state = $syslog_state>0 ? 0 : 1;
	} else {
		die "$Script: Invalid syslog specifier: $syslog\n";
	}
	$changed=1;
	print "Syslog value is: ",('DISABLED','ENABLED','ENABLED_ALL')[$syslog_state],"\n" if $verbose;
}

if (defined $status) {
	my $pkg_match_re = qr/$status/i if $status;
	       #Itk::EventData::moShellInstallAndUpgradeDataSource::    DEBUG1,DEBUG2,DEBUG3,DEBUG4,DEBUG5,DEBUG6,DEBUG7,DEBUG8,DEBUG9,ENTER
	print "Package                                                 Enabled Groups\n";
	print '-'x125,"\n";
	foreach my $pkg (sort keys %packages) {
		next if (defined $pkg_match_re && $pkg !~ $pkg_match_re);
		my $enabled = join ',',grep $packages{$pkg}[$trace_group_index{$_}], @trace_groups;
		printf "%-55s %s\n",$pkg, $enabled;
	}
	print "\nStack traces are: ",('DISABLED','ENABLED','ENABLED_EVAL')[$stacktrace_state],"\n";
}

save_constants() if ($changed or $reset);

