#!/usr/bin/perl
#------------------------------------------------------------------------------
#
# � Ericsson AB 2006 - All Rights Reserved
#
# No part of this material may be reproduced in any form without the written
# permission of the copyright owner. The contents are subject to revision without
# notice due to continued progress in methodology, design and manufacturing.
# Ericsson shall have no liability for any error or damage of any kind resulting
# from the use of these documents.
#
# Ericsson is the trademark or registered trademark of Telefonaktiebolaget LM
# Ericsson. All other trademarks mentioned herein are the property of their
# respective owners.
#------------------------------------------------------------------------------
# $Id: createhistoricaldb.pl 4443 2011-02-08 04:09:16Z eanzmark $

use strict;
use FindBin qw($Bin);
use lib "$Bin/../lib/";
use Storable;
use File::Path;
use Itk::NitsPackServer;
use Itk::CollectionHistory;
use Getopt::Long;

our $srv;
$srv = new Itk::NitsPackServer(config => "$Bin/../etc/itk.conf");

# Spool Directory
my $spooldir = "$ENV{HOME}/spool/pmxml";
my $dbfilename = "itkcollectdb";
my $storedir = "$ENV{HOME}/logstore/pm";

our ($debug,$debug2);

my $help;

sub usage {
	$"=',';
	print STDERR <<_EOF_;

createhistoricaldb [-h] [-spooldir <spooldir>] [-storedir <storedir>] [-dbfile] 
                   
small utility to create the historycal database file in order to replace 
xmlcollect.
                   
-h         : Show help
-spooldir  : xmlcollect spool directory 
             (default: $spooldir)
-storedir  : Store directory for the db file name
             (default: $storedir)
-dbfile    : name of the historical database 
             (default: $dbfilename)
_EOF_
	exit;
}

GetOptions(
    'h|help' 		=> \$help,
	'spooldir=s'	=> \$spooldir,
	'storedir=s'    => \$storedir,
	'dbfile=s'      => \$dbfilename
	
) or usage();

usage() if $help;

die "$spooldir does not exist" unless -d $spooldir;
mkpath $storedir unless -d $storedir;

my @files = `ls $spooldir/filelist_*.dat`;
my $numfiles = scalar @files;
my $done=0;
foreach my $filelist_file (@files) {
	chomp $filelist_file;
	$done++;
	
	my ($node) = ($filelist_file =~ /filelist_(.+)\.dat$/);
	unless ($node) {
		warn "Could not get nodename for '$filelist_file'\n";
		next;
	}
	print "Processing node : $node ($done / $numfiles)\r";
	my $filelist_file="$spooldir/filelist_${node}.dat";
	my $filelist = undef;
	if (-e $filelist_file) {
		$filelist = retrieve($filelist_file);
		
		my $collectionHistory = new Itk::CollectionHistory(node=>$node,
	                           storedir => $storedir,
		  					   dbfilename => $dbfilename);

		$collectionHistory->put("PMXML", $filelist);
		$collectionHistory->commit ();
	}
}

