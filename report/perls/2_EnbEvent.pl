#!/usr/bin/perl
##
###
### Author: eqsvimp    Eric Yin Z
### $Id: itkreport 9360 2014-11-04 05:06:20Z eqsvimp $
##
### GSC CHINA Tool & Data Centre Service
### 
#
use FindBin qw($Bin);
use lib "../../../lib/";
use lib "$Bin/../lib/";
use Itk::Utils::LoginSession qw(eca_login node_login);
use Parallel::ForkManager;
use Itk::NitsPackServer;
use DBI;
use warnings;

#global variable
our $srv;
our %info;
my $dir = "/home/eqsvimp/healthcheck/report/spool/";
$maxProc = 5;
# multiple nodes
@keynodelist = ("enb_730041","enb_730181","enb_730071","enb_730241","enb_731213","enb_731871");

$configFile ||= "$Bin/../etc/itk.conf";
$srv=new Itk::NitsPackServer(config => $configFile);

use constant TIMEOUT => {
        'PMXML'         =>      {MAX_PROC_TIME  => 600,  TIMEOUT_POLL_INTERVAL=>3},
        'GPEH'          =>      {MAX_PROC_TIME  => 1200,TIMEOUT_POLL_INTERVAL=>60},
        'OSS'           =>      {MAX_PROC_TIME  => 2400,TIMEOUT_POLL_INTERVAL=>60},
};

my %ecainfo = (  'user'      => "eca",  
                 'password'  => "ecastm",  
                 'port'      => 30023,
                 'cliPrompt' => "\@",
		         'host'      => "localhost",
  				 'timeout'   => 1000
);

my %ossinfo = (  'user'      => "",  
                 'password'  => "",  
                 'cliPrompt' => "\@ocsuas3>",
                 'timeout'   => 1000,                 
		         'host'      => ""
  
);

my ($nodesStarted,$nodesComplete,%childStartTimes)=();
($nodesStarted,$nodesComplete) = (0,0);
my $totalNodes = scalar @keynodelist;
my $pm = new Parallel::ForkManager($maxProc);
$pm->run_on_start(
        sub {
                my ($pid,$nodeId)=@_;
                $nodesStarted+=1;
                $childStartTimes{$pid}=time;
                my $nodesRemaining = $totalNodes-$nodesComplete;
                $srv->logInfo("Processing started for '$nodeId', Process ID '$pid' : total($totalNodes), running($nodesStarted), complete($nodesComplete), remaining($nodesRemaining) ");

        }
);

$pm->run_on_finish(
        sub {
                my ($pid, $exit_code, $nodeId, $exitSig) = @_;
                $nodesStarted-=1;
                $nodesComplete+=1;
                my $nodesRemaining = $totalNodes-$nodesComplete;
                if ($exitSig) {
                        $srv->logError("Child process terminated on signal $exitSig");
                }
                my $runTime = time() - $childStartTimes{$pid};
                $srv->logInfo("Processing ended for '$nodeId', Process ID '$pid' : total($totalNodes), running($nodesStarted), complete($nodesComplete), remaining($nodesRemaining) : runtime $runTime ");
                delete $childStartTimes{$pid};
        }
);

# To guard against hanging processes, we keep track of how long each child has
# been running and kill any that take longer than MAX_PROC_TIME
$SIG{ALRM}=sub {
        my $t=time;
        if (DEBUG3) {
                my @lines = map sprintf("\t$_ => %d s",$t - $childStartTimes{$_}), keys %childStartTimes;
                $srv->logInfo("Timeout Check: current process run times:\n".join("\n",@lines));
        }
        foreach my $pid (keys %childStartTimes) {
                if ($t - $childStartTimes{$pid} > TIMEOUT->{PMXML}->{MAX_PROC_TIME}) {
                        $srv->logInfo("Process pid=$pid exceeded MAX_PROC_TIME - killing it");
                        kill 15, $pid;
                }
        }
        alarm TIMEOUT->{PMXML}->{TIMEOUT_POLL_INTERVAL};
};
alarm TIMEOUT->{PMXML}->{TIMEOUT_POLL_INTERVAL};

########Mutiple processes start here
my $pid;
foreach my $nodename (@keynodelist) {
	$srv->logInfo("Start collection for $nodename"); 
#	$pid = $pm->start($nodename) and next;
	task_start($nodename,$dir);
#	$pm->finish;
}

	$pm->wait_all_children;
	
	my $totalRunTime = time() - $^T;
	$srv->logInfo("Processed $totalNodes nodes in $totalRunTime seconds.\n");

sub task_start
{
	my ($nodename,$dir) = @_;
	my $session = new Itk::Utils::LoginSession();
	$session->setField(%ecainfo);
	$session->ecaLogin();

#######get OSS password
	$session->{cliPrompt}= "\@eca";
	$read = $session->commandCommit('ecaadmin config -get server.hostTable -nwid stmsg | grep -A 10 oss_wran');
#eca ~ \$ \e[0mecaadmin config -get server.hostTable -nwid stmsg | grep -A 10 oss_w \cMran | grep password\cM\cJ\cI       
# password => 'CNYfeb\@16',\cM\cJ\e[1;31meca\e[33m
    %ossinfo = getOSSinfo($read);
	$session->setField(%ossinfo);
	$session->nodeLogin();

#######login the node
	$session->{cliPrompt}= "ENB.*>";
	$session->commandCommit("amos $nodename");
	$session->{cliPrompt}= "Password";
	$session->commandCommit($command);
	$session->{cliPrompt}= "ENB.*>";
	$read = $session->commandCommit("rbs\r");
	
##########command 
	$command = "lga -m 7d";
	$description = "Alarm & Event History";
	@res = captureFromTo($read,"HEAD","TAIL");
	my($critical,$major,$minor,$warnings) = (scalar(grep {/.*\s+C\s+.*/} @res),scalar(grep {/.*\s+M\s+.*/} @res),scalar(grep {/.*\s+m\s+.*/} @res),scalar(grep {/.*\s+w\s+.*/} @res));
	#write $info{$nodename}{comments} and $info{$nodename}{status}
	$info{$nodename}{$description}{comments} = "Critical:- $critical,Major:- $major,Minor:- $minor,Warning:- $warnings";
	if($critical >= 0){$info{$nodename}{$description}{status} = "Critical";}
	elsif($major >= 0){$info{$nodename}{$description}{status} = "Major";}
	elsif($minor >= 0){$info{$nodename}{$description}{status} = "Minor";}
	elsif($warnings >= 0){$info{$nodename}{$description}{status} = "Warning";}
#push @tmp , grep {/.*\s+M\s+.*w/} @res;
	writeCSV($dir,$nodename,$description,%info);

##########command 

##########command 

####
###exit session####
	close FH;
	$session->{cliPrompt} = $ossinfo{cliPrompt};
	$session->commandCommit("exit");
	$session->{cliPrompt} = $ecainfo{cliPrompt};
	$session->{timeout}= 1;
	$session->commandCommit("exit");
	$session->{cliPrompt} = "localhost";
	$session->commandCommit("exit");
	$srv->logInfo("$nodename => $command => comments :".$info{$nodename}{$description}{comments});
	$srv->logInfo("$nodename collection finished "); 


}

sub writeCSV
{
	my ($dir,$nodename,$description,%info) = @_;
	$srv->logInfo("write file >> $dir$nodename"); 
	open(FH, ">>$dir$nodename");
	print FH "$nodename,$description,$info{$nodename}{$description}{status},$info{$nodename}{$description}{comments}\n";

}

sub getOSSinfo
{
    my $read = shift;
    my ($username,$password,$hostaddress,%ossinfo);
    if($read =~ /.*username.*\>\s'(.*)'/) { $username = $1;}  #'CNYfeb@16',
	if($read =~ /.*password.*\>\s'(.*)'/) { $password = $1;}
	if($read =~ /.*hostaddress.*\>\s'(.*)'/) { $hostaddress = $1;}
	($ossinfo{user},$ossinfo{password},$ossinfo{host}) = ($username,$password,$hostaddress);
    
    return %ossinfo;
}

sub captureFromTo
{
    my ($read,$from,$to) = @_;
    my @result;
    my $count = 0;
    my @rows =  split '\cM\cJ', $read;
    my ($start,$end) = (0,$#rows);
    foreach my $row (@rows)
   {
  if($row =~ /$from/)
        {
          $start = $count;
          $from = "&$^&";
          next;
        }
        if($row =~ /$to/)
        {
          $end = $count;
          last;
        }
        $count++;
   }    
    for ($start..$end){ push @result, $rows[$_];} 
    return @result;
}


