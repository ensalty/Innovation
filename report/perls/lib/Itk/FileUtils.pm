###----------------------------------------------------------------------------
#
# ITK General purpose utility functions
# Provide various simple functions that would otherwise have to be repeated
# in each library, or result in a new dependancy to an external module.
#
# (c) Ericsson AB 2006 - All Rights Reserved
#
# No part of this material may be reproduced in any form
# without the written permission of the copyright owner.
# The contents are subject to revision without notice due 
# to continued progress in methodology, design and manufacturing. 
# Ericsson shall have no liability for any error or damage of any
# kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of
# Telefonaktiebolaget LM Ericsson. All other trademarks mentioned
# herein are the property of their respective owners. 
# 
#------------------------------------------------------------------------------
#
# Author: lmcmarl
# $Id: 
package Itk::FileUtils;

use strict;
use File::Copy;

our $srv;  #reference to global server object
*srv=\$main::srv;

##-----------------------------------------------------------------------------
# getDiskSpace($partition)
# - disk partition
# RETURNS:  
# - hash with the following structure:
#       size     Total diskspace
#       sizeUnit M or G 
#       used     disk space used
#       usedUnit M or G
#       percent  percentage of disk Space used.
#------------------------------------------------------------------------------
sub getDiskSpace {
	my ($partition, $unit)=@_;  
	
    my @df = `/bin/df -k $partition`;

    my ($size,$sizeUnit,$used,$usedUnit,$avail,$availUnit, $precentUsed);

    my %retValue;
	foreach my $df (@df) {
		
		chop ($df);
		$srv->logDebug(1, " >$df< ") if DEBUG1;
    	if ($df =~ /\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)%.*/) {
    		
        	$size = $1;
        	$sizeUnit = "k";
        	$used = $2;
        	$usedUnit = "k";
        	$avail= $3;
        	$availUnit = "k";
        	$precentUsed = $4;
        	last;
    	}
	}
	
	$retValue{size} = $size;
	$retValue{sizeUnit} = $sizeUnit;
	$retValue{used} = $used;
	$retValue{usedUnit} = $usedUnit;
	$retValue{usedUnit} = $usedUnit;
	$retValue{avail} = $avail;
	$retValue{availUnit} = $availUnit;
	$retValue{precentUsed} = $precentUsed;
	
	return %retValue;
}

##-----------------------------------------------------------------------------
# Backup the history file in case of failure
# getDiskSpace($partition)
# - $logstore	The directory where the history file is kept
# - $fileName	the name of the history file
# - $maxKeep	Number of backups to keep (5 by default if not specified)
# RETURNS:  
# - boolean		1 if successful, 0 otherwise
#------------------------------------------------------------------------------
sub backupHistoryFile {
	$srv->logEnter(\@_) if DEBUG;
	my ($logstore, $fileName, $maxKeep) = @_;
	
	unless (defined $logstore && defined $fileName) {
		$srv->logError("Cannot backup history file - folder [$logstore] or filename [$fileName] not supplied");
		return undef;
	}
	# keep 5 be default
	$maxKeep = 5 unless $maxKeep =~ /^\d+$/;
	
	# history filename
	my $filePath = "$logstore/$fileName";
	
	# first check if the current file exists
	unless (-e "$logstore/$fileName") {
		$srv->logInfo("No history file exists, no backup will be made.");
		return 1;
	}
	# cycle the existing backups
	for (my $i = $maxKeep; $i > 0; $i-- ) {
		my $backupName = $filePath.".bak.".$i;
		my $newBackupName = $filePath.".bak.".($i+1);
		if (-e $backupName) {
			# delete the last one and move the others
			if ($i == $maxKeep) {
				$srv->logDebug(5, "Deletig backup '$backupName'.") if DEBUG5;
				unlink $backupName;
			} else {
				$srv->logDebug(5, "Moving '$backupName' to '$newBackupName'.") if DEBUG5;
				rename $backupName, $newBackupName;
			}
		}
	}
	# copy the current database
	my $backupName = $filePath.".bak.1";
	$srv->logDebug(5, "Backing up '$filePath' to '$backupName'.") if DEBUG5;
	unless (copy $filePath, $backupName) {
		$srv->logError("Error backing up the history file '$filePath': $!");
		return undef;
	}
	return 1;
}


1;

