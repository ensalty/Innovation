#!/usr/bin/perl
###----------------------------------------------------------------------------
# Parser/Analyser for GCPU DSP Dumps
#
# (c) Ericsson AB 2007 - All Rights Reserved
#
# No part of this material may be reproduced in any form
# without the written permission of the copyright owner.
# The contents are subject to revision without notice due 
# to continued progress in methodology, design and manufacturing. 
# Ericsson shall have no liability for any error or damage of any
# kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of
# Telefonaktiebolaget LM Ericsson. All other trademarks mentioned
# herein are the property of their respective owners. 
# 
#------------------------------------------------------------------------------
# $Id$

package Itk::EventData::GCPUParser;
use strict;
use Itk::EventData::EventRec;
use Time::Local qw(timegm_nocheck);
use Hash::Util qw(lock_keys);


BEGIN {
	if (DEBUG6) {
		require Data::Dumper;
		import Data::Dumper;
	}
}

#==============================================================================
# CONSTANTS
#==============================================================================
use constant {
	OK => 1,			# Operation succeeded
	PARSE_FAILED => -1,		# Parsing failed
	OK_MISSEDDATA => 2,		# Operation succeeded but there was some missing data
};
#==============================================================================
# INSTANCE VARIABLES
#==============================================================================
use fields (
	'nodename',	# Name of currently processed node
	'avlogparser',	# Instance of av log parser
	'dumpdir',	# Directory into which completed ecda's should be placed
);
#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $srv;
*srv=\$main::srv;
#==============================================================================
# CLASS METHODS
#==============================================================================

#==============================================================================
# PRIVATE METHODS
#==============================================================================
##-----------------------------------------------------------------------------
# _warn($msg)
# Issue warning, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _warn {
	$srv->logWarn("($_[0]->{nodename}) $_[1]");
}

##-----------------------------------------------------------------------------
# _error($msg)
# Issue Error, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _error {
	$srv->logError("($_[0]->{nodename}) $_[1]");
}

#==============================================================================
# PUBLIC METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# new(%opts)
# Constructor
# % nodename	Name of node
# % dumpdir	Directory into which ECDA dumps will be moved after parsing
# % avlogparser Instance of AV Log Parser, if present then it will be used to
#               determine the running sw package and cv at the time of restart
#------------------------------------------------------------------------------
sub new {
	$srv->logEnter(\@_) if ENTER;
	my ($self, %opts)=@_;
	lock_keys(%opts, qw(nodename dumpdir avlogparser));
	$self=fields::new($self) unless ref $self;
	$self->{nodename}=$opts{nodename};
	$self->{dumpdir}=$opts{dumpdir};
	$self->{avlogparser}=$opts{avlogparser};
	return $self;
}

##-----------------------------------------------------------------------------
# parse($file, $callback)
# Parses the indicated avlog file
# - $file	Filename of syslog file to parse
# - $callback	Call back sub to recieve results
# RETURNS: $resultcode
# $resultcode	Result of parsing
#------------------------------------------------------------------------------
sub parse {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $file, $callback)=@_;
	
	$srv->logDebug(4, "Start parsing $file") if DEBUG4;
	
	my (
		$line,
		$ts,
	);
	
	# open the file
	my $fh;
	
	if ($file =~ /(.*)\.gz/) {
		open $fh, "gzip -dc $file |";
		if (! $fh || eof($fh)) {
			$self->_error("Failed to open '$file' (gunzipped): $!");
			return PARSE_FAILED;
		}
	} else {
		open $fh, $file or do {
			$self->_error("Failed to open '$file': $!");
			return PARSE_FAILED;
		};
	}
	
	my $rec = {
		file => $file
	};
	
	# Slot information should be available in the filename
	# Format: XXXX_c_logfiles_dspdumps_00_01_GCPUdump03-83.gz
	my ($subrack,$slotnr);
	if (($subrack,$slotnr) = ($file =~ /(\d+)_(\d+)_GCPUdump/)) {
		$rec->{slot} = "${subrack}${slotnr}00";
	} else {
		$rec->{slot} = "UNKNOWN";
	}
	
	# loop in the file
	while (<$fh>) {

		if ($line =~ /^Product number:\s*(.*)/) {
			$rec->{product} = $1;
			next;
		}

		if ($line =~ /^Product revision:\s*(.*)/) {
			$rec->{revision} = $1;
			next;
		}
		
		if (/^\[(\d+-\d+-\d+ \d+:\d+:\d+).+INFO:\[ULMA(\d+)] getFaultDesFromCm = (.+)$/) {
			$rec->{time} = $1;
			$rec->{snid} = $2;
			$rec->{errmsg} = $3;
			next;
		}
		
	}
	
	close $fh;
	
	# In ENB, whenever a baseband crash occurs, all DSPs are dumped
	# If the error message above was not found, then it was probably just
	# 'collateral damage' - ie. the real crash was in another DSP.
	# Only proceeed if an error message was found
	if ($rec->{errmsg}) {
		
		my $outRec;
		if ($outRec = $self->_validateRecord ($rec)) {
			&$callback($outRec);
		} else {
			$srv->logDebug(4, "Result of parsing $file : FAILED ") if DEBUG4;
			return PARSE_FAILED;	
		}
		
	}
		
	$srv->logDebug(4, "Result of parsing $file : OK") if DEBUG4;
	return OK;

}

#-----------------------------------------------------------------------------
# _validateRecord($eventrecord)
# Analyses the given events, calls the callback if the 
# RETURNS: $outrec
# - $outrec return the output record
#------------------------------------------------------------------------------
sub _validateRecord () {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $eventrecord)=@_;
		
	# fix me: 
	# put some validation here to discard record that we don't want.

	# get the file name. We store the basename of the file
	# (Without directories)
	my ($file) = ($eventrecord->{file} =~ /([^\/]+)$/);
    
	# build date with date & time 
	my ($yy,$mm,$dd,$h,$m,$s) = ($eventrecord->{time} =~ /(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})/) or do {
		$self->_error ("Invalid Time: $eventrecord->{time} in file: $file ");
		return undef;
	};
	
	my $ts;
	eval {
		$ts=timegm_nocheck($s,$m,$h,$dd,$mm-1,$yy);
	};
	if ($@) {
		$self->_error ("Event Timestamp '$eventrecord->{time} is invalid in file: $file");
		return undef;
	}
	
	my ($up, $cv);
	if ($self->{avlogparser}) {
		$srv->logDebug(4, " up ($up) and cv ($cv) taken from avglogparser " ) if DEBUG4;
		($up, $cv) = $self->{avlogparser}->getRunningUp($ts);
	} 
	
	##
	# if  $up or $cv not defined 
	# take it from history from the module directly.
	##
	if (!defined ($up) || !defined ($cv)) {
		my $sdata = $self->{avlogparser}->getStateData ();
		$cv = $sdata->{currentCv};
		$up = $sdata->{currentUp};
		$srv->logDebug(4, " up ($up) and cv ($cv) taken from history " ) if DEBUG4;	
	}
	

	# build the record
	my $rec = new Itk::EventData::EventRec::Restart(
		time		=> $ts,
		nodename	=> $self->{nodename},
		#nodetype	=> 
		#rncparent	=> 
		restarttype	=> 'auto(dsp)',
		#reason		=> 
		infotext	=> $eventrecord->{errmsg},
		scope		=> "DSP",
		slot		=> $eventrecord->{slot},
		#piutype	=> 
		pmdid		=> $file,
		#process		=> 
		#errorno		=> 
		#extra		=> 
		block		=> "$eventrecord->{product}_$eventrecord->{revision}",
		#stacktrace	=> 
		#productnumber	=> 
		#productrevision=> 
		#productdate	=> 
		#serialnumber	=> 
		sw		=> $up,
		cv		=> $cv,
		#newsw		=> 
		#newcv		=> 
		#cellodt	=> 
		#appdt		=> 
	);
    
	#return the record       
	return $rec;
}	
	

##-----------------------------------------------------------------------------
# parseFiles($fileList, $callback, %opts)
# Parses one or more avlog files, returning event records to the callback sub as
# they are found. The last processed record is stored separately for each file
# - $fileList	Ref to array containing local filenames of syslog files
# - $callback 	Ref to callback sub
# % pmddelivery	Specifies method for PMD delivery. If false or not present then
#           legacy delivery of PMDs applies. If true then they are moved into the
# 			dumpdir and the callback is called with the name as the first argument.
# RETURNS: $resultcode
# - $resultCode	  Result Code - see constants for definitions
#------------------------------------------------------------------------------
sub parseFiles {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $fileList, $callback, %opts)=@_;

	my (
		$rcode,		# Result code
	);

	#
	# parse all the file
	foreach my $file (@{$fileList}) {
		
		$srv->logDebug(4, "Parsing file : $file ");

		my $destFile;		
		if ($self->{dumpdir}) {
			($destFile) = ($file =~ /([^\/]+)$/);
			unless ($destFile) {
				$srv->logError("Could not derive ECDA file name");
				next;
			}
			$destFile="$self->{dumpdir}/$self->{nodename}_$destFile";
			rename $file, $destFile or do {
				$srv->logWarn("Problem renaming '$file' to '$destFile': $!");
				next;
			};
			&$callback($destFile) if $opts{pmddelivery};
		} else {
			$destFile=$file;
		}
		
		my ($parseRcode) = $self->parse($destFile, $callback);
				
		if ($parseRcode != OK) {
			$self->_warn("Failed parsing file $file, rcode=$rcode");
			$rcode=PARSE_FAILED;
			next;
		}
				
	}
		
	$rcode = OK unless defined $rcode;

	$srv->logDebug(4, "Result of file parsing: rcode=$rcode") if DEBUG4;
	
	return $rcode;
}


##-----------------------------------------------------------------------------
# DESTROY()
# Destructor
#------------------------------------------------------------------------------
sub DESTROY {
	my $self=shift;
	# Do destruction!!
}
#------------------------------------------------------------------------------
1; # Modules always return 1!

