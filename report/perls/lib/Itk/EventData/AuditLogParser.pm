#!/usr/bin/perl
###----------------------------------------------------------------------------
# Parser/Analyser for Cello Availability Log files
#
# (c) Ericsson AB 2007 - All Rights Reserved
#
# No part of this material may be reproduced in any form
# without the written permission of the copyright owner.
# The contents are subject to revision without notice due 
# to continued progress in methodology, design and manufacturing. 
# Ericsson shall have no liability for any error or damage of any
# kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of
# Telefonaktiebolaget LM Ericsson. All other trademarks mentioned
# herein are the property of their respective owners. 
# 
#------------------------------------------------------------------------------
# Author: Mario Landreville
# $Id$

package Itk::EventData::AuditLogParser;
use strict;
use Itk::EventData::EventRec;
use Time::Local qw(timegm_nocheck);
use Hash::Util qw(lock_keys);
use Data::Dumper;

BEGIN {
	if (DEBUG6) {
		require Data::Dumper;
		import Data::Dumper;
	}
}

#==============================================================================
# CONSTANTS
#==============================================================================
use constant {
	OK => 1,			                  # Operation succeeded
	PARSE_FAILED => -1,		              # Parsing failed
	OK_MISSEDDATA => 2,		              # Operation succeeded but there was some missing data
};
#==============================================================================
# INSTANCE VARIABLES
#==============================================================================
use fields (
	'collHist',		# Ref to collection history object
	'nodename',
	'nodetype',
	'startdate',		# Timestamp (unix epoch) of start time filter
	'record',
	'audits',
);

#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $srv;
*srv=\$main::srv;
#==============================================================================
# CLASS METHODS
#==============================================================================

#==============================================================================
# PRIVATE METHODS
#==============================================================================
##-----------------------------------------------------------------------------
# _warn($msg)
# Issue warning, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _warn {
	$srv->logWarn("($_[0]->{nodename}) $_[1]",(caller(0)));
}

##-----------------------------------------------------------------------------
# _error($msg)
# Issue Error, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _error {
	$srv->logError("($_[0]->{nodename}) $_[1]", (caller(0)));
}

#==============================================================================
# PUBLIC METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# new($collHist, %opts)
# Constructor
# - $collHist	Ref to instance of collection history
# % nodename	Name of node
# % nodetype	Type of node
# % startdate 	Starting date as a unix epoch timestamp; PMD files earlier than this will not be fetched
#------------------------------------------------------------------------------
sub new {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $collHist, %opts)=@_;
	$self=fields::new($self) unless ref $self;
	$self->{collHist}=$collHist;
	$self->{nodename}=$opts{nodename};
	$self->{nodetype}=$opts{nodetype};
	$self->{startdate}=$opts{startdate};
	$self->{record}=$opts{record};
	$self->{audits}=[];
	return $self;
}

##-----------------------------------------------------------------------------
# parse($file, $callback, $pmdParser, $syslogParser, $lastrecord, %opts)
# Parses the indicated avlog file
# - $file	Filename of syslog file to parse
# - $callback	Call back sub to recieve results
# - $lastrecord	Record ID of the last processed record. If this is not undef,
#		then we skip forward through the file until we find a matching
#		record; otherwise we just process all records in the file
# RETURNS: $resultcode, $lastrecord, $missingRecs
# - $resultCode	  Result Code - see constants for definitions
# - $lastrecord   Record ID of the last processed record
# - $missingRecs  Flag: if true, we could not find lastrecord so some records have been missed.
# - $startTime    Starting time for the time window covered by this parse (epoch secs)
# - $endTime      Ending time for the time window covered by this parse (epoch secs)
#------------------------------------------------------------------------------
sub parse {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $file, $callback, $lastrecord)=@_;

	
	my $parseErr = sub {
		$self->_error("Parse Error: $file:$. $_[0]");
	};
	
			
	my (
		$auditevents,  # Array into which found audit entries are placed for later analysis
		$maxlogrec,	# Ending log record number processed
		$minlogrec,	# Starting log record number found
		$startTime,	# Start time for this parse (either time of last processed, or first record found if none)
		$firstTime,	# Timestamp of earliest record in the file
		$endTime,	# Time of last record found in the file
		$ts,        # time stamp variable
		$logRec,     # log record
		$nbStruc,   # struct counter
		$nbArray,   # array counter     
	);
	
	# open the file
	my $fh;
	open $fh, $file or do {
		$self->_error("Failed to open '$file': $!");
		return PARSE_FAILED;
	};
	
	
	
	# initialize the array of events	
	$auditevents = [];
	$ts = undef;
	
	# lookup for month
	my $monthLookup = { Jan=>"0",Feb=>"1",Mar=>"2",Apr=>"3",
 	                    May=>"4",Jun=>"5",Jul=>"6",Aug=>"7", 
	                    Sep=>"8",Oct=>"9",Nov=>"10",Dec=>"11"};

	
	LOGRECORD:
	
	# loop in the file
	while (<$fh>) {

		## logRec number
		$logRec=undef;
		
		$nbStruc=undef;
		$nbArray=undef;
		
		# get the time stamp of the file
		if (! defined $ts && /<TimeStamp>/) {
			$_=<$fh>;
			my ($yy,$mm,$dd)=(/^\s*<year>\s*(\d+)\s*<\/year>\s*<month>\s*(\d+)\s*<\/month>\s*<day>\s*(\d+)\s*<\/day>/) or do {
				&$parseErr("Expected year,month & day");
				next LOGRECORD;
			};
			$_=<$fh>;
			my ($h,$m,$s)=(/^\s*<hour>\s*(\d+)\s*<\/hour>\s*<minute>\s*(\d+)\s*<\/minute>\s*<second>\s*(\d+)\s*<\/second>/) or do {
				&$parseErr("Expected hour/ minute & second");
				next LOGRECORD;
			};
			$_=<$fh>;
			/^\s*<\/TimeStamp>/ or do {
				&$parseErr("Expected: </TimeStamp>");
				next LOGRECORD;
			};
			
			eval {
				$ts=timegm_nocheck($s,$m,$h,$dd,$mm-1,$yy);
			};
			if ($@) {
				&$parseErr("Invalid Timestamp: $yy/$mm/$dd $h:$m:$s");
				next LOGRECORD;
			};
					
			if ($ts > 79833599) { # Skip the next part if time is in 1970
				if (defined $lastrecord) {
					# Starting time for this parse is actually the time of the last record
					# we parsed the last time (if any)
					$startTime = $ts if $logRec == $lastrecord;
				}
				$firstTime=$ts unless defined $firstTime and $firstTime < $ts;
				$endTime=$ts unless defined $endTime and $endTime > $ts;
			}

            $_=<$fh>;              			
		} ## time stamp


		## log record		
		if (/^\s*<LogRecord number = "(\d+)">/) {
			my $logRec=$1;
			
		   next LOGRECORD if (defined $lastrecord && $logRec <= $lastrecord);
		   $maxlogrec = $logRec unless defined $maxlogrec && $logRec < $maxlogrec;
		   $minlogrec = $logRec unless defined $minlogrec && $logRec > $minlogrec;

       			
			while (<$fh>) {
				
				# start a new record.
				my $auditRecord = {};
				$auditRecord->{ts} = $ts;
				$auditRecord->{no} = $logRec;


				## for the shell audit record, there is a timestemp par record.
			    if ($self->{record} eq 'shellauditdata') {

		           if (/<TimeStamp>/) {
			          $_=<$fh>;
			          my ($yy,$mm,$dd)=(/^\s*<year>\s*(\d+)\s*<\/year>\s*<month>\s*(\d+)\s*<\/month>\s*<day>\s*(\d+)\s*<\/day>/) or do {
				         &$parseErr("Expected year,month & day");
				          next LOGRECORD;
			          };
			       
			          $_=<$fh>;
			          my ($h,$m,$s)=(/^\s*<hour>\s*(\d+)\s*<\/hour>\s*<minute>\s*(\d+)\s*<\/minute>\s*<second>\s*(\d+)\s*<\/second>/) or do {
				         &$parseErr("Expected hour/ minute & second");
				         next LOGRECORD;
			          };
			       
			          $_=<$fh>;
			          /^\s*<\/TimeStamp>/ or do {
				         &$parseErr("Expected: </TimeStamp>");
				         next LOGRECORD;
			          };
			
			          eval {
				         $ts=timegm_nocheck($s,$m,$h,$dd,$mm-1,$yy);
			          };
			       
			          if ($@) {
				         &$parseErr("Invalid Timestamp: $yy/$mm/$dd $h:$m:$s");
				         next LOGRECORD;
			          };
					
			       } 
			       
			       # assign the time
			       $auditRecord->{ts} = $ts;
			       $_=<$fh>;
		        } ## SHELL_AUDIT_DATA

				
				# get the record
				if (/^\s*<RecordContent>/) {
					
					while (<$fh>) {
						
						my $line = $_;
						
						# get out of the loop if end of record.
						if ($line =~ /<\/RecordContent>/) {
							last;			
						}
						
						if ($self->{record} eq 'shellauditdata') {
						
						   if ($line=~ /<User> (.*) <\/User> <Termname> (.*) <\/Termname>/ )  {
							  $auditRecord->{username} = $1;
							  $auditRecord->{terminal} = $2;
						   }
						
						   if ($line=~ /<Event> (.*) <\/Event>/ )  {
							  $auditRecord->{event} = $1;
						   }

						   if ($line=~ /<Info> (.*) <\/Info>/ )  {
							  $auditRecord->{info} = $1;
						   }
				
						   next;		
						}
						
						# get the audit record type if not defined.
						if (! defined ($auditRecord->{type})) {
							if ($line =~ /(SET|DELETE|CREATE|ACTION)/) {
								$srv->logDebug(9, " audit record detected is".$auditRecord->{type}) if DEBUG9;
								$auditRecord->{type} = $1;
								next;
							} 
						}
						
						# get the time of the record
						if ($line =~ /^\/\/\s*Unknown\s*(\w*)\s(\w*)\s*(\w*)\s(\w*)[,]\s(\d+):(\d+):(\d+)$/) { 
							my ($s,$m,$h,$mm,$dd,$yy) = ($7,$6,$5,$monthLookup->{$2},$3,$4);
							eval {
								$ts = timegm_nocheck($s,$m,$h,$dd,$mm,$yy);
							};
							if ($@) {
								&$parseErr("Invalid Timestamp: $yy/$mm/$dd $h:$m:$s");
								next LOGRECORD;
							};
							
							$auditRecord->{ts} = $ts;
							next;
						}
						
						# ignore this line
						if ($line =~ /^\s*returnValue\signore$/) { 
							next;
						}
						
						# ignore this line
						if ($line =~ /^\s*returnValue none$/) {
							next;
						}
						
						# ignore this line
						if ($line =~ /^\s*exception\snone$/) { 
							next;
						}
						
						# ignode this line 
						if ($line =~ /^\)|\($/ ) {
							next;
						}
						
						# get the mo 
						if ($line =~ /^\s*mo\s"(.*)"/) {
							$auditRecord->{mo} = $1;
							$srv->logDebug(9, " audit record mo is".$auditRecord->{mo}) if DEBUG9;
							next;
						}
						# get the struct
						if ($line =~ /^\s(\w*)\s*Struct$/) {
							if (!defined ($nbStruc )) {
								$nbStruc=0;
								$auditRecord->{Struct}=[];
							} else {
								$nbStruc++;
							}
							$auditRecord->{Struct}->[$nbStruc]= {};
							$auditRecord->{Struct}->[$nbStruc]->{name} = $1;
														 
							next;
						}
						# get the action name
						if ($line =~ /^[ ]actionName ([A-Za-z]*)$/) {
							$auditRecord->{actionName} = $1;
							next;
						}
						# get the reference 
						
						
						# get the array definition	
						if ($line =~ /^[ ]*(\w*)\s*Array\s(\w*)\s*(\d*)[ ]$/) {
							if (!defined ($nbArray)) {
								$nbArray=0;
								$auditRecord->{array} = [];
							} else {
								$nbArray++;
							}
							$auditRecord->{array}->[$nbArray] = {};
							$auditRecord->{array}->[$nbArray]->{name} = $1;
							$auditRecord->{array}->[$nbArray]->{type} = $2;
							$auditRecord->{array}->[$nbArray]->{nbElement} = $3;
							$auditRecord->{array}->[$nbArray]->{arrayValues} = [];
							next;
						}
						# get the array reference with no attribute
						if ($line =~ /^[\t]*Array\sReference\s(\d*)$/) {
							$auditRecord->{arrayReference}->{number} = $1;
							$auditRecord->{arrayReference}->{array} = [];									
							next;
							# get the array reference with an attribute
						}
						if ($line =~ /^[ ]*(\w*)\s*Array\sReference\s(\d*)$/) {
							$auditRecord->{arrayReference}->{name} = $1; 
							$auditRecord->{arrayReference}->{number} = $2;
							$auditRecord->{arrayReference}->{array} = [];									
							next;
							# get the reference
						} 
						if ($line =~ /^[ ]Reference\s*\"([a-zA-Z0-9=\-\,]*)\"/) {
							if (defined ($auditRecord->{arrayReference})) {
								my $referenceArray = $auditRecord->{arrayReference}->{array};
								push @$referenceArray, $1;
								next;
							}
							# get the reference
						} 
						if ($line =~ /^[\t]+\"(.*)\"/) {
							if (defined ($auditRecord->{arrayReference}))  {
								my $array = $auditRecord->{arrayReference}->{array};
								push @$array, $1;
								next;
							}
							# get the number of element								
						}
						if ($line =~ /^[\t]+nrOfElements\s(\d*)$/) {
							if (defined ($auditRecord->{Struct}->[$nbStruc])) { 
								$auditRecord->{Struct}->[$nbStruc]->{nbParameter} = $1;
								$auditRecord->{Struct}->[$nbStruc]->{parameters} = [];
								$auditRecord->{Struct}->[$nbStruc]->{filled}=0;
								next;
							} 
							# get the structures's attribute		
						}
						if ($line =~ /^[\t]+(\w*)[ ](\w*)[ ]([\-0-9]*)$/) {
							if (defined ($auditRecord->{Struct}->[$nbStruc])) { 
								my $parameters = $auditRecord->{Struct}->[$nbStruc]->{parameters};
								push @$parameters, [$1,$2,$3];
								next;
							} 
							# get the number attribute
						} 
						if ($line =~ /^\snrOfAttributes\s(\d*)/) {
							$auditRecord->{nbAttribute} = $1;
							$auditRecord->{attributes} = [];
							next;							    
						}
						# get the array's values
						if ($line =~ /^[\t]+([\-]*\d*)$/) {
							# check if array mode
							if( defined $auditRecord->{array}->[$nbArray] ) {
								my $array = $auditRecord->{array}->[$nbArray]->{arrayValues};
								push @$array, $1;
								next;
							}
						}
						
						# get the ation name
						if ($line =~ /^\s*mo\s"(.*)"/) {
							$auditRecord->{mo} = $1;
							next;
						}
						# get the number of parameters
						if ($line =~ /^\s*nrOfParameters\s(\d*)/) {
							$auditRecord->{nbParameter} = $1;
							$srv->logDebug(9, " number of parameter is".$auditRecord->{nbParameter}) if DEBUG9;
							next;
						}
						# get the parent
					   	if ($line =~ /^\s*parent\s"(.*)"/) {
							$auditRecord->{parent} = $1;
							next;
						}
						# get the identity
						if ($line =~ /^\s*identity\s(.*)/) {
							$auditRecord->{identity} = $1;
							next;
						}
						# get the mo type
						if ($line =~ /^\s*moType\s(\w*)/) {
							$auditRecord->{moType} = $1;
							next;
						}
						# get String Parameter Accept all characaters
						if ($line =~ /^\s*(String|Reference)\s\"(.*)\"$/ ) {
							if (defined  $auditRecord->{Struct}->[$nbStruc]->{nbParameter} ) {
								my $parameterName = ' '; 
								my $parameterType = $1;
								my $parameterValue = $2;
							    $srv->logDebug(9, " (String | Reference) parameter detected type:$parameterType Value: $parameterValue") if DEBUG9;
								my $parameters = $auditRecord->{Struct}->[$nbStruc]->{parameters};
								push @$parameters, [$parameterName,$parameterType,$parameterValue];
								next;
							} 
						}

					   	
						# get String of reference Attribute Accept all characaters
						if ($line =~ /^[ ]*(\w*)\s(Reference|String)\s\"(.*)\"$/ ) {
							# get the attribute Array
							my $attributes=[];
							if (defined $auditRecord->{attributes}) { 
								$attributes = $auditRecord->{attributes};
							} 
							
							 $srv->logDebug(9, " regular attribute parameter $1,$2,$3 ") if DEBUG9;
							# store attribute name, type and value 
							push @$attributes, [$1,$2,$3];
							$auditRecord->{attributes} = $attributes;
							next;
						}


						# get the parameters
						if ($line =~ /^[\t]+(\w*)\s([a-zA-Z0-9\.=,]*)/) {
							if (defined  $auditRecord->{Struct}->[$nbStruc]->{nbParameter} ) { 
								my $parameterName = ' '; 
								my $parameterType = $1;
								my $parameterValue = $2;
   							    $srv->logDebug(9, " (start with tab) parameter detected type:$parameterType Value: $parameterValue") if DEBUG9;
								my $parameters = $auditRecord->{Struct}->[$nbStruc]->{parameters};
								push @$parameters, [$parameterName,$parameterType,$parameterValue];								
								next;
							} 
					   	}
						# get the regular attribute
						# start with space with 3 attributes
						if ($line =~ /^[ ]+(\w+)\s(\w+)\s([-a-zA-Z0-9]+)$/) {
							# get the attribute Array
							my $attributes=[];
							if (defined $auditRecord->{attributes}) { 
								$attributes = $auditRecord->{attributes};
							} 
 							  $srv->logDebug(9, " regular attribute 3 parameter $1,$2,$3 ") if DEBUG9;
							# store attribute name, type and value 
							push @$attributes, [$1,$2,$3];
							$auditRecord->{attributes} = $attributes;
							next;
						}

						# start with space with 2 attributes
						if ($line =~ /^[ ]+(\w+)\s([-a-zA-Z0-9]+)$/) {
							# get the attribute Array
							my $attributes=[];
							if (defined $auditRecord->{attributes}) { 
								$attributes = $auditRecord->{attributes};
							} 
 							  $srv->logDebug(9, " regular attribute 2 parameter $1, $2") if DEBUG9;
							# store attribute name, type and value 
							push @$attributes, [undef,$1,$2];
							$auditRecord->{attributes} = $attributes;
							next;
						}
						$srv->logDebug(7, "line ignored (".$self->{nodename}.":>$line<") if DEBUG7;
												
					} # while 
					
					# keep the audit report
					
					$srv->logDebug(6, "Record: ".Dumper ($auditRecord)) if (DEBUG4) ;					
					push @$auditevents, $auditRecord;
					last;
				} # if						
				
			}
		} # processinf a record
	}
	
	close $fh;
	
	my $outrecs = undef;
	if ($self->{record} eq 'auditdata') {
	    $outrecs = $self->_validateEvents($auditevents, $callback);
	} elsif ($self->{record} eq 'shellauditdata') {
		$outrecs = $self->_validateShellEvents($auditevents, $callback);
	} 
	
	my $missingrecs = $minlogrec - $lastrecord;
	
	# If we did not manage to find the lastprocessed record then start time is the earliest record we did find
	$startTime = $firstTime unless defined $startTime;
	
	$srv->logDebug(4, "Result of parsing $file: lastrecno = $lastrecord ") if DEBUG4; 
	$srv->logDebug(4, "minrec = $minlogrec ") if DEBUG4;
	$srv->logDebug(4, "maxrec = $maxlogrec  ") if DEBUG4;
	$srv->logDebug(4, "missingrecs = $missingrecs ") if DEBUG4;
	$srv->logDebug(4, "output records=$outrecs") if DEBUG4;
	

	return OK, $maxlogrec, $missingrecs, $startTime, $endTime;
	
	
	
}


##-----------------------------------------------------------------------------
# _validateShellEvents ($events, $callback, %opts)
# Analyses the given events, calls the callback if the 
# - \@events	AoA containing event data in internal format
# - $callback	Call back sub to recieve results
# RETURNS: $outrecs
# - $outrecs	Number of records that were output
#------------------------------------------------------------------------------
sub _validateShellEvents {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $auditEvents, $callback)=@_;

	my $nodename=$self->{nodename};
	my $nodetype=$self->{nodetype};
	my $startDate=$self->{startdate};
	my $outrecs = 0;				# Number of records output

	# validate each event.
	foreach my $ev (@$auditEvents) {
       
	   $srv->logDebug(6, "get Shell audit record with: nodename: $self->{nodename}, nodetype:$self->{nodetype}, time: $ev->{ts}, User: $ev->{username}, Terminal: $ev->{terminal}, Event: $ev->{event}, info: $ev->{info}");

       # unescape xml Character
       $ev->{info} =~ s/&lt;/</g;
       $ev->{info} =~ s/&gt;/>/g;
       $ev->{info} =~ s/&quot;/"/g;
       $ev->{info} =~ s/&apos;/'/g;
       $ev->{info} =~ s/&amp;/&/g;


       # escape semicolum.
	   $ev->{info} =~ s/;/&#59/g;

	   $srv->logDebug(6, "get Shell audit record with: nodename: $self->{nodename}, nodetype:$self->{nodetype}, time: $ev->{ts}, User: $ev->{username}, Terminal: $ev->{terminal}, Event: $ev->{event}, info: $ev->{info}");

       		            
	   my $rec = new Itk::EventData::EventRec::ShellAudit(
			time => $ev->{ts},
			nodename =>  $self->{nodename},
			nodetype => $self->{nodetype},
			username => $ev->{username},
			terminal => $ev->{terminal},
			event => $ev->{event},     
			info => $ev->{info});
			
       &$callback($rec);            
       $outrecs++;
	}
	return $outrecs;
}


##-----------------------------------------------------------------------------
# _validateEvents($events, $callback, $pmdParser, %opts)
# Analyses the given events, calls the callback if the 
# - \@events	AoA containing event data in internal format
# - $callback	Call back sub to recieve results
# RETURNS: $outrecs
# - $outrecs	Number of records that were output
#------------------------------------------------------------------------------
sub _validateEvents {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $auditEvents, $callback)=@_;

	my $nodename=$self->{nodename};
	my $nodetype=$self->{nodetype};
	my $startDate=$self->{startdate};
	my $outrecs = 0;				# Number of records output
    my $seq = 0;
  
	# validate each event.
	foreach my $ev (@$auditEvents) {

		my ($ts, $evType, $rec, $mo, $attributes, $attributeName, $attributeType, $value);
			
		my @attributes = ();
		
		## check action type
		$mo = undef;
		$attributeName = undef;
		$attributeType = undef;
		$value = undef;
		$evType = undef;
		$ts= undef;
        $seq = $ev->{no};
		$ts = $ev->{ts};


		# get event type
		$evType = $ev->{type};
		
		if (!defined ($evType)) {
			# put error Message
			$srv->logDebug(6, "event type is not defined  ".Dumper($ev)) if DEBUG4;
			goto RECORD;
		}
		
		# get the mo, when the event is create, the mo is build
		if ($evType eq "CREATE") {
			$mo = $ev->{parent}.",".$ev->{moType}."=".$ev->{identity};
			# make a record to create the mo.
			$value = $ev->{moType}."=".$ev->{identity};
			$rec=$self->_getRecord ($seq, $ts, $nodename, $nodetype, $mo, $evType, \@attributes);
		} else {
			$mo = $ev->{mo};
		}

		# if action, get the action name.
		if ($evType eq "ACTION") {
			my $actionName = $ev->{actionName};
			$rec=$self->_getRecord ($seq, $ts, $nodename, $nodetype, $mo, $evType.":".$actionName, \@attributes);
		}

		# process the delete
		if ($evType eq "DELETE") {
    		$rec=$self->_getRecord ($seq, $ts, $nodename, $nodetype, $mo, $evType,  \@attributes);
		}

		# process the delete
		if ($evType eq "SET") {
    		$rec=$self->_getRecord ($seq, $ts, $nodename, $nodetype, $mo, $evType,  \@attributes);
		}

		#--------------------------
		# process Structure
		#--------------------------

		my $strucName = undef;
		# process parameters
		if (defined ($ev->{Struct}) ) {
			foreach my $s (@{$ev->{Struct}}) {
			   $strucName = $s->{name};
			    # for each parameter
			   foreach my $element (@{$s->{parameters}}) {
					$attributeName = $strucName."~".$element->[0];
				#----------------------------------------------------
				# reference doesn't have any value type
				#----------------------------------------------------
					if ($attributeName eq "Reference") {
						$value = $element->[1];					
					} else {
						$attributeType = $element->[1];
						$value = $element->[2];
					}
				}
		  		push (@attributes, {'name'=>$attributeName, 'type'=>$attributeType, 'value'=>$value});	
		  	} ## for each
		}
		
		#-------------------------
		# process the parameter
		#-------------------------
		my $strucName = undef;
		# process parameters
		if (defined ($ev->{parameters}) ) {
			# for each parameter
			foreach my $element (@{$ev->{parameters}}) {
				$attributeName = $element->[0];
				#----------------------------------------------------
				# reference doesn't have any value type
				#----------------------------------------------------
				if ($attributeName eq "Reference") {
					$value = $element->[1];					
				} else {
					$attributeType = $element->[1];
					$value = $element->[2];
				}
		  		push (@attributes, {'name'=>$attributeName, 'type'=>$attributeType, 'value'=>$value});	
			}
		}

		#----------------------
		# process attributes
		#----------------------
		if (defined ($ev->{attributes})) {
			foreach my $element (@{$ev->{attributes}}) {
				$attributeName = @$element[0];
				$attributeType = @$element[1];
				$value = $element->[2];
				#if ( defined ($mo) && defined ($attributeName) && defined ($attributeType) && defined ($value) ) {
			  		push (@attributes, {'name'=>$attributeName, 'type'=>$attributeType, 'value'=>$value});	
				#} else {
				#	$srv->logDebug(6, "can't create a record : mo or attribute name or attribute type or value is missing\n".Dumper($ev)) if DEBUG4;
				#}	
				
			}	
		}
		
		#---------------------
		# process array
		#--------------------
		if (defined ($ev->{array})) {
			foreach my $e (@{$ev->{array}}) {
			   $attributeName = $e->{name};
			   $attributeType = "Array".$e->{type};
			   if (defined ($e->{arrayValues})) {
			   	   $value = join ("~", @{$e->{arrayValues}});
			       if (index ($value,"~") == -1 ) {
				     $value = $value."~";
			       }
			   }
			
			   # create the record if all the field are defined.
			   #if ( defined ($mo) && defined ($attributeName) && defined ($attributeType) && defined ($value) ) {				
		  		push (@attributes, {'name'=>$attributeName, 'type'=>$attributeType, 'value'=>$value});	
			   #} else {
			   #  $srv->logDebug(6, "can't create a record : mo or attribute name or attribute type or value is missing\n".Dumper($ev)) if DEBUG4;
			   #}
			}    
		}
		#--------------------	
		#process references
		#--------------------	    
		if (defined ($ev->{arrayReference})) {
			$attributeName = $ev->{arrayReference}->{name};
			$attributeType = "Reference";
			foreach my $value (@{$ev->{arrayReference}->{array}}) {
		  		push (@attributes, {'name'=>$attributeName, 'type'=>$attributeType, 'value'=>$value});	
			}
		}


		#--------------------
		# call the callback
		#--------------------
        if (defined ($rec)) {
			&$callback($rec);
			$outrecs++;				
		} 
		
		RECORD:
		
	} # foreach record
	
	return $outrecs;
}


##-----------------------------------------------------------------------------
# _getRecord ($time, $nodeName, $nodeType, $mo, $action, $attributeName, $attributeType, $value)
# Get a record instance with the values.
# - $time		Ref to the time
# - $nodeName 	Ref to the node name
# - $nodetype   Ref to the node type
# - $mo			ref to the mo
# - $action     ref to the action
# - $attribute  ref to the attributes
# RETURNS: $resultcode
# - $rec		ref on the record
#------------------------------------------------------------------------------

sub _getRecord {
	my ($self, $seq, $time, $nodeName, $nodeType, $mo, $action, $attributes) = @_;
		
	$srv->logDebug(6, "getRecord with: $time, $nodeName, $nodeType, $mo, $action");
	
	
	if ($mo =~ /^(\w+=\w+),((\w+=[\w-%\.\/]+),)*(\w+=[\w-%\.\/]+){1}$|^(\w+=\w+)$/ ){
	            
		my $rec = new Itk::EventData::EventRec::Audit(
			time => $time,
			id => $seq,
			nodename => $nodeName,
			nodetype => $nodeType,
			mo => $mo,
			action  => $action,           
			attributes => $attributes);
            
	    return $rec;
	    
	} else {
		$self->_error("getRecord: record is rejected mo: $mo is invalid ");
		return undef;
	}

}



##-----------------------------------------------------------------------------
# parseFiles($fileList, $callback, $pmdParser)
# Parses one or more avlog files, returning event records to the callback sub as
# they are found. The last processed record is stored separately for each file
# - $fileList	Ref to array containing local filenames of syslog files
# - $callback 	Ref to callback sub
# RETURNS: $resultcode
# - $resultCode	  Result Code - see constants for definitions
# - $startTime    Starting time for the time window covered by this run (epoch secs)
# - $endTime      Ending time for the time window covered by this run (epoch secs)
#------------------------------------------------------------------------------
sub parseFiles {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $fileList, $callback)=@_;

	my (
		$startTime,	# Start of time window. Each run should provide a window of time for which we believe we have captured all events
		$endTime,	# End of time window
		$rcode,		# Result code
	);
	
	
	# last_record is a hash of file => last record#
	# In case the software is rolled back and then forward we might see cases where more than one of the
	# possible files has changes	
	my $lastRecTable=$self->{collHist}->get('last_record');
	$srv->logDebug(4, "lastRecTable (Beginning)".Dumper ($lastRecTable)) if DEBUG4;
		
				
	foreach my $file (@{$fileList}) {
		
		my ($baseFileName) = ($file =~ /([^\/]+)$/);
		$srv->logDebug(4, "parsing file : $baseFileName ");
		
				
		my $lastrecord = $lastRecTable->{$baseFileName};		
        $srv->logDebug(4, "last record is: " . Dumper ($lastrecord) );

				
		my ($parseRcode, $lastRecInFile, $missedRec, $fileStartTime, $fileEndTime) = $self->parse($file, $callback, $lastrecord );
		
		if (defined $lastRecInFile) {
			$lastRecTable->{$baseFileName} = $lastRecInFile;
		}

		$startTime = $fileStartTime unless (defined $startTime and $startTime < $fileStartTime);
		$endTime = $fileEndTime unless (defined $endTime and $endTime > $fileEndTime);
		
		if ($parseRcode != OK) {
			$self->_warn("Failed parsing file $file, rcode=$rcode");
			$rcode=PARSE_FAILED;
			next;
		}
		
		$rcode = OK_MISSEDDATA if (! defined $rcode && $missedRec);
		
	}

    $srv->logDebug(4, "lastRecTable (end)".Dumper ($lastRecTable)) if DEBUG4;
	$self->{collHist}->put('last_record', $lastRecTable);

	
	$rcode = OK unless defined $rcode;

	$srv->logDebug(4, "Result of file parsing: rcode=$rcode, starttime=$startTime, endtime=$endTime") if DEBUG4;
	
	return $rcode, $startTime, $endTime;

}
##-----------------------------------------------------------------------------
# DESTROY()
# Destructor
#------------------------------------------------------------------------------
sub DESTROY {
	my $self=shift;
	# Do destruction!!
}
#------------------------------------------------------------------------------
1; # Modules always return 1!
