#!/usr/bin/perl
###----------------------------------------------------------------------------
#
# Controller for collection and parsing of event logs from CPP Nodes
#
# (c) Ericsson AB 2007 - All Rights Reserved
#
# No part of this material may be reproduced in any form
# without the written permission of the copyright owner.
# The contents are subject to revision without notice due 
# to continued progress in methodology, design and manufacturing. 
# Ericsson shall have no liability for any error or damage of any
# kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of
# Telefonaktiebolaget LM Ericsson. All other trademarks mentioned
# herein are the property of their respective owners. 
# 
#------------------------------------------------------------------------------
#
# Author: eharmic
# $Id: CollectionController.pm 7485 2012-08-30 01:49:25Z eharmic $

package Itk::EventData::CollectionController;
use strict;
use Hash::Util qw(lock_keys);
use Itk::EventData::FileFetcher;
use Itk::EventData::PmdParser;
use Itk::EventData::SyslogParser;
use Itk::EventData::AvlogParser;
use Itk::EventData::UpgradeLogParser;
use Itk::EventData::AuditLogParser;
use Itk::EventData::AlarmLogParser;
use Itk::EventData::ECDAParser;
use Itk::EventData::GCPUParser;

use Exporter 'import';
our @EXPORT_OK = qw (fmtTime);

BEGIN {
	if (DEBUG6) {
		require Data::Dumper;
		import Data::Dumper;
	}
}


#==============================================================================
# CONSTANTS
#==============================================================================
use constant {
	# Result Codes
	OK => 1,			# Operation succeeded
	OK_MISSEDDATA => 2,		# Operation succeeded, but some missing data
};
#==============================================================================
# INSTANCE VARIABLES
#==============================================================================
use fields (
	'eventTypes',		# Types of event we are processing
	'filetypes',		# Needed file types for these events
	'storedir',		# Local Storage Area
	'zpm',			# Path to ZPM executable
	'nodelete',		# If true, files will not be deleted at end of run
	'dbfilename',       	# filename of the historic database,
	'noping',            	# no ping option set 
	'dumpdir',		# Directory to move dumps to at end
	'localdir'       
);
#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $srv;  #reference to global server object
*srv=\$main::srv;

# Table mapping event types to files that need to be fetched to get data
my %eventTable = (
	restart => {
		files => [ 'avlog', 'syslog', 'pmd', 'ecda', 'gcpu'],
	},
	upgrade => {
		files => [ 'avlog', 'upgradelog'],
	},
	install => {
		files => [ 'avlog', 'upgradelog'],
	},
	alarm => {
		files => [ 'alarmlog' ],
	},
    audit => { 
    	files => [ 'auditlog'],
    },
    shellaudit => { 
    	files => [ 'shellauditlog'],
    },
    integrity => {
    	files => [ ],
    }
);
lock_keys(%eventTable);
#==============================================================================
# CLASS METHODS
#==============================================================================
##-----------------------------------------------------------------------------
# fmtTime($ts)
# Format a timestamp as an ISO 8601 string
# - $ts		Timestamp in epoch seconds
# - $f		Format. 0=yyyy-mm-dd hh:mm; 1=yyyy-mm-dd
# RETURNS: $time
# - $time 	Time as a string
#------------------------------------------------------------------------------
sub fmtTime {
	my ($ts, $f)=@_;
	my ($s,$m,$h,$dd,$mm,$yy)=gmtime($ts);
	if ($f==1) {
		return sprintf("%04d-%02d-%02d", $yy+1900,$mm+1,$dd);
	} else {
		return sprintf("%04d-%02d-%02d %02d:%02d:%02d", $yy+1900,$mm+1,$dd,$h,$m,$s);
	}
}

##-----------------------------------------------------------------------------
# getSupportedEventTypes()
# Get list of event types that can be handled
# RETURNS: $list
# - $list	Ref to array of event types
#------------------------------------------------------------------------------
sub getSupportedEventTypes {
	return [keys %eventTable];
}

#==============================================================================
# PRIVATE METHODS
#==============================================================================

sub _determineFileTypes {
	$srv->logEnter(\@_) if ENTER;
	my ($self) = @_;
	my %filetypes;
	foreach my $eventType (@{$self->{eventTypes}}) {
		map $filetypes{$_}=1, @{$eventTable{$eventType}{files}};
	}
	return [ keys %filetypes ];
}

#==============================================================================
# PUBLIC METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# new(%opts)
# Constructor
# % eventTypes	Comma separated list of event types to be included in processing
# % storedir	Local storage directory
# % zpm		Path to ZPM executable
# % nodelete	If true, do not clean out fetched log files
# % dbfilename  Collection History database file
# % localdir	Local Directory containing files to be processed; used for offline mode
# % noping	Omit ping check for remote hosts
# % dumpdir	Directory that PMDs will be moved to after parsing
#------------------------------------------------------------------------------
sub new {
	$srv->logEnter(\@_) if ENTER;
	my ($self, %opts)=@_;
	lock_keys(%opts, qw(localdir eventTypes storedir zpm nodelete dbfilename noping dumpdir));
	$self=fields::new($self) unless ref $self;
	
	if (defined $opts{eventTypes}) {
		$self->{eventTypes}=ref $opts{eventTypes} eq 'ARRAY' ? $opts{eventTypes} : [ split ',',$opts{eventTypes} ];
	} else {
		$self->{eventTypes}=getSupportedEventTypes(); # All event types
	}
	
	if (defined $opts{localdir}) {
		$self->{localdir} = $opts{localdir};
	}
	
	$self->{filetypes} = $self->_determineFileTypes();
	$self->{storedir}=$opts{storedir};
	$self->{zpm}=$opts{zpm};
	$self->{nodelete}=$opts{nodelete};
	$self->{dumpdir}=$opts{dumpdir};
	$self->{dbfilename}=$opts{dbfilename};
	$self->{noping}=$opts{noping};
	return $self;
}

##-----------------------------------------------------------------------------
# run($nodeInfo, $callback, %opts)
# Run collection process against indicated Node
# - $nodeInfo	Ref to a hash containing information about the node to be
#		processed. The following entries are expected in the hash:
#		 * username     => node username
#		 * passwd       => node password
#		 * ip_address   => node ip address
#		 * itk_nodename => Node unique name or equivalent
#		 * transferMethod => ftp or sftp transfer method
# - $callback	Ref to a subroutine that will be called back to deliver the
#		event records discovered during processing
#		The sub will be called with an event record as the first parameter
#		(see Itk::EventData::EventRec)
# % progress 	Ref to a sub that will be called with status updates
# % startdate	Starting Date (as unix epoch timestamp). Data earlier than this 
#		will not be processed
# % collhist	Collection History instance to be used. If not supplied then one
#       will be created.
# % filefetcher	Instance of Itk::FileFetcher. If supplied then this instance is used
#       for file fetching; otherwise an instance is created
# % pmddelivery	Specifies method for PMD delivery. If false or not present then
#           legacy delivery of PMDs applies. If true then they are moved into the
# 			dumpdir and the callback is called with the name as the first argument.
# RETURNS: Nothing
#------------------------------------------------------------------------------
sub run {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $nodeInfo, $callback, %opts)=@_;
	lock_keys(%opts, qw(progress startdate collhist pmddelivery filefetcher));
	
	my %iData; # Integrity data which will be returned
	
	# Get collection history object for this node
	my $collHist = $opts{collhist};
	
	my %iData; # Hash used to collect data about integrity & timespan of each event type
	
    # The collection history contains the start time and end time of the last run
    # in a hash with keys starttime, endtime
    $iData{lastrun} = $collHist->get('lastrun'); 
        
    $srv->logDebug(6, "last run from historical db is: ". Dumper ($iData{lastrun})) if (DEBUG6);
					
	# take the lastrun file from 
	
	# ----------------------------------------------------------------------
	# Do file fetching
	# ----------------------------------------------------------------------
	my ($rcode, $rtable);
	my $fetcher;
	
	&{$opts{progress}}('Fetching Files') if exists $opts{progress};
	
	
	if (defined ($self->{localdir})) {
		$fetcher = new Itk::EventData::LocalFileFetcher($collHist, $nodeInfo, startdate => $opts{startdate}, localdir => $self->{localdir});
	   ($rcode, $rtable) = $fetcher->fetchLogFiles (filetypes => $self->{filetypes}, nodelete=>$self->{nodelete});
	} else {
		if ($opts{filefetcher}) {
			$fetcher = $opts{filefetcher};
		} else {
			$fetcher = new Itk::EventData::FileFetcher($collHist, $nodeInfo, startdate => $opts{startdate});
		}
	   ($rcode, $rtable) = $fetcher->fetchLogFiles(filetypes => $self->{filetypes}, noping=>$self->{noping});
	}
	
	$iData{filefetch} = { rcode => $rcode, filetable => $rtable };
	
	$srv->logDebug(6, "($nodeInfo->{itk_nodename}) Result Table from filefetcher: ".Dumper($rtable)) if (DEBUG6);
	
	if ($rcode == OK) {
		# ----------------------------------------------------------------------
		# Parse PMDs
		# ----------------------------------------------------------------------
		# Check if any PMD files were found. If they were then
		# the result table will contain an array in Key pmd=>files
		my $pmdParser=new Itk::EventData::PmdParser(nodename => $nodeInfo->{itk_nodename}, zpm => $self->{zpm}, dumpdir=>$self->{dumpdir});
		
		if (exists $rtable->{pmd} && defined $rtable->{pmd}{files} && @{$rtable->{pmd}{files}}) {
			
			$srv->logDebug(4, 'Parse PMD files: '.join(',',@{$rtable->{pmd}{files}})) if DEBUG4;
			
			&{$opts{progress}}('Parsing PMDs') if exists $opts{progress};
		
			my $rcode = $pmdParser->parseFiles($rtable->{pmd}{files}, $callback, tmpDir=>$self->{storedir}, pmddelivery=>$opts{pmddelivery});
			
			$iData{pmd} = { rcode => $rcode };
		}

		# ----------------------------------------------------------------------
		# Parse syslog files
		# ----------------------------------------------------------------------
		my $syslogParser = new Itk::EventData::SyslogParser($collHist,
			nodename => $nodeInfo->{itk_nodename}, 
			startdate => $opts{startdate}
		);
		
		if (exists $rtable->{syslog} && defined $rtable->{syslog}{files} && @{$rtable->{syslog}{files}}) {
			&{$opts{progress}}('Parsing Syslog') if exists $opts{progress};
			my ($rcode, $startTime, $endTime) = $syslogParser->parseFiles($rtable->{syslog}{files},$callback,$pmdParser);
			$iData{syslog} = { rcode => $rcode, starttime => $startTime, endtime => $endTime };
		}
	
		# ----------------------------------------------------------------------
		# Parse avlog files
		# ----------------------------------------------------------------------
		my $avParser=new Itk::EventData::AvlogParser($collHist, 
			nodename => $nodeInfo->{itk_nodename}, 
			startdate => $opts{startdate}
		);
		
		if (exists $rtable->{avlog} && defined $rtable->{avlog}{files} && @{$rtable->{avlog}{files}}) {
			&{$opts{progress}}('Parsing Avlog') if exists $opts{progress};
			my ($rcode, $startTime, $endTime) = $avParser->parseFiles($rtable->{avlog}{files}, $callback, $pmdParser, $syslogParser);
			$iData{avlog} = { rcode => $rcode, starttime => $startTime, endtime => $endTime };
		}
		
		# Ensure that even if no avlog files were found, we still flush out
		# all the restarts from the syslog
		$syslogParser->flushEvents($callback, $avParser);
		
		# ----------------------------------------------------------------------
		# Parse ECDA (DSP Dumps on WRAN RBS)
		# ----------------------------------------------------------------------
		# Check if any ECDA files were found. If they were then
		# the result table will contain an array in Key =>files
		my $ecdaParser=new Itk::EventData::ECDAParser(
			nodename => $nodeInfo->{itk_nodename}, 
			dumpdir=>$self->{dumpdir},
			avlogparser => $avParser
		);
		
		if (exists $rtable->{ecda} && defined $rtable->{ecda}{files} && @{$rtable->{ecda}{files}}) {
			
			$srv->logDebug(4, 'Parse ECDA files: '.join(',',@{$rtable->{ecda}{files}})) if DEBUG4;
			
			&{$opts{progress}}('Parsing ECDA dumps') if exists $opts{progress};
			
			my $rcode = $ecdaParser->parseFiles($rtable->{ecda}{files}, $callback, pmddelivery=>$opts{pmddelivery});
			
			$iData{ecda} = { rcode => $rcode };
		}

		# ----------------------------------------------------------------------
		# Parse GCPU (DSP Dumps on ENB)
		# ----------------------------------------------------------------------
		# Check if any GCPU dump files were found. If they were then
		# the result table will contain an array in Key =>files
		my $gcpuParser=new Itk::EventData::GCPUParser(
			nodename => $nodeInfo->{itk_nodename}, 
			dumpdir=>$self->{dumpdir},
			avlogparser => $avParser
		);
		
		if (exists $rtable->{gcpu} && defined $rtable->{gcpu}{files} && @{$rtable->{gcpu}{files}}) {
			
			$srv->logDebug(4, 'Parse GCPU files: '.join(',',@{$rtable->{gcpu}{files}})) if DEBUG4;
			
			&{$opts{progress}}('Parsing GCPU dumps') if exists $opts{progress};
			
			my $rcode = $gcpuParser->parseFiles($rtable->{gcpu}{files}, $callback, pmddelivery=>$opts{pmddelivery});
			
			$iData{gcpu} = { rcode => $rcode };
		}
		
		# ----------------------------------------------------------------------
		# Parse Upgrade Logs
		# ----------------------------------------------------------------------
		my $upParser = new Itk::EventData::UpgradeLogParser($collHist,
			nodename => $nodeInfo->{itk_nodename}, 
			startdate => $opts{startdate}
		);

		if (exists $rtable->{upgradelog} && defined $rtable->{upgradelog}{files} && @{$rtable->{upgradelog}{files}}) {
			&{$opts{progress}}('Parsing Upgradelog') if exists $opts{progress};
			my ($rcode, $startTime, $endTime) = $upParser->parseFiles($rtable->{upgradelog}{files}, $callback, avlogparser => $avParser);
			$iData{upgradelog} = { rcode => $rcode, starttime => $startTime, endtime => $endTime };
		}

		# ----------------------------------------------------------------------
		# Parse Audit Logs
		# ----------------------------------------------------------------------
		my $auditParser = new Itk::EventData::AuditLogParser($collHist,
			nodename => $nodeInfo->{itk_nodename}, 
			startdate => $opts{startdate},
			record => 'auditdata',	
		);

		if (exists $rtable->{auditlog} && defined $rtable->{auditlog}{files} && @{$rtable->{auditlog}{files}}) {
			&{$opts{progress}}('Parsing Auditlog') if exists $opts{progress};
			my ($rcode, $startTime, $endTime) = $auditParser->parseFiles($rtable->{auditlog}{files}, $callback);
			$iData{auditlog} = { rcode => $rcode, starttime => $startTime, endtime => $endTime };
		}

		# ----------------------------------------------------------------------
		# Parse Shell Audit Logs
		# ----------------------------------------------------------------------
		my $shellAuditParser = new Itk::EventData::AuditLogParser($collHist,
			nodename => $nodeInfo->{itk_nodename}, 
			startdate => $opts{startdate},
			record => 'shellauditdata'			
		);

		if (exists $rtable->{shellauditlog} && defined $rtable->{shellauditlog}{files} && @{$rtable->{shellauditlog}{files}}) {
			&{$opts{progress}}('Parsing ShellAuditlog') if exists $opts{progress};
			my ($rcode, $startTime, $endTime) = $shellAuditParser->parseFiles($rtable->{shellauditlog}{files}, $callback);
			$iData{shellauditlog} = { rcode => $rcode, starttime => $startTime, endtime => $endTime };
		}


		# ----------------------------------------------------------------------
		# Parse Alarm Logs
		# ----------------------------------------------------------------------
		my $alarmParser = new Itk::EventData::AlarmLogParser($collHist,
			nodename => $nodeInfo->{itk_nodename}, 
			startdate => $opts{startdate},
			fdnmap => $nodeInfo->{mapFdn}
		);

		if (exists $rtable->{alarmlog} && defined $rtable->{alarmlog}{files} && @{$rtable->{alarmlog}{files}}) {
			&{$opts{progress}}('Parsing Alarmlog') if exists $opts{progress};
			my ($rcode, $startTime, $endTime) = $alarmParser->parseFiles($rtable->{alarmlog}{files}, $callback, alarmlogparser => $avParser);
			$iData{alarmlog} = { rcode => $rcode, starttime => $startTime, endtime => $endTime };
		}

		# Safety Check
		$pmdParser->reportUnusedPmds();
	}
	
	$self->_integrityAssesment($_, \%iData,$nodeInfo,$callback) foreach (@{$self->{eventTypes}});
	
	$collHist->put('lastrun',$iData{thisrun});
	
	# Commit changes to the collection history
	# Only do this if we created it - if supplied by caller then they are
	# responsible for committing
	$collHist->commit() unless $opts{collhist};
	
	unless ($self->{nodelete}) {
		$fetcher->removeFiles();
	}
}
##-----------------------------------------------------------------------------
# _integrityAssesment($type, $iData,$nodeInfo,$callback)
# Asses integrity of data for given event type, then output an integrity record
# - $type	Type of event to produce an integrity record for
# - $iData	Ref to hash of intgrity data
# - $nodeInfo	Ref to hash of node info (see params for run)
# - $callback	Sub to send the resulting record to
#------------------------------------------------------------------------------
sub _integrityAssesment {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $type, $iData, $nodeInfo, $callback) = @_;

	$srv->logDebug(5, "Integrity Assesment for type=$type") if DEBUG5;

	#use Data::Dumper; #print STDERR Dumper($iData,$nodeInfo);

	my ($result, $startTime, $endTime);
	
	{ 
		if ($iData->{filefetch}{rcode} != Itk::EventData::FileFetcher::OK) {
			$result = $iData->{filefetch}{rcode};
			last;
		}
		
		# Find out:
		# 1) If there was any new data (ie. new files) for any of the input files for this record type.
		# 2) The earliest time that the files were fetched
		# 2) If there was any missing records in the data for any of the input files. Each parser will return OK_MISSINGDATA in that scenario
		# 3) The min endtime of all used files, and the max starttime
		
		my (	$newFiles,	# Flag: new files were found? 
			$missingdata,   # Flag: Was there any missing data?
			$minEndTime,    # earliest end time span for records found in any input file
			$maxStartTime,  # latest start time span for records found in any input file
			$minFetchTime,	# earliest fetch time of all input files
		);
		
		my $fileTable=$iData->{filefetch}{filetable};
		foreach my $fileType (@{$eventTable{$type}{files}}) {
			
			if (exists $fileTable->{$fileType}) {
				$newFiles=1  if defined $fileTable->{$fileType}{files} and @{$fileTable->{$fileType}{files}};
				$minFetchTime=$fileTable->{$fileType}{time} if defined $fileTable->{$fileType}{time} and ( ! defined $minFetchTime or $fileTable->{$fileType}{time} < $minFetchTime);
			}
			
			$srv->logDebug(5, sprintf("Check file type '$fileType': newFiles=$newFiles MinFetchTime = $minFetchTime (%s)",fmtTime($minFetchTime))) if DEBUG5;
			
			if (exists $iData->{$fileType}) {
				$missingdata=1 if $iData->{$fileType}{rcode} == OK_MISSEDDATA;
				$srv->logDebug(5, sprintf("rcode = $iData->{$fileType}{rcode}, starttime = $iData->{$fileType}{starttime} (%s), endtime=$iData->{$fileType}{endtime} (%s)",fmtTime($iData->{$fileType}{starttime}),fmtTime($iData->{$fileType}{endtime}))) if DEBUG5;
				next unless defined $iData->{$fileType}{endtime} && defined $iData->{$fileType}{starttime};
				$minEndTime = $iData->{$fileType}{endtime} unless defined $minEndTime and $minEndTime < $iData->{$fileType}{endtime};
				$maxStartTime = $iData->{$fileType}{starttime} unless defined $minEndTime and $maxStartTime > $iData->{$fileType}{starttime};
			}
		}
		
		$srv->logDebug(5, sprintf("Analysis result: new files=%s missing data: %s minFetchTime=$minFetchTime (%s) maxStartTime=$maxStartTime (%s) minEndTime=$minEndTime (%s)",($newFiles ? 'yes' : 'no'),($missingdata ? 'yes' : 'no'),fmtTime($minFetchTime),fmtTime($maxStartTime),fmtTime($minEndTime))) if DEBUG5;
		
		# If never run before towards this node, there will be no
		# lastrun dates, in which case use the beginning of this run
		my $lastrun_end = $iData->{lastrun}{$type}{endtime} || $maxStartTime;
		
		if (not $newFiles) {
			$startTime=$lastrun_end;
			$endTime=time;
			$result=OK;
			last;
		}
		
		if ($missingdata) {
			$result=OK_MISSEDDATA;
			$startTime=$maxStartTime;
			$endTime=$minFetchTime;
			last;
		} else {
			$result=OK;
			$startTime=$lastrun_end;
			$endTime=$minFetchTime;
			last;
		}
		
	}
	
	$srv->logDebug(5, sprintf("Result of Integrity Assesment for type=$type:\n\trcode=$result starttime=$startTime (%s) endtime=$endTime (%s)",fmtTime($startTime),fmtTime($endTime))) if DEBUG5;

	my $rec = new Itk::EventData::EventRec::Integrity(
		nodename => $nodeInfo->{itk_nodename},
		integritytype => $type,
		result => $result,
		timestart => defined $startTime ? fmtTime($startTime) : undef,
		timeend => defined $endTime ? fmtTime($endTime) : undef,
		time => time
	);
	
	&$callback($rec);
	
	$iData->{thisrun}{$type} = {
		starttime=>$startTime,
		endtime=>$endTime
	};
}	
##-----------------------------------------------------------------------------
# DESTROY()
# Destructor
#------------------------------------------------------------------------------
sub DESTROY {
	my $self=shift;
	# Do destruction!!
}
#------------------------------------------------------------------------------
1; # Modules always return 1!


