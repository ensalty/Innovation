###----------------------------------------------------------------------------
# Modules handling file transfer functionality (eg ftp, sftp)
#
# � Ericsson AB 2006 - All Rights Reserved
#
# No part of this material may be reproduced in any form without the written
# permission of the copyright owner. The contents are subject to revision without
# notice due to continued progress in methodology, design and manufacturing.
# Ericsson shall have no liability for any error or damage of any kind resulting
# from the use of these documents.
#
# Ericsson is the trademark or registered trademark of Telefonaktiebolaget LM
# Ericsson. All other trademarks mentioned herein are the property of their
# respective owners.
#
#-------------------------------------------------------------------------------
#
# Author: David Markham
# $Id: FileTransfer.pm 7874 2012-11-25 23:51:43Z eanzmark $
#

# README! 
# This functionality has been moved to Itk::Utils::FileTransfer
# Further development should be using that module, as this will disappear soon...

use strict;
use Time::Local qw/ timegm /;

package Itk::FileTransfer;

use fields (
	'host',  # Remote hostname or IP address
	'port',  # Remote port
	'timeout',  # Current active timeout (seconds), or "0" for no timeout
	'remotewd',  # Current remote working directory
	'localwd',  # Current local working directory
);

#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $srv;  #reference to global server object
*srv=\$main::srv;
my %months = (jan=>0, feb=>1, mar=>2, apr=>3, may=>4, jun=>5, jul=>6, aug=>7, sep=>8, oct=>9, nov=>10, dec=>11);  #Month abbreviation strings (lower case for comparison)
my $currentYear;

sub _checkPath {
  my ($self, $filename, $path) = @_;
  
  if ($path and $filename !~ m|/|) {  #if a default path is defined and the remote filename doesn't contain '/' (ie just a filename, not a path)
  	$filename = $path.'/'.$filename;  #prepend path
		$srv->logDebug(3, "Added path: $filename") if DEBUG3;
  }
  return $filename;
}


#==============================================================================
# CLASS METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# fileTimestampToUtcEpoch($timestamp)
# Converts a file timestamp into a UTC epoch seconds time value.
# - $timestamp		File timestamp in one of the following formats:
#       Oct 22 16:04  /  May 29 2008
# RETURNS: $mtime
# - $mtime  Epoch seconds value (UTC) of timestamp, or undef if conversion not possible
#------------------------------------------------------------------------------
sub fileTimestampToUtcEpoch {
  my ($timestamp) = @_;

	$currentYear = (gmtime(time))[5]+1900 unless defined $currentYear;  #only calculate this once per instance
	my ($mtime);
	$srv->logDebug(4, "timestamp for conversion: '$timestamp'") if DEBUG4;
	if ($timestamp =~ /(\w\w\w)\s+(\d+)\s+(\d+):(\d+)/) {  #format "Oct 22 16:04"

		my $mon = $months{lc($1)};
		return undef unless defined $mon;
		my $checkYear = $currentYear;

		# Handle the specific case of Feb 29 - we don't want to evaluate this for the current
		# year if the file was really from last year (otherwise gmtime will die) (issue #1248)
		if ($mon==1 and $2==29) {  # Feb 29
			my ($currentDay, $currentMon) = (gmtime(time))[3..4];
			# If current day is before Feb 29, dir is from the previous year
			$checkYear-- if ($currentMon==0 or ($currentMon==1 and $currentDay<29));
		}

		eval { $mtime = Time::Local::timegm(0, $4, $3, $2, $mon, $checkYear); };
		if ($@) {
			$srv->logError("Could not determine mtime from '$timestamp' (year=$checkYear)");
			return undef;
		}
		
		# If this timestamp is greater than the current time then assume date is from previous year
		# We include a grace period of 12 hours (43200 secs) to allow for servers which
		# are on a different timezone to their nodes
		if ($mtime > (time + 43200) ) {  
			$checkYear--;
			eval { $mtime = Time::Local::timegm(0, $4, $3, $2, $mon, $checkYear); };
			if ($@) {
				$srv->logError("Could not determine mtime from '$timestamp' (prevyear=$checkYear)");
				return undef;
			}
		}

	} elsif ($timestamp =~ /(\w\w\w)\s+(\d+)\s+(\d{4})/) {  #format "May 29 2008"
		my $mon = $months{lc($1)};
		return undef unless defined $mon;
		# use time of "00:00:00" on this date
		eval { $mtime = Time::Local::timegm(0, 0, 0, $2, $mon, $3); };
		if ($@) {
			$srv->logError("Could not determine mtime from '$timestamp'");
			return undef;
		}
	}

	$srv->logDebug(4, "mtime result: $mtime") if DEBUG4;
	return $mtime;
}


##-----------------------------------------------------------------------------
# extractCelloFileDetails($line)
# Extracts file fields from the line formats produced by CPP nodes via 'ls -l' and ftp 'dir'
# - $line		Line with file details
# RETURNS: $hashRef
# - $hashRef  Reference to hash containing file details (or undef if line not recognised)
#    Hash keys: type, attr, numlink, size, mday, month, year, hours, min, sec, name, time
#------------------------------------------------------------------------------
# NB - moved here from Itk::NodeData::Functions - currently unused at r4446
# Dependencies should be checked if this is needed
#sub extractCelloFileDetails {
#	my ($line) = @_;
#	my (%details, $mon);
#	if ($line =~ /^([d-])([rwx-]{9})\s+(\d+)\s+(\d+)\s+(\d+) (\S+) (\d+) (\d\d):(\d\d):(\d\d) (.*)$/) {  #Cello 'ls -l' format
#		%details = (type=>$1, attr=>$2, numlink=>$3, size=>$4, mday=>$5, month=>$6,
#								year=>$7, hours=>$8, min=>$9, sec=>$10, name=>$11);  #NB third string is presumed to be number of links (as in unix)
#	} elsif ($line =~ /^([d-])([rwx-]{9})\s+(\d+)\s+(\S+)\s+(\S+)\s+(\d+)\s+(\S+)\s+(\d+)\s+(\d\d):(\d\d)\s+(.*)$/) {  #Cello ftp server 'dir' format
#		%details = (type=>$1, attr=>$2, numlink=>$3, size=>$6, mday=>$8, month=>$7,
#								hours=>$9, min=>$10, name=>$11);  #NB third string is presumed to be number of links (as in unix)
#		$details{year} = Itk::Utils::dateStr(time(), 'YYYY');  #Cello doesn't return the year if this year (or seconds) through its FTP server...
#		$details{sec} = 0;
#	} elsif ($line =~ /^([d-])([rwx-]{9})\s+(\d+)\s+(\S+)\s+(\S+)\s+(\d+)\s+(\S+)\s+(\d+)\s+(\d\d\d\d)\s+(.*)$/) {  #Cello ftp server 'dir' format, file from previous years
#		%details = (type=>$1, attr=>$2, numlink=>$3, size=>$6, mday=>$8, month=>$7,
#								year=>$9, name=>$10);  #NB third string is presumed to be number of links (as in unix)
#		$details{hours} = 0;
#		$details{min} = 0;
#		$details{sec} = 0;
#	} else {return undef;}
#	#map month abbreviation to zero-based month number
#	MONTH: for($mon=0; $mon<(Itk::Utils::MONTH_ABBREVS); $mon++) {
#		last MONTH if ((Itk::Utils::MONTH_ABBREVS)[$mon] eq $details{month});
#	};
#	$details{time} = &Time::Local::timelocal($details{sec}, $details{min}, $details{hours}, $details{mday}, $mon, $details{year});
#	return \%details;
#}


##------------------------------------------------------------------------------
# new(%options)
# Constructs a new FileTransfer object (abstract class!)
# - %options
# % host  Remote hostname or IP address
# % port  Remote port
# % timeout  Login/command timeout to use initially
# RETURNS: FileTransfer object
#------------------------------------------------------------------------------
sub new {
	my __PACKAGE__ $self = shift;
	my (%options) = @_;
	$self = fields::new($self) unless (ref $self);
	$self->{host} = $options{host};
	$self->{port} = $options{port};
	$self->{timeout} = $options{timeout};
	return $self;
}


##------------------------------------------------------------------------------
# requestPassword([$prompt])
# Requests a password to be entered by the user on STDIN (typed characters not echoed)
# - $prompt  Optional string to prompt user with
# RETURNS: $password
# - $password Password string supplied by user
#------------------------------------------------------------------------------
sub requestPassword {
  my ($self, $prompt) = @_;
  $prompt = 'Password:' unless $prompt;  #default prompt if nothing supplied
  system "stty -echo";
  print $prompt;
  chomp(my $pw = <STDIN>);
  print "\n";
  system "stty echo";
  return $pw;  #NB - trailing chars has been removed
}


##------------------------------------------------------------------------------
# login($user, $password)
# Attempts to login to the defined host.
# - $user  User name
# - $password  Password
# RETURNS: Boolean indicating success
#------------------------------------------------------------------------------
sub login {
	my ($self, $user, $password) = @_;
	return undef;
}


##------------------------------------------------------------------------------
# ls([$dir])
# Get a directory listing (names only)
# - $dir  Remote directory
# RETURNS: 
# In array context, list of items returned from server
# In scalar context, returns a reference to the list
#------------------------------------------------------------------------------
sub ls {
	my ($self, $dir) = @_;
	return undef;
}

##------------------------------------------------------------------------------
# dir([$dir])
# Get a directory listing in long format
# - $dir  Remote directory
# RETURNS: 
# In array context, list of lines returned from server
# In scalar context, returns a reference to the list
#------------------------------------------------------------------------------
sub dir {
	my ($self, $dir) = @_;
	return undef;
}

##------------------------------------------------------------------------------
# pwd()
# Returns current remote working directory.
#------------------------------------------------------------------------------
sub pwd {
	my ($self) = @_;
	return undef;
}

##------------------------------------------------------------------------------
# get($remoteFile, $localFile)
# Get remoteFile from the server and store locally
# - $remoteFile  Remote path
# - $localFile  Local path
# RETURNS: 
# Non-zero value if file fetched successfully, undef otherwise
#------------------------------------------------------------------------------
sub get {
	my ($self, $remoteFile, $localFile) = @_;
	return undef;
}


##------------------------------------------------------------------------------
# put( $localFile , $remoteFile )
# Put localFile on the server and store as remoteFile 
# - $localFile  Local path 
# - $remoteFile  Remote path
# RETURNS: 
# Non-zero value if file uploaded successfully, undef otherwise
#------------------------------------------------------------------------------
sub put {
	my ($self, $localFile, $remoteFile ) = @_;
	return undef;	
}

##------------------------------------------------------------------------------
# cwd($dir)
# Change to a remote working directory
# - $dir  Remote directory
# RETURNS: Boolean indicating success
#------------------------------------------------------------------------------
sub cwd {
	my ($self, $dir) = @_;
	return undef;
}


##------------------------------------------------------------------------------
# clwd($dir)
# Change to a local working directory
# - $dir  Local directory
# RETURNS: Boolean indicating success
#------------------------------------------------------------------------------
sub clwd {
	my ($self, $dir) = @_;
	$self->{localwd} = $dir;
	return 1;
}


##------------------------------------------------------------------------------
# mkdir($dir)
# Change to a remote working directory
# - $dir  Remote directory
# RETURNS: Boolean indicating success
#------------------------------------------------------------------------------
sub mkdir {
	my ($self, $dir ) = @_;
	return undef;
}

##------------------------------------------------------------------------------
# dirStat($dir)
# Supplies a data structure containing the stat details for files in a remote
# directory.
# - $dir  Remote directory
# RETURNS: @fileStatData or undef if dir does not exist
# - @fileStatData  Contains an item for each file in the directory. Each list entry
#     is a hashref, with the following keys:
#     * name
#     * size
#     * mtime (file modification time as UTC-based epoch seconds value)
#     * isdir (boolean indicating if entry is a directory)
#------------------------------------------------------------------------------
sub dirStat {
	my ($self, $dir) = @_;
	return undef;
}


##------------------------------------------------------------------------------
# delete($file)
# Deletes a remote file
# - $file  Remote file
# RETURNS: Boolean indicating success
#------------------------------------------------------------------------------
sub delete {
	my ($self, $file) = @_;
	return undef;
}


##------------------------------------------------------------------------------
# quit()
# Quits the FileTransfer session
# RETURNS: Boolean indicating success
#------------------------------------------------------------------------------
sub quit {
	my ($self) = @_;
	return undef;
}


##------------------------------------------------------------------------------
# reportError()
# Reports an error based on object settings
# RETURNS: nothing
#------------------------------------------------------------------------------
sub reportError {
	my ($self, $msg) = @_;
	$srv->logError($msg);
}

##------------------------------------------------------------------------------
# reportWarn()
# Reports a warning based on object settings
# RETURNS: nothing
#------------------------------------------------------------------------------
sub reportWarn {
	my ($self, $msg) = @_;
	$srv->logWarn($msg);
}

##------------------------------------------------------------------------------
# reportInfo()
# Reports an info message based on object settings
# RETURNS: nothing
#------------------------------------------------------------------------------
sub reportInfo {
	my ($self, $msg) = @_;
	$srv->logInfo($msg);
}

##------------------------------------------------------------------------------
# lastError()
# Supplies a string giving details for the last internal error seen in this session
# RETURNS: $error
# - $error  Details of last error seen, "0" if none
#------------------------------------------------------------------------------
sub lastError {
	my ($self) = @_;
	return 'unknown';
}

##------------------------------------------------------------------------------
# lastErrorWasTimeout()
# Returns true if the last error seen was due to a timeout.
# RETURNS: $result
# - $result  1 if last error was timeout, 0 if different error type, undef if unknown or no last error
#------------------------------------------------------------------------------
sub lastErrorWasTimeout {
	return undef;
}

#############################################################################

package Itk::FTP;
#############################################################################
# Implements ftp using Net::FTP for Itk FileTransfer functionality

use base 'Itk::FileTransfer';
use Net::FTP;

use fields (
	'ftp',  # 
	'_timeoutOccured',  # boolean flag to indicate that last error was a timeout
	'_lastErrorString',  # contains last error string
);

##------------------------------------------------------------------------------
# _evalTimeoutError()
# Checks to see the result of an eval to a Net::FTP procedure - these are set up
# to die by default, and we need to protect against this for timeouts. Any
# non-timeout error will cause a "die".
# RETURNS: Boolean indicating that error was a timeout
#------------------------------------------------------------------------------
sub _evalTimeoutError {
	my ($self) = @_;

	#reset instance error variables
	$self->{_timeoutOccured} = 0;
	$self->{_lastErrorString} = undef;
	return 0 unless $@;  #check result of last eval block

	#if the eval had an error for anything other than a timeout, just die
	die unless $@ =~ /timeout/i;

	$self->{_lastErrorString} = "$self->{host}:$@";
	chomp $self->{_lastErrorString};
	$srv->logDebug(1, $self->{_lastErrorString}) if DEBUG1;
	$self->{_timeoutOccured} = 1;
	return 1;  # yes, timed out
}


##------------------------------------------------------------------------------
# new(%options)
# Constructs a new Itk::FTP object.
# See Itk::FileTransfer::new() for base class details.
# RETURNS: Itk::FTP object
#------------------------------------------------------------------------------
sub new {
	my __PACKAGE__ $self = shift;
	my (%options) = @_;
	$self = fields::new($self) unless (ref $self);
	$self->SUPER::new(%options);
	$srv->logDebug(2, "Launch ftp to $self->{host}") if DEBUG2;
	my $debug;
	$debug = 9 if DEBUG4;  #Itk DEBUG4 turns on full Net::FTP tracing
	eval {
		$self->{ftp} = Net::FTP->new($self->{host}, Timeout=>$self->{timeout}, Debug=>$debug, Passive => 1) or return undef;
	};
	return undef if (not $self->{ftp} or $self->_evalTimeoutError());
	return $self;
}


##------------------------------------------------------------------------------
# login($user, $password)
# See Itk::FileTransfer::login() for base class details.
#------------------------------------------------------------------------------
sub login {
	my ($self, $user, $password) = @_;
	$srv->logDebug(2, "Login to $self->{host} (user=$user)") if DEBUG2;
	return undef unless $self->{ftp};

	my $result;
	eval {
		$result = $self->{ftp}->login($user, $password);
		$self->{ftp}->binary() if $result;
	};
	return undef if (not $result or $self->_evalTimeoutError());
	return $result;
}

##------------------------------------------------------------------------------
# mkdir( $dir )
# See Itk::FileTransfer::mkdir() for base class details.
#------------------------------------------------------------------------------
sub mkdir {
	my ($self, $dir ) = @_;
	return undef unless $self->{ftp};
	$srv->logDebug(2, "Created dir $dir") if DEBUG2;
	
	my $result;
	eval {
		$result = $self->{ftp}->mkdir($dir);
	};
	return $self->_evalTimeoutError()? undef : $result;	
}


##------------------------------------------------------------------------------
# dirStat($dir)
# See Itk::FileTransfer::dirStat() for base class details.
#------------------------------------------------------------------------------
sub dirStat {
	my ($self, $dir) = @_;
	return undef unless $self->{ftp};

	my @output;
	eval {
		foreach($self->{ftp}->dir($dir)) {
			#Test for standard ftp server 'dir' line format (including Cello ftp server)
			if (/^([d-])([rwx-]{9}\S?)\s+(\d+)(?:\s+(\S+))?\s+(\S+)\s+(\d+)\s+(\S+)\s+(\d+)\s+(\d\d:\d\d|\d{4})\s+(.+)$/) {
				my %fileStat;
				#warning - cut-down verson only provided here... extra stat fields need to be added as required...
				$fileStat{size} = $6;
				# convert the last mod time, i.e: for file 'May 16 14:05', for dir 'Sep 19 2006'
				$fileStat{mtime} = Itk::FileTransfer::fileTimestampToUtcEpoch("$7 $8 $9");
				$fileStat{name} = $10;
				$fileStat{isdir} = ($1 eq 'd');  #this is a customised entry for efficiency
				push(@output, \%fileStat);
			} else {
				$srv->logDebug(3, "Unrecognised dir entry: '$_'") if DEBUG3;
			}
		}
	};
	return undef if $self->_evalTimeoutError();
	return \@output;
}

##------------------------------------------------------------------------------
# dir( $dir )
# See Itk::FileTransfer::dir() for base class details.
#------------------------------------------------------------------------------
sub dir {
	my ( $self, $dir ) = @_;
	return undef unless $self->{ftp};
	
	my $result;
	eval {
		$result = $self->{ftp}->dir($dir);
	};
	return $self->_evalTimeoutError()? undef : $result;
}


##------------------------------------------------------------------------------
# ls( $dir )
# See Itk::FileTransfer::ls() for base class details.
#------------------------------------------------------------------------------
sub ls {
	my ( $self, $dir ) = @_;
	return undef unless $self->{ftp};
	
	my $result;
	eval {
		$result = $self->{ftp}->ls($dir);
	};
	return $self->_evalTimeoutError()? undef : $result;
}


##------------------------------------------------------------------------------
# get($remoteFile, $localFile)
# See Itk::FileTransfer::get() for base class details.
#------------------------------------------------------------------------------
sub get {
	my ($self, $remoteFile, $localFile) = @_;
	$srv->logEnter(\@_) if ENTER;
	return undef unless $self->{ftp};
	$localFile = $self->_checkPath($localFile, $self->{localwd});
	#trust FTP session to maintain control of remotewd

	my $result;
	eval {
		$result = $self->{ftp}->get($remoteFile, $localFile);
	};
	return $self->_evalTimeoutError()? undef : $result;
}

##------------------------------------------------------------------------------
# put( $localFile, $remoteFile )
# See Itk::FileTransfer::put() for base class details.
#------------------------------------------------------------------------------
sub put {
	my ($self, $localFile, $remoteFile ) = @_;
	return undef unless $self->{ftp};

	my $result;
	eval {
		$result = $self->{ftp}->put($localFile, $remoteFile );
	};
	return $self->_evalTimeoutError()? undef : $result;
}



##------------------------------------------------------------------------------
# cwd($dir)
# See Itk::FileTransfer::cwd() for base class details.
#------------------------------------------------------------------------------
sub cwd {
	my ($self, $dir) = @_;
	return undef unless $self->{ftp};

	my $result;
	eval {
		$result =  $self->{ftp}->cwd($dir);
	};
	return $self->_evalTimeoutError()? undef : $result;
}

##------------------------------------------------------------------------------
# pwd()
# See Itk::FileTransfer::pwd() for base class details.
#------------------------------------------------------------------------------
sub pwd {
	my ($self) = @_;
	return undef unless $self->{ftp};

	my $result;
	eval {
		$result =  $self->{ftp}->pwd();
	};
	return $self->_evalTimeoutError()? undef : $result;
}

##------------------------------------------------------------------------------
# delete($file)
# See Itk::FileTransfer::delete() for base class details.
#------------------------------------------------------------------------------
sub delete {
	my ($self, $file) = @_;
	return undef unless $self->{ftp};

	my $result;
	eval {
		$result =  $self->{ftp}->delete($file);
	};
	return $self->_evalTimeoutError()? undef : $result;
}


##------------------------------------------------------------------------------
# quit()
# See Itk::FileTransfer::quit() for base class details.
#------------------------------------------------------------------------------
sub quit {
	my ($self) = @_;
	return undef unless $self->{ftp};

	my $result;
	eval {
		$result =  $self->{ftp}->quit();
	};
	return $self->_evalTimeoutError()? undef : $result;
}

##------------------------------------------------------------------------------
# lastError()
# Supplies a string giving details for the last internal error seen in this session
# RETURNS: $error
# - $error  Details of last error seen, "0" if none
#------------------------------------------------------------------------------
sub lastError {
	my ($self) = @_;
	return $self->{_lastErrorString}? $self->{_lastErrorString} : 0;
}

##------------------------------------------------------------------------------
# lastErrorWasTimeout()
# Returns true if the last error seen was due to a timeout.
# RETURNS: $result
# - $result  1 if last error was timeout, 0 if different error type, undef if unknown or no last error
#------------------------------------------------------------------------------
sub lastErrorWasTimeout {
	my ($self) = @_;
	return $self->{_timeoutOccured};
}


#############################################################################

package Itk::SFTP;
#############################################################################
# Implements sftp using Net::SSH2 for Itk FileTransfer functionality

use IO::File;

use base 'Itk::FileTransfer';
my $LIB_SSH2_Available;
BEGIN {
	eval {
		require Net::SSH2;
		import Net::SSH2;
		$LIB_SSH2_Available=1;
	};
}

use fields (
	'ssh',  # Underlying Net::SSH2 session
	'sftp',  # Net::SSH2::SFTP session
	'_invalidSession',  # Flag to indicate that an irrecoverable error has occured, and no further commands will be allowed apart from 'quit'
);


##------------------------------------------------------------------------------
# new(%options)
# Constructs a new Itk::SFTP object.
# See Itk::FileTransfer::new() for base class details.
# RETURNS: Itk::SFTP object
#------------------------------------------------------------------------------
sub new {
	# If lib ssh2 was not able to be loaded then we try to use the expect version instead
	my __PACKAGE__ $self = shift;
	my (%options) = @_;
	return new Itk::SFTPexpect(%options) unless $LIB_SSH2_Available;

	$self = fields::new($self) unless (ref $self);
	$self->SUPER::new(%options);
	
	$srv->logDebug(3, $self->{host}." Creating ssh handle ... ") if DEBUG3;
	$self->{ssh} = Net::SSH2->new();
	#Check library version to ensure they are at the needed levels
	my $libssh2Vers = $self->{ssh}->version;
	my ($libssh2VersNumeric) = ($libssh2Vers =~ /^(\d+\.\d+)/);  #extract initial x.y string
	my $netssh2Vers = $self->{ssh}->VERSION;
	if ($libssh2VersNumeric < 1.2 or $netssh2Vers < 0.28) {  #versions needed for R1.23 code
		$srv->logError("Please upgrade SSH libraries! Need libssh2>=v1.2 (have $libssh2Vers) and Net::SSH2>=v0.28 (have $netssh2Vers)");
		return undef;
	}
	$srv->logDebug(3, $self->{host}." done ... turning on libssh2 debugging ") if DEBUG3;
	$self->{ssh}->debug(1) if DEBUG3;  #FIXME - change to DEBUG4?
	$self->{port} = 22 unless $self->{port};  #default port
	$self->{remotewd} = '.';

	#FIXME - should call Itk::RemoteSession derivative to initialise ssh session

	$self->{timeout} = 5 unless defined $self->{timeout};  #FIXME - temp timeout until support is extended
	my $connectResult;
	eval { $connectResult = $self->{ssh}->connect($self->{host}, $self->{port}, Timeout=>$self->{timeout}); };
	#Net::SSH2 croaks if connect fails - need to report this back as failure to caller
	#(NB - this was the case for old version, not sure about new)
	if ($@) {
		$srv->logDebug(1, $@) if DEBUG1;
		return undef;
	}
	unless ($connectResult) {
		$srv->logDebug(1, $self->{host}." Can't connect: ".$self->lastError()) if DEBUG1;
		return undef;
	}
	$srv->logDebug(3, $self->{host}." connect:ok") if DEBUG3;

	return $self;
}


##------------------------------------------------------------------------------
# lastError()
# Supplies a string giving details for the last internal error seen in this session
# See Itk::FileTransfer::lastError() for base class details.
# RETURNS: $error
# - $error  Details of last error seen, "0" if none
#------------------------------------------------------------------------------
sub lastError {
	my ($self) = @_;
	return undef unless $self->{ssh};
	
	my @errDetails = $self->{ssh}->error;
	return 0 if $errDetails[0] == 0;  #no error

	#Compile string with last error details
	my $errStr = sprintf("sshErr=%d (%s) %s", @errDetails);
	if ($errDetails[0] == -31 and $self->{sftp}) {  #if is an sftp error (-31 = LIBSSH2_ERROR_SFTP_PROTOCOL)
		my @sftpErrDetails = $self->{sftp}->error;
		$errStr .= sprintf(" / sftpErr=%d (%s) %s", @sftpErrDetails);
	}
	
	return $errStr;
}


##------------------------------------------------------------------------------
# lastErrorWasTimeout()
# Returns true if the last error seen was due to a timeout.
# RETURNS: $result
# - $result  1 if last error was timeout, 0 if different error type, undef if unknown or no last error
#------------------------------------------------------------------------------
sub lastErrorWasTimeout {
	my ($self) = @_;
	return undef unless $self->{ssh};
	my @errDetails = $self->{ssh}->error;
	return undef if $errDetails[0] == 0;  #no error
	#return 1 if is a timeout (-9 = LIBSSH2_ERROR_TIMEOUT, -30 = LIBSSH2_ERROR_SOCKET_TIMEOUT, -1 = LIBSSH2_ERROR_SOCKET_NONE)
	return 1 if ($errDetails[0] == -1 or $errDetails[0] == -9 or $errDetails[0] == -30);
	return 0;  #other error type
}


##------------------------------------------------------------------------------
# login($user, $password)
# See Itk::FileTransfer::login() for base class details.
#------------------------------------------------------------------------------
sub login {
	my ($self, $user, $password) = @_;
	return undef unless $self->{ssh};

	#Warning - auth_list is causing glibc crash in libssh2 0.18
	#my @authList = $self->{ssh}->auth_list($user);  #get available auth methods from remote server

	my $sock = $self->{ssh}->sock() || do{
		$srv->logDebug(1, $self->{host}." No value returned by sock(): ".$self->lastError()) if DEBUG1;
		return undef;
	};
	#test for startup error conditions on the connection (strange states sometimes seen on forwarded ssh ports)
	my @pollErr = ({ handle => $sock, events => ['err','hup','nval'] });
	my $pollErrVal = $self->{ssh}->poll(100, \@pollErr);
	if (not defined $pollErrVal or $pollErrVal > 0) {  #if an error in polling, or any of these events were found
		$srv->logDebug(1, $self->{host}." pollErr returned err:$pollErr[0]->{revents}->{err},hup:$pollErr[0]->{revents}->{hup},nval:$pollErr[0]->{revents}->{nval}") if DEBUG1;
		return undef;
	}
	$srv->logDebug(3, $self->{host}." Poll:err:ok") if DEBUG3;

	$srv->logDebug(3, $self->{host}." attempting authentication...") if DEBUG3;
	my $authMethod = $self->{ssh}->auth(rank=>['password', 'keyboard-auto'], username=>$user, password=>$password) or do{
		$srv->logDebug(1, $self->{host}." Unable to authenticate: ".$self->lastError()) if DEBUG1;
		return undef;
	};
	$srv->logDebug(3, $self->{host}." succeeded with method '$authMethod', now initiating sftp...") if DEBUG3;
	$self->{sftp} = $self->{ssh}->sftp() or do{
		$srv->logDebug(1, $self->{host}." Unable to initiate sftp: ".$self->lastError()) if DEBUG1;
		return undef;
	};

	return 1;
}


##------------------------------------------------------------------------------
# dirStat($dir)
# See Itk::FileTransfer::dirStat() for base class details.
#------------------------------------------------------------------------------
sub dirStat {
	my ($self, $path) = @_;
	return undef if ($self->{_invalidSession} or not $self->{sftp});
	
	$path = $self->{remotewd} unless defined $path;
	my (@output, $fileStat);
	my $sftpdir = $self->{sftp}->opendir($path) or do{
		$srv->logDebug(1, $self->{host}.": opendir($path) failed: ".$self->lastError()) if DEBUG1;
		return undef;
	};
	$srv->logDebug(3, $self->{host}.": Collecting dirStats for $path") if DEBUG3;
	while ($fileStat = $sftpdir->read()) {
		$fileStat->{isdir} = ($fileStat->{mode} & 040000);  #this is a customised entry for efficiency
		$srv->logDebug(4, $self->{host}.": found $fileStat->{name}, isdir=$fileStat->{isdir}") if DEBUG4;
		push(@output, $fileStat);
	}

	return \@output;
}

##------------------------------------------------------------------------------
# ls($dir)
# See Itk::FileTransfer::ls() for base class details.
#------------------------------------------------------------------------------
sub ls {
	my ($self, $path) = @_;
	return undef if ($self->{_invalidSession} or not $self->{sftp});
	
	$path = $self->{remotewd} unless defined $path;
	my (@output, $fileStat);
	my $sftpdir = $self->{sftp}->opendir($path) or do{
		$srv->logError($self->{host}.": opendir($path) failed: ".$self->lastError());
		return undef;
	};
	$srv->logDebug(3, $self->{host}." Collecting file name for $path") if DEBUG3;
	while ($fileStat = $sftpdir->read()) {
		push(@output, $fileStat->{name} );
	}
	return wantarray? @output : \@output;
}


##------------------------------------------------------------------------------
# dir($dir)
# See Itk::FileTransfer::ls() for base class details.
#------------------------------------------------------------------------------
sub dir {
	my ($self, $path) = @_;
	return undef if ($self->{_invalidSession} or not $self->{sftp});
	
	my $dirStatResult = $self->dirStat($path) or return undef;
	my @dirOutput;
	#Fake a "dir" listing for this path
	#FIXME - needs to be completed
	foreach my $dirStatEntry (@$dirStatResult) {
		my $dirLine = sprintf("%s--------- 1 - - %8d MMM DD HH:MM %s",
		    $dirStatEntry->{isdir}? 'd' : '-',
		    $dirStatEntry->{size},
#		    $dirStatEntry->{mtime},
		    $dirStatEntry->{name},
		);
		push(@dirOutput, $dirLine);
	}
	return wantarray? @dirOutput : \@dirOutput;
}


##------------------------------------------------------------------------------
# get($remoteFile, $localFile)
# See Itk::FileTransfer::get() for base class details.
#------------------------------------------------------------------------------
sub get {
	my ($self, $remoteFile, $localFile) = @_;
	$srv->logEnter(\@_) if ENTER;
	return undef if ($self->{_invalidSession} or not $self->{sftp});
	$remoteFile = $self->_checkPath($remoteFile, $self->{remotewd});
	$localFile = $self->_checkPath($localFile, $self->{localwd});

#caused program restart in cello...
#$self->{ssh}->scp_get($remoteFile, $localFile);

	open(OUTFILE, "> $localFile") or do{
		$srv->logDebug(3, $self->{host}." Can't write to $localFile") if DEBUG3;
		return undef;
	};

	my $file = $self->{sftp}->open($remoteFile, IO::File::O_RDONLY) or do{
		$srv->logError($self->{host}.": open($remoteFile) failed: ".$self->lastError());
		my @errDetails = $self->{ssh}->error;
		$self->{_invalidSession} = 1 if $errDetails[0] == -1;  #LIBSSH2_ERROR_SOCKET_NONE, can't continue sftp session after this
		return undef;
	};

	my ($buffer, $count);
	my $bufSize = 32500;  #seems fairly optimal, no gain seen with larger sizes, smaller size increases total transfer time
	while (1) {

# 	Not sure about timeouts in this read loop. Socket loss situations are caught, but all RSG connection statuses ok?
#		if ($self->{timeout}) {
#			my @poll = ({ handle => $sock, events => 'in' });
#			$srv->logDebug(3, $self->{host}." Poll:in") if DEBUG3;
#			return undef unless $self->{ssh}->poll($self->{timeout} * 1000, \@poll) and $poll[0]->{revents}->{in};
#			$srv->logDebug(3, $self->{host}." Poll:in:ok") if DEBUG3;
#		}

		$count = $file->read($buffer, $bufSize);
		$srv->logDebug(5, $self->{host}.": read $count bytes") if DEBUG5;
		last unless defined $count and $count>0;
		print OUTFILE $buffer;
	}
	close(OUTFILE);

	#If the transfer has finished normally, count should be set to zero here
	#We only check error status if last read() has returned undef (Bug #1232)
	unless (defined $count) {
		#check for ssh error
		# Nb. ssh error -31 is LIBSSH2_ERROR_SFTP_PROTOCOL - if this error then you need to check sftp error
		# sftp error 1 is SSH_FX_EOF (An attempt to read past the end-of-file was made) - this is ignored here
		my $ssherr=$self->{ssh}->error;
		if ($ssherr and not ($ssherr == -31 and $self->{sftp}->error == 1)) {
			$srv->logError($self->{host}.": get($remoteFile) failed: ".$self->lastError());
			unlink $localFile;
			return undef;
		}
	}
	$srv->logDebug(3, $self->{host}." got $remoteFile") if DEBUG3;

	return 1;
}

##------------------------------------------------------------------------------
# put($localFile, $remoteFile)
# See Itk::FileTransfer::put() for base class details.
#------------------------------------------------------------------------------
sub put {
	my ($self, $localFile, $remoteFile) = @_;
	$srv->logEnter(\@_) if ENTER;
	return undef if ($self->{_invalidSession} or not $self->{sftp});
	$remoteFile = $self->_checkPath($remoteFile, $self->{remotewd});
	$localFile = $self->_checkPath($localFile, $self->{localwd});
	
	my ($infile, $outfile);
	eval {
		open($infile, $localFile) or
			die $self->{host}." Can't read from $localFile: $!\n";
	
		$outfile = $self->{sftp}->open($remoteFile, IO::File::O_CREAT|IO::File::O_WRONLY) or 
			die $self->{host}.": open($remoteFile) O_CREAT|O_WRONLY failed: ".$self->lastError()."\n";
	
		my ($buffer, $count, $wrcount);
		my $bufSize = 32738;  # Due to bug in libssh, must not send a buffer size >=
				      # 32768 - 25 - 4
				      # See: http://www.libssh2.org/mail/libssh2-devel-archive-2010-02/0168.shtml
		while (1) {
			$count = read($infile, $buffer, $bufSize);
			last unless defined $count and $count>0;
			$wrcount = $outfile->write($buffer);
			last unless defined $wrcount && $wrcount == $count;
			$srv->logDebug(5, $self->{host}.": wrote $wrcount bytes") if DEBUG5;
		}
		
		# Check if we exited due to file read error
		die "Error reading file $localFile: $!" unless (defined $count);
		
		unless (defined $wrcount) {
			#check for ssh error
			my $ssherr=$self->{ssh}->error;
			if ($ssherr) {
				die $self->{host}.": get($remoteFile) failed: ".$self->lastError()."\n";
			}
		}
		
		if ($count>0 && $wrcount != $count) {
			die "Write count $wrcount != read count $count: ssh error=".$self->lastError()."\n";
		}
	};

	if ($@) {
		$srv->logError($@);
		close $infile if $infile;
		return undef;
	}
	
	close $infile;
	$srv->logDebug(3, $self->{host}." put $remoteFile") if DEBUG3;

	return 1;
}

##------------------------------------------------------------------------------
# cwd($dir)
# See Itk::FileTransfer::cwd() for base class details.
#------------------------------------------------------------------------------
sub cwd {
	my ($self, $dir) = @_;
	return undef unless $dir;
	return undef if ($self->{_invalidSession} or not $self->{sftp});
	#check to see that dir exists
	$self->{sftp}->opendir($dir) or do{
		$srv->logError($self->{host}.": opendir($dir) failed: ".$self->lastError());
		return undef;
	};
	$self->{remotewd} = $dir;
	return 1;
}


##------------------------------------------------------------------------------
# delete($file)
# See Itk::FileTransfer::delete() for base class details.
#------------------------------------------------------------------------------
sub delete {
	my ($self, $remoteFile) = @_;
	return undef if ($self->{_invalidSession} or not $self->{sftp});
	$remoteFile = $self->_checkPath($remoteFile, $self->{remotewd});
	my $result = $self->{sftp}->unlink($remoteFile) or do{
		$srv->logError($self->{host}.": unlink($remoteFile) failed: ".$self->lastError());
	};
	return $result;
}


##------------------------------------------------------------------------------
# quit()
# See Itk::FileTransfer::quit() for base class details.
#------------------------------------------------------------------------------
sub quit {
	my ($self) = @_;
	
	$self->{sftp} = undef if defined $self->{sftp};
	if (defined $self->{ssh}) {
		my $sock = $self->{ssh}->sock();
		$sock->close() if defined $sock;
		$self->{ssh}->disconnect();
		$self->{ssh} = undef;
	}
	undef $self->{_invalidSession};

	$srv->logDebug(1, $self->{host}." disconnected: quit()") if DEBUG1;
	return 1;
}

#####################################################################################

package Itk::SFTPexpect;
#############################################################################
# Implements sftp using Expect for Itk FileTransfer functionality

use base 'Itk::FileTransfer';
my $Expect_Available;
BEGIN {
	eval {
		require Expect;
		import Expect;
		$Expect_Available=1;
	};
}

$Expect::Debug = 3 if DEBUG5;  #DEBUG5 turns on detailed expect tracing

use fields (
	'expect',  # 
	'identityFilePath',  # Path to private keyfile to be used for connection
);

use constant {
	PASSWORD_PROMPT => 'assword',  # regex for detecting a password prompt
	KEY_PROMPT => 'yes\/no',  # regex for detecting a key prompt
	NORMAL_PROMPT => '(?:sftp)?> $',  # regex for detecting a normal prompt
	SFTP_BIN => 'sftp',  # location of sftp executable ('/usr/bin/sftp')
};

sub _startExpect {
	my ($self, $cmd) = @_;
	$srv->logDebug(1, "start Expect with '$cmd'") if DEBUG1;
	$self->{expect} = new Expect;
#	$self->{expect}->raw_pty(1);
#	$self->{expect}->stty();
	$self->{expect}->spawn($cmd) || do{
    $self->reportError("Unable to launch session '$cmd'"); 
	  return undef;
	};

	$self->{expect}->log_stdout(DEBUG6 ? 1 : 0);  #turn off STDOUT
#$self->{expect}->log_file("/home/itk/output_expect.log");
	return 1;
}

sub _expectRE {
	my ($self, $regex) = @_;

	unless ($self->{expect}->expect($self->{timeout}, '-re', $regex)) {
	  return undef;
	}
	$srv->logDebug(3, "BEFORE:\n".$self->{expect}->before) if DEBUG3;
	return $self->{expect}->exp_match();  #return string that matched expect
}

sub _sendLine {
	my ($self, $line) = @_;
	$srv->logDebug(3, "Sending '$line'") if DEBUG3;
	$self->{expect}->send($line."\n");
	#now wait for prompt
	return undef unless $self->{expect}->expect($self->{timeout}, '-re', NORMAL_PROMPT);
	my $before = $self->{expect}->before;
	$srv->logDebug(3, "BEFORE:\n".$before) if DEBUG3;
	$before =~ s/^$line[\x0D\x0A]*//;  #remove line sent from output
	return $before unless wantarray;
	#prepare array output if required
	my @output;
	foreach (split(/\n/, $before)) {
  	s/\x0D//g;  #get rid of extra chars
  	push(@output, $_);
	}
	return @output;
}

##------------------------------------------------------------------------------
# new(%options)
# Constructs a new Itk::SFTPexpect object.
# See Itk::FileTransfer::new() for base class details.
# RETURNS: Itk::SFTPexpect object
#------------------------------------------------------------------------------
sub new {
	$srv->logEnter(\@_) if ENTER;
	die "Neither Net::SSH2 nor Expect are available" unless $Expect_Available;
	
	my __PACKAGE__ $self = shift;
	my (%options) = @_;
	$self = fields::new($self) unless (ref $self);
	$self->{identityFilePath} = delete $options{idfile};
	$self->SUPER::new(%options);
#open(STDIN,  "<&Client")   || die "can't dup client to stdin";
#open(STDOUT, ">&Client")   || die "can't dup client to stdout";
#open(STDERR, ">&STDOUT") || die "can't dup stdout to stderr";
	#NB - can't start expect yet because we don't have the user name
	return $self;
}


##------------------------------------------------------------------------------
# login($user, $password)
# See Itk::FileTransfer::login() for base class details.
#------------------------------------------------------------------------------
sub login {
	my ($self, $user, $password) = @_;
	$srv->logEnter(\@_) if ENTER;
	return 1 if $self->{expect};  #return true if already started

	my $sftpcmd = SFTP_BIN;
	$sftpcmd .= " -o IdentityFile=\"$self->{identityFilePath}\"" if defined $self->{identityFilePath};
	$sftpcmd .= " $user\@$self->{host}";
	$srv->logDebug(2, "Launching '$sftpcmd'") if DEBUG2;

	$self->_startExpect($sftpcmd) or return undef;

	my $keyPrompt = KEY_PROMPT;
	my $matchString = $self->_expectRE(PASSWORD_PROMPT.'|'.$keyPrompt);
	unless (defined $matchString) {
    $self->reportError("Unable to connect to $self->{host}"); 
	  return undef;
	}

	if ($matchString =~ /$keyPrompt/) {  #initial ssh connection, need to accept key
	  $self->_sendLine('yes');
	  unless ($self->_expectRE(PASSWORD_PROMPT)) {
	    $self->reportError("Unable to handshake with $self->{host}"); 
	    return undef;
	  }
	}
	return $self->_sendLine($password);  #true if we get a prompt
}


##------------------------------------------------------------------------------
# ls($dir)
# See Itk::FileTransfer::ls() for base class details.
#------------------------------------------------------------------------------
sub ls {
	my ($self, $dir) = @_;
	$srv->logEnter(\@_) if ENTER;
	my $command = "ls $dir";
	my @dirEntries = $self->_sendLine($command);
	return undef unless @dirEntries;
	return wantarray? @dirEntries : \@dirEntries;
}


##------------------------------------------------------------------------------
# dir($dir)
# See Itk::FileTransfer::dir() for base class details.
#------------------------------------------------------------------------------
sub dir {
	my ($self, $dir) = @_;
	$srv->logEnter(\@_) if ENTER;
	my $command = "ls -l $dir";
	my @dirEntries = $self->_sendLine($command);
	return undef unless @dirEntries;
	return wantarray? @dirEntries : \@dirEntries;
}


##------------------------------------------------------------------------------
# get($remoteFile, $localFile)
# See Itk::FileTransfer::get() for base class details.
#------------------------------------------------------------------------------
sub get {
	my ($self, $remoteFile, $localFile) = @_;
	$srv->logEnter(\@_) if ENTER;
	return undef unless $self->{expect};
	$remoteFile = $self->_checkPath($remoteFile, $self->{remotewd});
	$localFile = $self->_checkPath($localFile, $self->{localwd});

	my $output = $self->_sendLine("get $remoteFile $localFile");
	return undef unless defined $output;
	return undef if $output =~ /Couldn't open/;  #file error
	return 1;
}

##------------------------------------------------------------------------------
# put($lcoalFile, $remoteFile)
# See Itk::FileTransfer::put() for base class details.
#------------------------------------------------------------------------------
sub put {
	my ($self, $remoteFile, $localFile) = @_;
	$srv->logEnter(\@_) if ENTER;
	return undef unless $self->{expect};
	$remoteFile = $self->_checkPath($remoteFile, $self->{remotewd});
	$localFile = $self->_checkPath($localFile, $self->{localwd});

	my $output = $self->_sendLine("put $localFile $remoteFile");
	return undef unless defined $output;
	if ($output =~ /Couldn't get handle: (.+)/) {
		$srv->logError("SFTP put error: $1");
		return undef; 
	}
	return 1;
}

##------------------------------------------------------------------------------
# cwd($dir)
# See Itk::FileTransfer::cwd() for base class details.
#------------------------------------------------------------------------------
sub cwd {
	my ($self, $dir) = @_;
	return undef unless $dir and $self->{expect};

	my $output = $self->_sendLine("cd $dir");
	return undef unless defined $output;
	return undef if $output =~ /o such file/;  #if couldn't change directory
	$self->{remotewd} = $dir;
	return 1;
}


##------------------------------------------------------------------------------
# pwd()
# See Itk::FileTransfer::pwd() for base class details.
#------------------------------------------------------------------------------
sub pwd {
	my ($self) = @_;
	return undef unless $self->{expect};

	my $output = $self->_sendLine('pwd');
	return undef if (not defined $output or $output !~ /directory:\s+(\S.*\S)/);
	return $1;
}


##------------------------------------------------------------------------------
# quit()
# See Itk::FileTransfer::quit() for base class details.
#------------------------------------------------------------------------------
sub quit {
	my ($self) = @_;
	return undef unless $self->{expect};
	$self->_sendLine('quit');
	$self->{expect}->soft_close();
	$self->{expect} = undef;
	return 1;
}

#####################################################################################


1; # Modules must return 1
