#!/usr/bin/perl
###----------------------------------------------------------------------------
#
# ServerNitsPack Class. 
# This class provides logging fubctionality in a similiar fashion as to the 
# Server class in a full ITK System. Using this class enables more streamlining 
# and possibilities for code reuse between the nits pack and a full ITK system
#
# == Logging ==
#
# Provides the means to log error, warning, info and debug messages to STDOUT. 
# The actual level of logging performed is determined by DEBUG constants.
#
# (c) Ericsson AB 2006 - All Rights Reserved
#
# No part of this material may be reproduced in any form
# without the written permission of the copyright owner.
# The contents are subject to revision without notice due 
# to continued progress in methodology, design and manufacturing. 
# Ericsson shall have no liability for any error or damage of any
# kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of
# Telefonaktiebolaget LM Ericsson. All other trademarks mentioned
# herein are the property of their respective owners. 
#------------------------------------------------------------------------------
# $Id: NitsPackServer_autotrace.pm 1850 2009-01-06 22:02:11Z eanzmark $

#-------------------------------------------------------------------------------
# Print Stack Trace
# This sub must be in package DB in order to force caller() to provide the variable
# @DB::args. See perldoc for caller()
#-------------------------------------------------------------------------------
package DB;

sub stacktrace { 
	my $start=shift;
	my $trace;
	my $i=$start;
	
	require Data::Dumper;
	import Data::Dumper;
	$Data::Dumper::Terse=1; 
	$Data::Dumper::Indent=0;
	$Data::Dumper::Maxdepth=1;
	$Data::Dumper::Useqq=1;
	
	while (($package, $filename, $line, $sub)=caller($i++)) {
		my @outargs;
		foreach (@args) {
			if (ref $_) {
				push @outargs, "$_";
			} else {
				push @outargs, Dumper($_);
			}
		}
		my $args = join(",",@outargs);
		my $lev = $i-$start;
		$trace .= "\n$lev\t$sub($args)\n\tcalled from $package($filename:$line)\n";
	}
	return $trace;
} 

package Itk::NitsPackServer;
use strict;
use Hash::Util qw(lock_keys);

#==============================================================================
# Initialisation
#==============================================================================
# This sets up the debug environment. Any package that 'uses' this module will
# get a set of debug constants defined.

my (	@trace_groups,		# List of all trace groups
	%trace_group_index, 	# Map of trace groups
	@ITKTRACE		# trace terms: each entry is a ref to [$pkg_match_re, $operator, \@groups]
);

sub import {
	my ($package) = (caller);
	
	# First read all constants into hash %packages
	my %pkgtrace;
	$pkgtrace{$_}=0 foreach @trace_groups;
	if (@ITKTRACE) {
		foreach my $term (@ITKTRACE) {
			my ($pkg_match,$oper,$groups) = @{$term};

			# Does this package match the currently processed package?
			next if ($pkg_match && $package !~ $pkg_match);
			
			foreach my $grp (@{$groups}) {
				# FIX ME: Default of neither a + or - causes uninitialised warnings for oper. Hence needs a default setting. check in original server to see why : TR ?
				$pkgtrace{$grp} = (defined $oper and $oper eq '-') ? 0 : 1;
			}
		}
	}
	# Now turn the %pkgtrace into a set of constants
	# We also calculate the DEBUG constant which is a bitmap of the enabled
	# DEBUGx's
	my $debug=0;
	no strict 'refs';
	foreach my $group (reverse @trace_groups) {
		my $value = $pkgtrace{$group};
		$debug = ($debug << 1) + ($value ? 1 : 0);
		my $fullname = "${package}::$group";
		*$fullname = sub () { $value };
	}
	$debug=$debug << 3;
	my $fullname = "${package}::DEBUG";
	*$fullname = sub() { $debug };
}

BEGIN {
	@trace_groups=qw(DEBUG1 DEBUG2 DEBUG3 DEBUG4 DEBUG5 DEBUG6 DEBUG7 DEBUG8 DEBUG9 ENTER);
	my $n=0;
	%trace_group_index=map { $_ => $n++ } @trace_groups;
	if (exists $ENV{ITKTRACE}) {
		
		print STDERR "ITKTRACE = $ENV{ITKTRACE}\n";
	
		# ITKTRACE can be of the form:
		# [Package_Re=][+-][GROUP1[,GROUP2...][;[Package_Re=][+-][GROUP1[,GROUP2...]...]
		# Package_Re matches a package name
		# + means turn on these trace(s)
		# - means turn off these trace(s)
		# Neither + nor - means turn off all traces except these
		foreach my $spec (split ';',$ENV{ITKTRACE}) {
			my ($pkg_match,$oper,$groups) = ($spec =~ /(?:([^=]+)=)?([+-])?(\w+(?:,\w+)*)/);
			my @groups;
			$groups=uc $groups;
			if ($groups eq 'ALL') {
				@groups = @trace_groups;
			} elsif ($groups eq 'NONE') {
				@groups=();
			} else {
				@groups=split ',',$groups;
			}
			foreach (@groups) { 
				$_="DEBUG$_" if /^[\d]$/; # Allow shorthand: 1 = DEBUG1, etc
				die "itktrace: Invalid trace group: $_\n" unless exists $trace_group_index{$_};
			}
			die "Invalid trace specifier in ITK_TRACE: $spec\n" unless defined $groups;
			push @ITKTRACE, [ $pkg_match ? qr/$pkg_match/i : undef, $oper,\@groups];
		}
	}
	
	# make sure our own constants exist!!
	import();
	
	# Install die handler. There are two possibilities:
	# 1) Stack trace enabled. 
	# If possible, we use Carp to print the
	# whole stack backtrace. Then, if possible, use logError to report
	# the error and exit. If that is not possible then die instead
	#
	# 2) Stack trace disabled.
	# If possible, use logError to report
	# the error and exit. If that is not possible then die instead
	if ($ENV{ITKSTACKTRACE}) {
		$SIG{__DIE__}=sub {
			my ($msg)=@_;
			return if defined $^S && $^S; # Just continue if this is within an eval
			if (defined $^S) {
				$msg=$msg."Stacktrace:\n".DB::stacktrace(2);
				if (defined $::srv) {
					$main::srv->logError($msg, undef, caller());
					exit ($! != 0 ? $! : 255);
				} else {
					die $msg;
				}
			} else {
				die $msg;
			}
		};
	} else {
		$SIG{__DIE__}=sub {
			my ($msg)=@_;
			return if defined $^S && $^S; # Just continue if this is within an eval
			if (defined $^S && defined $::srv) {
				chomp $msg;
				$main::srv->logError($msg, undef, caller());
				exit ($! != 0 ? $! : 255);
			} else {
				die $msg;
			}
		};
	}		
}

#==============================================================================
# CONSTANTS
#==============================================================================
use constant {
	LOGLEVEL_ERROR	=> 0,					# Log level for errors
	LOGLEVEL_WARN	=> 1,					# Log level for warnings
	LOGLEVEL_INFO	=> 2,					# Log level for infos
	LOGLEVEL_DEBUG  => 3,					# Starting Log level for debug messages
								# Note levels 3-11 are reserved for debug levels 1-9
	LOGLEVEL_ENTER  => 12,					# Log level for enter messages
	JOBSTOP_FILE => '/tmp/jobstop',    			# File whose existence indicates whether ITK jobs should be aborted
};
use constant LOGLEVEL_NAMES => qw(ERROR WARN INFO DEBUG1 DEBUG2 DEBUG3 DEBUG4 DEBUG5 DEBUG6 DEBUG7 DEBUG8 DEBUG9 ENTER); # Symbol names for log levels
#==============================================================================
# INSTANCE VARIABLES
#==============================================================================
use fields (
	'defaultNwid',			# Default NWID
	'globalConfig',			# Hash storing global config
	'nitspack_configfile'		# Configuration file specific to nits_pack
);
#==============================================================================
# CLASS VARIABLES
#==============================================================================
#==============================================================================
# CLASS INITIALISATION
#==============================================================================

#==============================================================================
# PRIVATE METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# _logEntry($class, $msg, $nwid, $package, $filename, $line)
# Write an entry to the server log
# - $class		Log Class (See constants above)
# - $msg		Message
# - $nwid		Customer to which the log message applies
# - $package		Package that generated the message (from caller())
# - $filename		File that generated the message (from caller())
# - $line		Line that generated the message (from caller())
# RETURNS: Nothing
#------------------------------------------------------------------------------
sub _logEntry {
	# No logEnter as it might result in recursive calls
	my $self=shift;
	my $class = shift;
        my $msg = shift;
	my $nwid = shift;
	my ($package, $filename, $line) = @_;
	
	# Put the message together. Local server time is used
        my ($ss,$mm,$hh,$dd,$mo,$yy) = localtime(time());
	my $classname = (LOGLEVEL_NAMES)[$class];
	$msg = "$classname: $msg\n";
        my $logout=sprintf("[%04d-%02d-%02d %02d:%02d:%02d] $package($filename:$line) $msg", $yy+1900, $mo+1, $dd, $hh, $mm, $ss);
	
	# New behaivour:
	# DEBUG/ENTER:
	# - Controlled by constants in /etc/itk/trace.conf
	# - Always output to STDERR
	# - Always prefixed with timestamp & localisation info
	# WARN/ERROR/INFO:
	# - always enabled
	# - if called from an interactive program:
	#        - output to STDERR with no prefix
	#        - output to logfile with a prefix
	# - otherwise
	#        - output to STDERR with a prefix
	#        - output to logfile with a prefix
	# Masks no longer used at all
	if ($class >= LOGLEVEL_DEBUG) {
		print STDERR $logout;
	} else {
		if (-t STDERR) {
			print STDERR $msg;
		} else {
			print STDERR $logout;
		}
	}
}

#==============================================================================
# PUBLIC METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# new(%options)
# Constructor   Constructs a new Nits Pack Server Object. this object is to be used 
#               for debug printouts on a Nits Pack system only.
#------------------------------------------------------------------------------
sub new {
	my ($self,%opt);
	if (ref $_[1] eq 'HASH') {
		$self=$_[0];
		%opt=%{$_[1]};
	} else {
		($self, %opt)=@_;
	}
	lock_keys(%opt, qw(nwid config));
	$self=fields::new($self) unless ref $self;
	if (defined $opt{config}) {
		$self->{nitspack_configfile} = $opt{config};
	} 
	return $self;
}



##-----------------------------------------------------------------------------
# logFatal($msg[,$nwid])
# Log an error message, and then die with this message
# - $msg	Message to be logged
# - $nwid	nwid identifies the customer network that the message relates to
# RETURNS: Nothing
#------------------------------------------------------------------------------
sub logFatal {
	my ($self, $msg, $nwid) = @_;
	die "$msg\n";
}

##-----------------------------------------------------------------------------
# logError($msg[,$nwid][,$package][,$filename][,$line])
# Log an error. If package, filename or line are not supplied then they are
# determined from caller() - this is the normal case.
# - $msg	Message to be logged
# - $nwid	nwid identifies the customer network that the message relates to
# - $package	Package that generated the message (from caller())
# - $filename	File that generated the message (from caller())
# - $line	Line that generated the message (from caller())
# RETURNS: Nothing
#------------------------------------------------------------------------------
sub logError {
	# No logEnter as that would be very annoying
	my ($self,$msg,$nwid,@caller)=@_;
	$nwid ||= $self->{defaultNwid};
	@caller=caller() unless @caller;
	$self->_logEntry(LOGLEVEL_ERROR,$msg, $nwid, @caller);
}

##-----------------------------------------------------------------------------
# logWarn($msg[,$nwid])
# Log a warning
# - $msg	Message to be logged
# - $nwid	nwid identifies the customer network that the message relates to
# RETURNS: Nothing
#------------------------------------------------------------------------------
sub logWarn {
	# No logEnter as that would be very annoying
	my $self=shift;
	my $msg=shift;
	my $nwid=shift || $self->{defaultNwid};
	$self->_logEntry(LOGLEVEL_WARN,$msg, $nwid, caller());
}

##-----------------------------------------------------------------------------
# logInfo($msg[,$nwid])
# Log an Info
# - $msg	Message to be logged
# - $nwid	nwid identifies the customer network that the message relates to
# RETURNS: Nothing
#------------------------------------------------------------------------------
sub logInfo {
	# No logEnter as that would be very annoying
	my $self=shift;
	my $msg=shift;
	my $nwid=shift || $self->{defaultNwid};
	$self->_logEntry(LOGLEVEL_INFO,$msg, $nwid, caller());
}

##-----------------------------------------------------------------------------
# logDebug($level, $msg[,$nwid][,@caller])
# Log a debug message. 
# - $level	Debug message level (1-9)
# - $msg	Message to be logged
# - $nwid	nwid identifies the customer network that the message relates to
# - $caller	Caller Info. If not supplied then caller() is used.
# RETURNS: Nothing
#------------------------------------------------------------------------------
sub logDebug {
	# No logEnter as that would be very annoying
	my ($self, $level, $msg, $nwid, @caller)=@_;
	$nwid ||= $self->{defaultNwid};
	@caller = caller() unless @caller;
	# This is an interim measure. Check debug const for caller
	# and if disabled then exit, otherwise proceed
	# In future this will not be necessary since all client code
	# should use DEBUGx constants to compile in/out
	my $ref=$caller[0]."::DEBUG$level";
	{
		no strict 'refs';
		return unless &{$ref}();
	}
	$self->_logEntry(LOGLEVEL_DEBUG+$level-1,$msg, $nwid, @caller);
}

##-----------------------------------------------------------------------------
# logEnter($paramsref[,$nwid])
# Log entry into a subroutine message. Logs the subroutine name and the
# parameter list passed in.
# - $paramsref	Reference to calling parameter list
# - $nwid	nwid identifies the customer network that the message relates to
# RETURNS: Nothing
#------------------------------------------------------------------------------
sub logEnter {
	# No logEnter as that would be recursive and really stupid
	my $self=shift;
	my $p=shift;
	my $nwid=shift || $self->{defaultNwid};
	# See explanation in logDebug for the reason for this
	my $ref=(caller())[0].'::ENTER';
	{
		no strict 'refs';
		return unless &{$ref}();
	}
	my $msg;
	$msg=(caller(1))[3].' Params: <'.join('>,<',@{$p}).'>';
	$self->_logEntry(LOGLEVEL_ENTER,$msg, $nwid, caller());
}


##-----------------------------------------------------------------------------
# setPrintMask()
# Deprecated. Does nothing.
# RETURNS: Nothing
#------------------------------------------------------------------------------
sub setPrintMask {
	$_[0]->logWarn("setPrintMask is deprecated. Use the itktrace command to set tracing");
}

##-----------------------------------------------------------------------------
# getConfig($key[, $nwid])
# Get a specific config item. If a customer specific setting for the key
# exists, then it will be returned, otherwise a global setting will be returned
# - $key	Key
# - $nwid	Customer nwid. Use _global force fetch of global config even
#               if a value might be overridden by a local setting
# RETURNS: $value
# - $value	Value of key. If nwid is supplied, and this key exists in the
#		given customer's namespace, then that value will be returned,
#		otherwise if the key exists in the global namespace, then that
#		value will be returned. If the key does not exist in either
#		namespace then undef is returned.
#		If nwid is not supplied, the default nwid for this server instance
#		will be checked (if any), then the global namespace will be
#		checked.
#------------------------------------------------------------------------------
sub getConfig {
	# No logEnter as it might result in recursive calls
	my $self=shift;
	my $key = shift or die "No Key Supplied";
	##my $nwid = shift || $self->{defaultNwid};
	
	# Check if config already loaded. If not, load it
	$self->_loadConfig() if (not $self->{globalConfig});
	
	# Check if global config is requested specifically
	#if (lc $nwid eq '_global') {
	#	return $self->{globalConfig}{$key} if exists $self->{globalConfig}{$key};
	#	$self->logDebug(1, "Attempt to fetch non-existant key: $key from global itk.conf") if DEBUG;
	#	return undef;
	#}
	
	#$self->_loadConfig($nwid) if ($nwid and not exists $self->{customerConfig}{$nwid});
	
	#return $self->{customerConfig}{$nwid}{$key} if ($nwid and exists $self->{customerConfig}{$nwid}{$key});
	return $self->{globalConfig}{$key} if exists $self->{globalConfig}{$key};
	#$self->logDebug(1, "Attempt to fetch non-existant key: $key from nwid: $nwid") if DEBUG;
	$self->logDebug(1, "Attempt to fetch non-existant key: $key from config file") if DEBUG1;
	return undef;
}
##-----------------------------------------------------------------------------
# _loadConfig([$nwid])
# Loads config data into memory. If possible, loads from cached data, otherwise
# parses config files
# - $nwid	Network ID identifying customer - specific config to be loaded
#		If this is undef, load the global config instead
# RETURNS: Nothing
#------------------------------------------------------------------------------
sub _loadConfig {
	# No logEnter as it might result in recursive calls
	my $self=shift;
	my $nwid=shift;

	##my $etcdir=ITK_ETC;
	##my ($file,$conf_cache_key, $config);
	my ($file, $config);

	# If called with an NWID parameter, then config file will be in ../etc/itk.conf.d/<nwid>.conf
	#if ($nwid) {
	#	$file="$etcdir/itk.conf.d/${nwid}.conf";
	#	$conf_cache_key="config_$nwid";
	#	# Customer specific config stored in self->{customerConfig}{nwid}
	#	$config=$self->{customerConfig}{$nwid}={};
	#} else {
	
		$file=$self->{nitspack_configfile};
		#$conf_cache_key="global_config";
		# Global config stored in self->{customerConfig}{nwid}
		$config=$self->{globalConfig}={};
	#}

	# Config is to be stored in the cache the first time a file is read
	# on subsequent occasions, it is read from the cache. The key used for
	# the cache is 'config/<nwid>' for customer specific config, or
	# 'global_config' for global config.
	# To keep track of wether the cache is up to date or not, when the data
	# is cached the mtime of the file is stored in a separate key with name
	# config_<nwid>_mtime or global_config_mtime
	
	#my $cache=new Cache::FileCache({ namespace => CONFIG_CACHE_NAMESPACE });
	#my $mtime = $cache->get("${conf_cache_key}_mtime");
	#my $newmtime = (stat($file))[9];
	#if ((not defined $mtime) or $mtime < $newmtime) {
		# Config cache was non-existant or older than the last
		# file modification time - [re-]read from the config file
		# and then save in the cache
		_loadConfigFile($file,$config);
	#	$cache->set($conf_cache_key,$config);
	#	$cache->set("${conf_cache_key}_mtime",$newmtime);
	#} else {
		# Cache is up to date. Just load from the cache
	#	if ($nwid) {
	#		$self->{customerConfig}{$nwid} = $cache->get($conf_cache_key);
	#	} else {
	#		$self->{globalConfig} = $cache->get($conf_cache_key);
	#	}
	#}
}
##-----------------------------------------------------------------------------
# _parseConfigFile($file, $config)
# Parses a config file, storing data in the supplied hash
# - $file	Filename of config file
# - $cref	Ref to hash in which it will be stored
# RETURNS:	Nothing.
#------------------------------------------------------------------------------
sub _loadConfigFile {
	# No logEnter as it might result in recursive calls
	my $file = shift;
	my $cref = shift;
	
	die "_loadConfigFile >> nits_pack config file is undefined, perhaps you forgot to pass this parameter at serve creation ??, Aborting"
		unless defined $file;
	
	# If file does not exist, then undef foreach & all!
	return undef unless -e $file;
	
	unless (open FH, $file) {
		die "Could not open config file $file: $!\n";
	}
	
	my ($key, $val, $term);
	while (<FH>) {
		chomp; # Discard line feed at end of line
		next if /^\s*#/; # Skip full-line comments
		$_ =~ s/#.+$//;  # Strip comments from lines
		next if /^\s*$/; # Skip blank lines
		
		# Process multi-line strings. These look like perl 'here' documents
		if (/^\s*([\w\.]+)\s*<<(\w+)$/) { 
			$key=$1;
			$val="";
			my $term=qr/^$2$/; # This expression will match the terminator
					   # on a line by itself
			
			# Read each line, accumulate in $val & terminate when $term is matched
			while (<FH>) {
				last if ($_ =~ $term);
				$val .= $_;
			}
			die "Unterminated multiline string in config file $file on line $.\n" if eof(FH) and $_ !~ $term;
			$cref->{$key}=$val; # Store result
			next;
		}
		
		# Process 'eval' declaration. These look like this:
		# keyname	eval {
		#               .. perl expression..
		# }
		# The perl expression is read in from file, eval'd and the result
		# is stored. Useful for nested data structures
		# Paring similar to block above
		if (/^\s*([\w\.]+)\s*eval\s*{$/) {
			$key=$1;
			$val="";
			while (<FH>) {
				last if (/^}$/);
				$val .= $_;
			}
			die "Unterminated eval string in config file $file on line $.\n" if eof(FH);
			$cref->{$key}=eval "$val";
			die "Error evaluating expression in config file $file on line $.\n$@\n" if $@;
			next;
		}

		# Process 'sub' declaration. These look like this:
		# keyname	sub {
		#               .. perl subrouting..
		# }
		# The perl subrouting is read in from file, eval'd (compiled) and the result
		# is stored. Useful for subs to be defined in config
		# Note: At present Code values cannot be cached! These shall not be
		# used until that is overcome
		# Paring similar to blocks above
#		if (/^\s*(\w+)\s*sub\s*{\s*$/) {
#			$key=$1;
#			$val="";
#			while (<FH>) {
#				last if (/^}$/);
#				$val .= $_;
#			}
#			die "Unterminated sub declaration string in config file $file on line $.\n" if eof(FH);
#			$cref->{$key}=eval "sub { $val }";
#			die "Error compiling sub declaration in config file $file on line $.\n$@\n" if $@;
#			next;
#		}

		($key,$val)=split ' ',$_,2;
		if ($val =~ /^\s*(['"])(.+)\1\s*$/) { 	# Quoted String
			$val=$2;
		} elsif ($val =~ /^\s*([+-]?\d+(?:\.\d+)?)\s*$/) {	# Decimal Number
			$val=$1;
		} elsif ($val =~ /^\s*0[bx]\d+\s*$/) {
			$val = eval ($val);
			if ($@) {
				my ($err)=($@ =~ /(.+)\sat\s\(eval/);
				die "$err in config file $file line $.\n";
			}
		} elsif ($val =~ /^\s*(\w+)\s*$/) {	# Bareword
			$val=$1;
		} else {
			die "Syntax error in config file $file line $.\n";
		}
		$cref->{$key}=$val;
	}
	
	close FH;
}

##-----------------------------------------------------------------------------
# jobstopFileExists(%options)
# Checks for the presence of a file in a predefined location on the server.
# The presence of this file indicates that current jobs should be stopped as
# soon as is convenient, and new jobs should not be started.
# - %options		Currently unused
# RETURNS: boolean value indicating whether file is present
#------------------------------------------------------------------------------
sub jobstopFileExists {
	my ($self, %options) = @_;
	if (-f JOBSTOP_FILE) {
		$self->logInfo('Abort file found! ('.JOBSTOP_FILE.')');
		return 1;
	}
	return 0;
}

##-----------------------------------------------------------------------------
# DESTROY()
# Destructor
#------------------------------------------------------------------------------
sub DESTROY {
	$_[0]->logEnter(\@_) if (ENTER);	
	my $self=shift;
	$self->logDebug(4, "Server object instance destroyed.") if DEBUG4;
}
#------------------------------------------------------------------------------
1; # Modules always return 1!


