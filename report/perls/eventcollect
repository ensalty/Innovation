#!/usr/bin/perl
#------------------------------------------------------------------------------
#
# eventcollect : Collect event based data, parse and store output in spool directory
#
# � Ericsson AB 2006 - All Rights Reserved
#
# No part of this material may be reproduced in any form without the written
# permission of the copyright owner. The contents are subject to revision without
# notice due to continued progress in methodology, design and manufacturing.
# Ericsson shall have no liability for any error or damage of any kind resulting
# from the use of these documents.
#
# Ericsson is the trademark or registered trademark of Telefonaktiebolaget LM
# Ericsson. All other trademarks mentioned herein are the property of their
# respective owners.
#------------------------------------------------------------------------------
# EPA/SR/G/R/U M. Harris - GSDC Australia
# $Id: eventcollect 8021 2013-01-08 02:59:57Z eanzmark $

use strict;
use FindBin qw($Bin);
use lib "$Bin/../lib";
use Getopt::Long;
use File::Path;
use Parallel::ForkManager;
use Time::Local;
use Itk::NitsPackServer;
use Itk::NitsPackNodeDetails;
use Itk::EventData::CollectionController;
use Itk::EventData::FileFetcher;
use Itk::FileUtils;
use Itk::CollectionHistory;
BEGIN {
	if (DEBUG3) {
		require Data::Dumper;
		import Data::Dumper;
	}
}

use constant {
	MAX_PROC_TIME  => 600, # Max amount of time a single node is allowed to take
	TIMEOUT_POLL_INTERVAL => 60, # How often parent process checks for timed out children
};
	
#------------------------------------------------------------------------------
# Global Variables
#------------------------------------------------------------------------------
our $srv;
my (
	# Command line options
	$help,		# Help was requested
	$ipdatabase,	# IP Database file
	$sitefile,	# Site file
	$eventType,	# Type(s) of events to process
	$spooldir,	# Directory for output files
	$spoolPrefix,	# Prefix for output files
	$handler,	# Handler file
	$logStore,	# Temp. Log store Directory
	$transferMethod,# FTP or SFTP transfer method
	$connectionTimeout,	# Time out for FTP operations
	$maxProc,	# Maximum # of parallel processes
	$jobName,	# Name of lockfile
	$nodeFilter,	# Regex to filter nodes to process
	$configFile,	# Location of config file
	$startDate,	# Filter starting date
	$zpm,		# Location of ZPM executable
	$gzip,		# Location of gzip executable
	$nodelete,	# Supress deletion of fetched files
	# Other Globals
	$nodelist,	# Ref to node details for each node
	$nodeNameXform,	# Node Name Transform
	%outputfiles,	# Outputfilenames in hash of prio => [ filename, filepath ]
	$lockfile,	# Locking file used to prevent multiple executions
	$workingDir,	# Working directory for output ($spooldir/working.<pid>
	$errorPath,	# Path in which error'd commits are stored
	$dbfilename,  # name of the historic database
	$dbBackups, # number of db backups to keep
	$rmdumps,	# Flag: Shall we discard PMD / ECDA dumps (true), or transfer them to eits (false)
	$noping,     # ping option
	$localdir,   #local directory
	$fileTransferPort, #port for file Transfer connection
	$shellPort,   # port for shell connection 
	%fdnMapIP, # fdn map with the node ip.
	$fdnFileName, # name of the fdn file 
	   
);

my %eventPriorities = (
	'prio1' => qr/^D;(restart|upgrade|alarm|install)/,
	'prio2' => undef, # Means all unmatched types use this priority
);

my @eventPriorities = sort keys %eventPriorities;
my $loPriority = (grep ! defined $eventPriorities{$_}, @eventPriorities)[0];

#------------------------------------------------------------------------------
# usage
#------------------------------------------------------------------------------
sub usage {
	my $eventtypes = join(', ',@{Itk::EventData::CollectionController::getSupportedEventTypes()});
	print STDERR <<_EOF_;

eventcollect [-h] [-i <ipdatabase>] [-s <sitefile>] [-d <spooldir>] 
        [-spoolprefix <prefix>] [-l <logstore>] [-m <ftp|sftp>] [-connectionTimeout <n>]
        [-lock <lockname>] [-nodematch <regex>] [-handler <file>] [-noping]
        [-c <configfile>] [-startdate] [-zpm <path>] [-nodelete] [-dbfile <filename>]
        [-local <directoty>] [-rmdumps]

Collect event data from nodes, process and store output data in spool directory.

-h
        Show help
        
-i|-ipdatabase
        Specify IP database. If not specified (and not read in config file), 
        then it will be autodetected from moshell in the path.
        Default: '$ipdatabase'
        
-s|-sitefile
        Specify sitefile. If not specified then it is assumed that event data 
        is to be fetched from *all* nodes in the ipdatabase. This option may
        be specified multiple times for multiple site files.
        Default: '$sitefile'
        
-d|-spooldir
        Directory into which output files will be placed for pickup
        Default: '$spooldir'
        
-spoolprefix
        Prefix to be added to output filenames
        Default: '$spoolPrefix'
        
-l|-logstore
        Directory to be used for local storage of logfiles, status files, etc
        Default: '$logStore'

-dbfile 
        name of the historic database file.

-m|-transfermethod
        File Transfer Method: ftp or sftp
        Default: '$transferMethod'
        
-t|eventtype
        Specify event types to process. Comma separated list of one or more
        of the following: $eventtypes
        Defaults to all of the above.

-noping 
        noping command is sent before file collection.
        Defaut: ping option is on 

-timeout
        FTP/SFTP/Telnet/ssh Timeout. 
        Default: '$connectionTimeout'

-fileTransferPort
		Port used for file transfer. Use this parameter if the port differs from the standard.
		i.e. For ftp the port used is 21. For sftp, the port used is 22. 
		
-shellPort
		Port used for file transfer. Use this parameter if the port differs from the standard. 
		i.e. For telnet the port used is 23. For ssh, the port used is 22. 

-maxproc
        Maximum number of parallel processes to fork
        Default: '$maxProc'

-j|job
        Job name. If specified then prevents another instance of eventcollect
        with the same job name from starting.
        Default: '$jobName'

-nodematch
        Regular expression; if supplied then only matching nodes will be processed.
        Default: None

-startdate
        Minimum date, from which data will be collected. This can be used on the
        initial run of eventcollector to limit the amount of data processed.
        PMDs from dates earlier than the given date will not be fetched, and
        processing of other files will skip dates earlier than the given date.
        Default: None

-zpm
        Specify where to find the ZPM executable. If not specified, then ZPM
        will be searched for in the moshell found in the location specified 
        by the MOSHELL_HOME environment variable or in the path.
        Default: '$zpm'

-gzip
        Specify where to find the gzip executable. If not specified then gzip
        must be in the path.
        Default: '$gzip'

-handler
        Load the specified file, which is interpreted as perl code. Within
        this file you may define subroutines that can get called at various
        points in the process:
        
        preRun() : If it exists, this will be called before any nodes are
        processed. You can use this to set up anything which needs to be
        set up prior to fetching any files. This sub should return a true value
        to indicate success - if it returns a false value then processing will 
        be aborted.
        
        preNode(\$node) : If it exists, this will be called before each node is
        processed. \$node is the name of the node to be processed. This sub 
        should return a true value to indicate success - if it returns a false 
        value then processing will be aborted for the given node.
                
        postNode(\$node) : If it exists, this will be called after each node is
        processed. \$node is the name of the node that was processed. No return
        value is expected.
        
        postRun(\$file) : If it exists, this will be called at the end of the
        run. \$file is the output file. You can use this to transfer the file
        back to the remote destination if needed.
        
        nodeNameTransform(\$node) : If present, this will be called for each
        node processed. \$node is the name of the node as found in the
        ipdatabase / sitefile. The return value is used as the node name in
        output files. This is useful when the nodename in the ipdatabase does
        not align with the node name that will be used in ITK.

-nodelete
        If this flag is included, the fetched files will not be deleted at the
        end of the run. The default behaivour is to delete all files after
        processing each node.
        Default: '$nodelete'
	
-rmdumps
        If this flag is included, then the fetched PMDs and ECDAs will be
	discarded. The default behaivour is to transfer them to the EITS.
        Default: '$rmdumps'
        
-c|-config
        Specify config file location
        Default: '$configFile'

-local 
        Specify a directory on the same host where you run xmlcollect. It expects all the 
        the file to be in the same directory.
        
-dbbackups
        The number of hisory db backups to keep and cycle. If set to 0 or 'none', 
        backup will be skipped. The default number is 5.
        
_EOF_
	exit;
}
##-----------------------------------------------------------------------------
# find ipdatabase file
# Will check as follows:
# 1. If there is an environment variable called IPDATABASE, then return that
# 2. If there is an environment variable called MOSHELL_HOME, then return
#	MOSHELL_HOME/sitefiles/ipdatabase
# 3. If there is an moshell in the path, then find its dir and return
#	DIR/sitefiles/ipdatabase
#------------------------------------------------------------------------------
sub find_ipdatabase {
	return $ENV{IPDATABASE} if $ENV{IPDATABASE};
	
	my $moshelldir=$ENV{MOSHELL_HOME};
	unless ($moshelldir) {
		# Search path for moshell
		foreach (split ':', $ENV{PATH}) {
			$moshelldir=$_ and last if -x "$_/moshell";
		}
		return undef unless -d $moshelldir;
	}
	my $ipdatabase="$moshelldir/sitefiles/ipdatabase";
	return undef unless -e $ipdatabase;
	return $ipdatabase;
}
##-----------------------------------------------------------------------------
# find zpm
# Will check as follows:
# 1. If there is an environment variable called MOSHELL_HOME, then check
#	MOSHELL_HOME/commonjars/zpm or MOSHELL_HOME/zpm
# 2. If there is a zpm in the path then return that
# 3. If there is an moshell in the path, then find its dir and return
#	DIR/sitefiles/ipdatabase
#------------------------------------------------------------------------------
sub find_zpm {
	my $moshelldir=$ENV{MOSHELL_HOME};
	unless ($moshelldir) {
		# Search path for moshell
		foreach (split ':', $ENV{PATH}) {
			return "$_/zpm" if -x "$_/zpm";
			$moshelldir=$_ and last if -x "$_/moshell";
		}
		return undef unless -d $moshelldir;
	}
	if (-x "$moshelldir/commonjars/zpm") {
		return "$moshelldir/commonjars/zpm";
	} elsif (-x "$moshelldir/zpm") {
		return "$moshelldir/zpm";
	} else { 
		return undef;
	}
}
##-----------------------------------------------------------------------------
# commitEventRecords
# Copy event records from tmp file into main output file.
# This will be executed by the forked processes so it must lock/unlock the 
# file to ensure there are no collisions.
# - $tmpFile	Name of temporary file
# - $nodeName	Name of node being processed.
#------------------------------------------------------------------------------
sub commitEventRecords {
	my ($tmpFile, $nodeName)=@_;

	# This sub will sort the events into different priority output files
	# according to the definitions in %eventPriorities
	
	my %prioFileHandles;
	
	foreach my $prio (@eventPriorities) {
		my $fh;
		my $outputPath = $outputfiles{$prio}[1];
		open $fh, ">>$outputPath" or
			die "Cannot open $outputPath : $!\n";
	
		flock $fh, 2 or
			die "Unable to get lock on '$outputPath'\n";

		$prioFileHandles{$prio} = $fh;
	}
	
	my $loPrioFh = $prioFileHandles{$loPriority};

	my $ifh;

	open $ifh, $tmpFile or 
		die "Cannot open '$tmpFile' for input: $!\n";

LINE:
	while (<$ifh>) {
		
		foreach my $prio (@eventPriorities) {
			my $prioRegex = $eventPriorities{$prio};
			if ($prioRegex && $_ =~ $prioRegex) {
				my $fh = $prioFileHandles{$prio};
				print $fh $_;
				
				next LINE;
			}
		}
		
		# Fall through to here means lowest priority record
		print $loPrioFh  $_;
	}
	
	close $ifh;
	foreach my $prio (@eventPriorities) {
		flock $prioFileHandles{$prio},8;
		close $prioFileHandles{$prio};
	}

	unless (unlink $tmpFile) {
		$srv->logError("Failure removing temporary file '$tmpFile': $!");
	}
	
}

##-----------------------------------------------------------------------------
# mapFdn
# Load FDN map file
# - $fdnfile	Name of FDN Mapping file
#------------------------------------------------------------------------------
sub mapFdn{
	my ($fdnfile) = @_;
		
	%fdnMapIP = ();
 	open( MAP, $fdnfile ) or die( "FdnMapFile was specified with bad path! ".$!."\n" );
	while( my $line = <MAP> ){
		chomp($line);
		my @stmp = split( /\s+/, $line );
		if( !defined $fdnMapIP{ $stmp[0] } ){
			## Map format is 
			$fdnMapIP{ $stmp[0] } = $stmp[1];
		}
	}
	close( MAP );
	print ">>>> Cached ".scalar ( keys( %fdnMapIP ))." nodes.\n"; 
}


#------------------------------------------------------------------------------
# MAIN
#------------------------------------------------------------------------------

GetOptions(
    'h|help' 			=> \$help,
	'i|ipdatabase=s'	=> \$ipdatabase,
	's|sitefile=s@'    	=> \$sitefile,
	't|eventtype=s'   	=> \$eventType,
	'd|spooldir=s'		=> \$spooldir,
	'spoolprefix=s'		=> \$spoolPrefix,
	'dbfile=s'			=> \$dbfilename,
	'l|logstore=s'		=> \$logStore,
	'm|transfermethod=s'  => \$transferMethod,
	'connectionTimeout=s' => \$connectionTimeout,
	'maxproc=s'			=> \$maxProc,
	'j|job=s'			=> \$jobName,
	'nodematch=s'		=> \$nodeFilter,
	'handler=s' 		=> \$handler,
	'c|configfile=s'	=> \$configFile,
	'startdate=s'		=> \$startDate,
	'zpm=s'			=> \$zpm,
	'gzip=s'		=> \$gzip,
	'nodelete'		=> \$nodelete,
	'rmdumps'		=> \$rmdumps,
	'noping'        => \$noping,
	'local=s'       => \$localdir,
	'shellPort=s'   => \$shellPort,
	'fileTransferPort=s' => \$fileTransferPort,
	'dbbackups'    => \$dbBackups,
	'fdnFile=s'     => \$fdnFileName,	
) or usage();

$configFile ||= "$Bin/../etc/itk.conf";
$srv=new Itk::NitsPackServer(config => $configFile);

# FIXME: Add defaults / errors
$ipdatabase ||= $srv->getConfig("ipdatabase") || find_ipdatabase();
$zpm ||= $srv->getConfig("zpm") || find_zpm();
$gzip ||= $srv->getConfig("gzip") || 'gzip';
$sitefile ||= $srv->getConfig("sitefile");
$spooldir ||= $srv->getConfig("spooldir") || "$ENV{HOME}/spool/event";
$spoolPrefix ||= $srv->getConfig("spoolprefix");
$handler ||= $srv->getConfig("event.handler");
$logStore ||= $srv->getConfig("event.logstore") || "$ENV{HOME}/logstore/event";
$connectionTimeout ||= $srv->getConfig("ftptimeout") || $srv->getConfig("connectionTimeout") || 30;
$transferMethod ||= ($srv->getConfig("transfermethod") || "ftp");
$maxProc ||= $srv->getConfig("event.maxproc") || 50;
$jobName ||= $srv->getConfig("event.jobname") || 'eventcollect' ;
$nodelete ||= $srv->getConfig("event.nodelete");
$dbfilename ||= $srv->getConfig("event.dbfile") || "event.db";
$rmdumps ||= $srv->getConfig("event.rmdumps") ;

usage() if $help;
die "No ipdatabase specified and could not find one\n" unless $ipdatabase;
die "ZPM Cannot be found\n" unless $zpm;

exit if ($srv->jobstopFileExists);




# Load handler subs, if any
if ($handler) {
	unless (my $return = do $handler) {
		    die "Couldn't compile $handler: $@\n" if $@;
		    die "Couldn't read $handler: $!\n" unless defined $return;
		    warn "Initialisation of '$handler' was not successful\nReminder: Modules need to return true!\n" unless $return;
	}
}

if (exists $main::{preRun}) {
	unless (&{$main::{preRun}}()) {
		$srv->logInfo("preRun handler returned false - this run is aborted");
		exit;
	}
}

# Get list of nodes to work with and filter them if necessary
$nodelist = Itk::NitsPackNodeDetails::getNodeDetails(
	ip_database => $ipdatabase,
	sitefile => $sitefile,
	nodename_transform => (exists $main::{nodeNameTransform}) ? $main::{nodeNameTransform} : undef
);

$nodeFilter = qr/$nodeFilter/ if defined $nodeFilter;

# Parse supplied startdate into unix epoch timestamp
if ($startDate) {
	my ($yy,$mm,$dd);
	unless (($yy,$mm,$dd) = ($startDate =~ /^(\d{4})-(\d{2})-(\d{2})$/)
		and ($mm > 0)
		and ($mm <= 12) ) {
	
		die "Invalid startdate: use format yyyyy-mm-dd";
	}
	
	$startDate = timegm(0,0,0,$dd,$mm-1,$yy);
}

$srv->logInfo("eventcollect starting: pid=$$, jobname=".($jobName || '(none)'));

if (DEBUG1) {
	my @runparams = qw(ipdatabase sitefile spooldir spoolPrefix handler logStore transferMethod timeout maxProc jobName nodeFilter);
	my $paramString = join "\n\t", map "$_='".eval("\$$_")."'", @runparams;
	$srv->logDebug(1,"Running using paramaters:\n\t".$paramString);
}

# obtain a lock to ensure that no other copy is running under this job name
if ($jobName) {
	$lockfile="/tmp/itklock-eventcollect-$jobName";
	open LOCK, ">$lockfile" or die "Cannot open $lockfile : $!";
	unless (flock LOCK, 6) {
		$srv->logError("Another instance of pmdaily is already running. This instance will abort");
		die "Unable to get lock: program aborted\n";
	}
}

# Make sure spool directory exists. NB. mkpath dies if it fails
# and will also attempt to create all intermediate dirs
mkpath "$spooldir/ready" unless -d "$spooldir/ready";
$workingDir="$spooldir/working.$$";
mkpath $workingDir unless -d $workingDir;
mkpath "$spooldir/error" unless -d "$spooldir/error";

# Make sure store dir exists
mkpath $logStore unless -d $logStore;

# do a backup of history db here
unless ($dbBackups =~ /^(0|none)$/) {
	Itk::FileUtils::backupHistoryFile($logStore, $dbfilename, $dbBackups);
} else {
	$srv->logDebug(1, "Backing up history database skipped due to -dbbackups set to $dbBackups.");
}

my ($ss,$mm,$hh,$dd,$mo,$yy)=localtime(time);
my $zpmVersion = undef;
if (-e $zpm) {
	$zpmVersion = `$zpm -v 2>&1`;
	chomp ($zpmVersion);
} 

# Create one output file per priority.
# Insert a header into each file
foreach my $prio (@eventPriorities) {
	
	my $outputfile=sprintf("${spoolPrefix}event_%04d%02d%02d_%02d%02d_%s.ied",$yy+1900,$mo+1,$dd,$hh,$mm,$prio);
	my $outputPath="$workingDir/$outputfile";
	$srv->logDebug(1,"Output file: Prio $prio File: $outputfile") if DEBUG1;

	# Write headers to the output file. Do this before any forking so that it only gets done once
	my $fh;
	open $fh, ">$outputPath" or die "Cannot open $outputfile : $!";

	# write a version
	if (!defined ($zpmVersion)) {
		print $fh Itk::EventData::EventRec::Base->version . "\n";
	} else {
		print $fh Itk::EventData::EventRec::Base->version . ";" .  $zpmVersion . "\n";
	}

	Itk::EventData::EventRec::Restart->outHdr($fh);
	Itk::EventData::EventRec::Integrity->outHdr($fh);
	Itk::EventData::EventRec::Install->outHdr($fh);
	Itk::EventData::EventRec::Upgrade->outHdr($fh);
	Itk::EventData::EventRec::Audit->outHdr($fh);
	Itk::EventData::EventRec::Alarm->outHdr($fh);
	Itk::EventData::EventRec::ShellAudit->outHdr($fh);

	close $fh;
	undef $fh;
	
	$outputfiles{$prio} = [ $outputfile, $outputPath ];
}

my $errorPath=sprintf("$spooldir/error/event_error_%04d%02d%02d_%02d%02d",$yy+1900,$mo+1,$dd,$hh,$mm,$ss);

if (exists $main::{preRun}) {
	&{$main::{preRun}}();
}

if (defined ($fdnFileName)) {	
	mapFdn ($fdnFileName);
}


my $dumpdir = ($rmdumps) ? undef : "$spooldir/ready";

# Initialise the modules
Itk::EventData::FileFetcher::init(timeout => $connectionTimeout, storedir => $logStore);

# Can survive a fork, so may as well create it before the fork

my $controller = undef;

if (defined ($localdir)) {
  $srv->logInfo("eventcollect is running local mode on directory : $localdir");
  $controller = new Itk::EventData::CollectionController(localdir=>$localdir, eventTypes => $eventType,storedir => $logStore, zpm=>$zpm, nodelete => $nodelete, dbfilename=> $dbfilename, noping=> $noping, dumpdir => $dumpdir);  
} else {
  $controller = new Itk::EventData::CollectionController(eventTypes => $eventType,storedir => $logStore, zpm=>$zpm, nodelete => $nodelete, dbfilename=> $dbfilename, noping=> $noping, dumpdir => $dumpdir);
}	
my $pm = new Parallel::ForkManager($maxProc); 
my @nodelist = sort keys %{$nodelist};
@nodelist = grep $_ =~ $nodeFilter, @nodelist if $nodeFilter;
my ($nodesStarted,$nodesComplete,%childStartTimes);
my $totalNodes = scalar @nodelist;
$pm->run_on_start(
	sub { 
		my ($pid,$nodeId)=@_;
		$nodesStarted+=1;
		$childStartTimes{$pid}=time;
		my $nodesRemaining = $totalNodes-$nodesComplete;
		print ">>>>> Processing started for '$nodeId', Process ID '$pid' : total($totalNodes), running($nodesStarted), complete($nodesComplete), remaining($nodesRemaining)\n" if -t STDOUT;
		$srv->logDebug(1, "Processing started for '$nodeId', Process ID '$pid' : total($totalNodes), running($nodesStarted), complete($nodesComplete), remaining($nodesRemaining)") if DEBUG1;
	}
);

$pm->run_on_finish(
	sub { 
		my ($pid, $exit_code, $nodeId, $exitSig) = @_;
		$nodesStarted-=1;
		$nodesComplete+=1;
		my $nodesRemaining = $totalNodes-$nodesComplete;
		if ($exitSig) {
			$srv->logError("Child process terminated on signal $exitSig");
		}
		print ">>>>> Processing ended   for '$nodeId', Process ID '$pid', Runtime $childStartTimes{$pid}s: total($totalNodes), running($nodesStarted), complete($nodesComplete), remaining($nodesRemaining)\n" if -t STDOUT;
		$srv->logDebug(1,"Processing ended   for '$nodeId', Process ID '$pid', Runtime $childStartTimes{$pid}s: total($totalNodes), running($nodesStarted), complete($nodesComplete), remaining($nodesRemaining)") if DEBUG1;
		delete $childStartTimes{$pid};
	}
);


# To guard against hanging processes, we keep track of how long each child has
# been running and kill any that take longer than MAX_PROC_TIME
$SIG{ALRM}=sub {
	my $t=time;
	if (DEBUG3) {
		my @lines = map sprintf("\t$_ => %d s",$t - $childStartTimes{$_}), keys %childStartTimes;
		$srv->logDebug(3, "Timeout Check: current process run times:\n".join("\n",@lines));
	}
	foreach my $pid (keys %childStartTimes) {
		if ($t - $childStartTimes{$pid} > MAX_PROC_TIME) {
			$srv->logError("Process pid=$pid exceeded MAX_PROC_TIME - killing it");
			kill 15, $pid;
		}
	}
	alarm TIMEOUT_POLL_INTERVAL;
};

alarm TIMEOUT_POLL_INTERVAL;

sub cleanupAndExit {
	my ($exitCode) = @_;
	
	my @outFiles;
	foreach my $prio (@eventPriorities) {
		my ($outputfile, $outputPath) = @{$outputfiles{$prio}};
		
		# gzip the output
		if (system("$gzip $outputPath")) {
			$srv->logError("Possible error zipping the output file");
		} else {
			$outputPath = "$outputPath.gz";
		}
		
		# Move output file to 'ready'
		my $finalFile = "$spooldir/ready/$outputfile.gz";
		unless (rename $outputPath, $finalFile) {
			$srv->logError("Could not move '$outputPath' to '$finalFile': $!");
		}
		
		push @outFiles, $finalFile;
		
	}
	
	# Clean working dir - issue#3227
	# First remove any temp node files (from nodes started but not finished properly).
	# Any data in these will not have been "committed", and will need to be collected next run.
	system("rm -Rf $workingDir/*.tmp");
	# Now working dir should be empty
	unless (rmdir $workingDir) {
		$srv->logError("Could not remove '$workingDir': $!");
	}
	
	if (exists $main::{postRun}) {
		&{$main::{postRun}}(@outFiles);
	}
	
	# Done. Release Lock
	if ($jobName) {
		flock LOCK, 8;
		close LOCK;
		unlink $lockfile;
	}

	$srv->logInfo("eventcollect run ".($exitCode? 'aborted' : 'completed').": pid=$$, jobname=".($jobName || '(none)'));
	exit $exitCode if $exitCode;
	exit;
}

my $mainPid = $$;
sub jobInterrupted {
	# Only call the cleanup routine for the main process
	# If a sig is received for a child, we just exit (eg timeout - issue#3227)
	exit unless $$ eq $mainPid;
	
	# Give children a chance to finish (otherwise they can append to the
	# output file after the main process has cleaned it up)
	sleep 2;
	cleanupAndExit(-1);
}
# connect signal handlers
$SIG{'KILL'} = \&jobInterrupted;
$SIG{'TERM'} = \&jobInterrupted;
$SIG{'INT'} = \&jobInterrupted;

NODE:
foreach my $nodeName (@nodelist) {
	my $nodeInfo = $nodelist->{$nodeName};
	
	last NODE if ($srv->jobstopFileExists);
	
	$srv->logDebug(1,"Trying node $nodeName") if DEBUG1;
	$srv->logDebug(3,"Node Info: ".Dumper($nodeInfo)) if DEBUG3;
	
	if (exists $main::{preNode}) {
		unless (&{$main::{preNode}}($nodeName)) {
			$srv->logInfo("preNode handler returned false for $nodeName - this node is skipped");
			next NODE;
		}
	}
	
	# Fork a new process. Parent loops (pid=undef) and child continues
	my $pid = $pm->start($nodeName) and next;
	
	my $tmpFile = "$workingDir/$nodeName.tmp";
	
	eval {
		
		my $progressCallback = sub {
			my ($status)=@_;
			# Modify command name so that ps command shows current status
			$0 = "eventcollect: $nodeName $status";
		};
		
		my $ip = $nodeInfo->{ip_address};
		
		# Data is written into a temporary file per node. Once we are sure
		# that the processing has successfully completed, we copy it into
		# the main output file. This prevents problems if the process dies for this node
		# Records are either all written, or none of them are.
		my $fh;
		unless (open $fh, ">$tmpFile") {
			die "Could not open '$tmpFile' for writing: $!\n";
		}

		my $callback = sub {
			my $rec=shift;
			# Discarding all records from 1970, or in the future
			my $ts = $rec->get('time');
			if ($ts <= 79833599) {
				$srv->logWarn("NODE: $rec->{nodename}, TYPE: [$rec->{nodetype}] - Record discarded due to timestamp in the 70's (stop living in the past, man)");
				return;
			} elsif ($ts > time + 86400) {
				$srv->logWarn("NODE: $rec->{nodename}, TYPE: [$rec->{nodetype}] - Record discarded due to timestamp in the future");
				return;
			}
			$rec->out($fh);
		};

		# assign shell port
		$nodeInfo->{shellPort} = $shellPort;
		$nodeInfo->{transferMethod} = $transferMethod;
		$nodeInfo->{port} = $fileTransferPort;
		$nodeInfo->{connectionTimeout} = $connectionTimeout;
					
		if (defined (%fdnMapIP)) {
			if (!defined $fdnMapIP{$nodeName} ) {
				$srv->logWarn("cannot map fdn with ". $nodeName);
			} else {
				$nodeInfo->{mapFdn} = $fdnMapIP{$nodeName};
			}
		}
		
		my $ch = new Itk::CollectionHistory(
			node => $nodeInfo->{itk_nodename},
			storedir => $logStore,
			dbfilename => $dbfilename
		);
		
		$controller->run($nodeInfo, $callback, progress => $progressCallback, startdate => $startDate, collhist=>$ch);

		close $fh;
		
		# Commit copies the records from the temp file into the output file
		commitEventRecords($tmpFile, $nodeName);
		
	};

	# If we did not succeed with the commit then we keep the record file for
	# later analysis
	if ($@) {
		$srv->logError("Error processing node $nodeName: $@");
		if (rename $tmpFile, "${errorPath}_$nodeName.ied") {
			$srv->logInfo("Uncommitted data was stored in '${errorPath}_$nodeName.ied'");
		} else {
			$srv->logError("Error saving uncommitted data: $!");
		}
	}

	if (exists $main::{postNode}) {
		&{$main::{postNode}}($nodeName);
	}
	
	$pm->finish; # perform exit in the child process

}
$pm->wait_all_children;	# Do not allow the program to terminate until all child processes are finished

cleanupAndExit();

