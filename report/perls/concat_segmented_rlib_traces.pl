#!/usr/bin/perl -w 
#
# File name: concat_segmented_rlib_traces.pl
#
# Description:
#   This script is used to concatenate the long segmented traces that are
#   produced by RlibTrace in Trace & Error logs. The output from the
#   script is sent to STDOUT.
#
# Usage:
#   Either of the following syntaxes can be used:
#     cat <trace_log> | concat_segmented_rlib_traces.pl
#   or
#     concat_segmented_rlib_traces.pl <trace_log> 
#
# NOTE: In case the trace_log is in PC format, i.e. with CR-LF for newline
#       instead of LF only, the script may fail.
#       Then try the following command instead:
#
#     dos2unix <trace_log> | concat_segmented_rlib_traces.pl
#
# Author: Johan Lundvall
#
# Revision History:
# 2004-10-20  First version (qralund)
# 2005-07-08  Added handling of alternative input data according to example 2 (ejohalu)
# 2007-02-23  Added improved handling input data according to example 1:
#             Concatenates board ID with key to avoid mixing traces from modules (ejohalu)
# 2008-06-26  Updated script to be able to handle CELL_TRACE_OBJ traces and RNC_EXCEPTION traces
# 2009-06-22  Updated script to be able to handle prinouts from "lh" commands in Moshell. (EMARALD)
# 2009-08-17  Added removal of ^M character at end-of-line to handle dos-format files directly (ejohalu)

use strict;

# ==================== Input data example 1 ====================================
# A segmented trace generates at least three traces as in the example below:
# [2004-10-13 13:29:35.892] 001800/rnhRnsapRoHndlC ../src/RlibTraceTraceD.cpp:720 INFO:< Segmenting traces for key 1209948936, 11 parts will follow >
# [2004-10-13 13:29:35.892] 001800/rnhRnsapRoHndlC ../src/RlibTraceTraceD.cpp:729 INFO:<key 1209948936 part 1>"rnhRnsapRoHndlC: signal data: RoamIfFroIurLinkRoExtSigGetAttribListRspD{rncModuleId -1,noFro 2,froIdList{size 2,array{RTInteger 0,RTInteger 1}},attribList{size 2,array{RoamIfFroIurLinkRoExtAttribD{mcc 240,mnc 99,mncLength 2,rncId 302,synchTimeout 2,synchRetransmissions 2,aliasPlmnIdentities{size 30,array{RoamIfFroIurLinkRoExtPlmnIdentitiesD{mcc -1,mnc -1,mncLength -1},RoamIfFroIurLinkRoExtPlmnIdentitiesD{mcc -1,mnc -1,mn"
# ... part 2 - (n-1), n = 11 in this case, same format as part one
# [2004-10-13 13:29:35.892] 001800/rnhRnsapRoHndlC ../src/RnhRnsapRoHndlC.cpp:1628 TRACE7:<key 1209948936 part 11>"1},RoamIfFroIurLinkRoExtPlmnIdentitiesD{mcc -1,mnc -1,mncLength -1},RoamIfFroIurLinkRoExtPlmnIdentitiesD{mcc -1,mnc -1,mncLength -1}}},noOfAliasPlmnIdentities 0}}}}"

# ==================== Input data example 2 ====================================
# A segmented trace generates at least three traces as in the example below:
# [2004-10-13 13:29:35.892] ../src/RlibTraceTraceD.cpp:720 INFO:< Segmenting traces for key 1209948936, 11 parts will follow >
# [2004-10-13 13:29:35.892] ../src/RlibTraceTraceD.cpp:729 INFO:<key 1209948936 part 1>"rnhRnsapRoHndlC: signal data: RoamIfFroIurLinkRoExtSigGetAttribListRspD{rncModuleId -1,noFro 2,froIdList{size 2,array{RTInteger 0,RTInteger 1}},attribList{size 2,array{RoamIfFroIurLinkRoExtAttribD{mcc 240,mnc 99,mncLength 2,rncId 302,synchTimeout 2,synchRetransmissions 2,aliasPlmnIdentities{size 30,array{RoamIfFroIurLinkRoExtPlmnIdentitiesD{mcc -1,mnc -1,mncLength -1},RoamIfFroIurLinkRoExtPlmnIdentitiesD{mcc -1,mnc -1,mn"
# ... part 2 - (n-1), n = 11 in this case, same format as part one
# [2004-10-13 13:29:35.892] ../src/RnhRnsapRoHndlC.cpp:1628 TRACE7:<key 1209948936 part 11>"1},RoamIfFroIurLinkRoExtPlmnIdentitiesD{mcc -1,mnc -1,mncLength -1},RoamIfFroIurLinkRoExtPlmnIdentitiesD{mcc -1,mnc -1,mncLength -1}}},noOfAliasPlmnIdentities 0}}}}"

# ==================== Design consideration, based on input data example =============
# The following characteristics of these traces must be considered:
# - traces for different key values may be interleaved in the log
# - key value is not unique in the log, it is only unique for traces that are interleaved
#   due to parallel execution
# - Some segmented traces contains line breaks inside the segments due to the fact
#   that type descriptors print binary data using put_uchar Ascii encoding, for
#   example in CpxU8ArraySC. These lines must be concatenated before analysis.

# For each trace, there is a need to store
# - The key value
# - The number of trace segments (parts) that build up the complete trace
# - The contents of each trace segment
# - The date & time for the first trace segment (to be used in output)
# - The trace object for the last trace segment (to be used in output)
# - The file and line info for the last trace segment (to be used in output)
# - The trace group for the last trace segment (to be used in output)

package TraceData;

# Constructor, only supported as class method
# Supply the following paramaters:
#    keyVal => <key value>
#    noOfParts => <number of parts>
#    dateTime => "[yyyy-mm-dd hh:mm:ss.ms_]",
sub TraceData::new
{
  my $class = shift @_;
  my $this = {
	      # Default member values in constructor
	      keyVal => 0,
	      noOfParts => 0,
	      parts => [],
	      moshell_board => "",
	      dateTime => "Date_Time_not_set",
	      traceObj => "Trace_object_not_set",
	      fileLine => "File:line_not_set",
	      traceGroup => "TRACE_group_not_set:",
	      # Override default values from constructor arguments
	      @_,
	      # Default member values in constructor that can not be overridden by constructor arguments
	      currentNoOfParts => 0
	     };
  return bless $this, $class;
}

# Add a trace segment. Supply the trace segment number and segment string and as parameters
sub TraceData::addSegment
{
  my $this = shift @_;
  my $segNo = shift @_;
  my $lineNoRef = shift @_;
  my $expectedSegNo = ($this->{currentNoOfParts}) + 1;
  if (! $this->allSegmentsAdded())
  {
    if ($segNo == $expectedSegNo )
    {
      push(@{$this->{parts}}, @_);
      ($this->{currentNoOfParts})++;
    }
    else
    {
      print STDOUT TraceConcatenation::getProgName() . ": Error: Line $$lineNoRef: Tried to add wrong segment number " . $segNo . " for trace key " . $this->{keyVal} . ". Expected segment number " . $expectedSegNo. "\n";
    }
  }
  else
  {
    print STDOUT TraceConcatenation::getProgName() . ": Error: Line $$lineNoRef: Tried to add too many segments for trace key " . $this->keyVal . "\n";
  }
}

# Set trace object. Supply the trace object from the last trace segment for the key.
sub TraceData::setTraceObj
{
  my $this = shift @_;
  $this->{traceObj} = shift @_;
}

# Set file and line info. Supply the line and file info from the last trace segment for the key.
sub TraceData::setFileLine
{
  my $this = shift @_;
  $this->{fileLine} = shift @_;
}

# Set trace group. Supply the trace group from the last trace segment for the key.
sub TraceData::setTraceGroup
{
  my $this = shift @_;
  $this->{traceGroup} = shift @_;
}

# Check whether or not all expected segments have been added.
sub TraceData::allSegmentsAdded
{
  my $this = shift @_;
  return $this->{currentNoOfParts} == $this->{noOfParts};
}

# Print out the concatenated trace.
sub TraceData::printData
{
  my $this = shift @_;
  if (! $this->allSegmentsAdded())
  {
    print STDOUT TraceConcatenation::getProgName() . ": Error: Tried to print incompletely concatenated trace for trace key " . $this->{keyVal} . "\n";
  }
  else
  {
    # Any ':' charachter is now part of the traceGroup field, do not add a ':' in the printout after traceGroup
    print STDOUT
      $this->{moshell_board} .
        $this->{dateTime} . " " .
          $this->{traceObj} . " " .
            $this->{fileLine} . " " .
              $this->{traceGroup}  .
                join('', @{$this->{parts}}) . "\n";
  }
}

package TraceConcatenation;

my $dateTimeFormat = qr(\[\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}\.\d{3}\]);
my $progName = undef;

sub createNewObject($$$$$)
{
  my ($objHashRef, $dateTime , $keyVal, $noOfParts, $moshell_board) = @_;

  my $obj = TraceData->new(
    keyVal => $keyVal,
    noOfParts => $noOfParts,
    moshell_board => $moshell_board,
    dateTime => $dateTime);
  # Add to container hash, use keyVal as key. First take out any incomplete object still
  # in the container for the same key
  my $oldObj = $$objHashRef{$keyVal};
  $$objHashRef{$keyVal} = $obj;

  if (defined($oldObj))
  {
    $oldObj->printData();
  }
}

sub addSegmentToObject($$$$$$$$$)
{
  my ($lineNoRef, $objHashRef, $dateTime, $traceObj, $fileLine, $traceGroup, $keyVal, $segNo, $segment) = @_;

  my $obj = $$objHashRef{$keyVal};
  if (defined($obj))
  {
    # Add the segment
    $obj->addSegment($segNo, $lineNoRef, $segment);

    # Check if it was the last segment
    if ($obj->allSegmentsAdded())
    {
      # Store the info from last trace part and then print the concatenated trace
      $obj->setTraceObj($traceObj);
      $obj->setFileLine($fileLine);
      $obj->setTraceGroup($traceGroup);
      $obj->printData();

      # Remove reference to this trace object
      $$objHashRef{$keyVal} = undef;
    }
  }
  else
  {
    print STDOUT getProgName() . ": Error: Line " . $$lineNoRef . ": No object found for trace key " . $keyVal . ", start of this key value is missing\n";
  }
}

# Parse one line from T&E log
sub parseLine ($$$)
{
  my $printLine = 0;
  my $returnLineForReparsing = "";

  my ($line, $lineNoRef, $objHashRef) = @_;
  chomp ($line);
  $line =~ s/\r$//;   #remove ^M 
  my $moshell_board = "";
  if ($line =~/(^\d{4}(SP[0-9])*: )/)
  {
  	$moshell_board = $1;
  }
  
  # Some segment lines contain line feed (or other line breaking) characters. They must be
  # concatenated with next line before further processing. Continue concatenation until the
  # last line ends with a citation mark ("). Fill in "\n" for the "replaced" line break.
  # However, if the next line starts with a dateTimeFormat, it should not be concatenated
  # but rather be processed as a normal line
  # Example lines
  # [2004-10-13 13:29:35.892] 001800/rnhRnsapRoHndlC ../src/RlibTraceTraceD.cpp:729 INFO:<key 1209948936 part 1>"rnhRnsapRoHndlC: signal data: RoamIfFroIurLinkRoExtSigGetAttribListRspD{rncModuleId -1,noFro 2,froIdList{size 2,array{RTInteger 0,RTInteger 1}},attribList{size 2,array{RoamIfFroIurLinkRoExtAttribD{mcc 240,mnc 99,mncLength 2,rncId 302,synchTimeout 2,synchRetransmissions 2,aliasPlmnIdentities{size 30,array{RoamIfFroIurLinkRoExtPlmnIdentitiesD{mcc -1,mnc -1,mncLength -1},RoamIfFroIurLinkRoExtPlmnIdentitiesD{mcc -1,mnc -1,mn"
  # [2004-10-13 13:29:35.892] ../src/RlibTraceTraceD.cpp:729 INFO:<key 1209948936 part 1>"rnhRnsapRoHndlC: signal data: RoamIfFroIurLinkRoExtSigGetAttribListRspD{rncModuleId -1,noFro 2,froIdList{size 2,array{RTInteger 0,RTInteger 1}},attribList{size 2,array{RoamIfFroIurLinkRoExtAttribD{mcc 240,mnc 99,mncLength 2,rncId 302,synchTimeout 2,synchRetransmissions 2,aliasPlmnIdentities{size 30,array{RoamIfFroIurLinkRoExtPlmnIdentitiesD{mcc -1,mnc -1,mncLength -1},RoamIfFroIurLinkRoExtPlmnIdentitiesD{mcc -1,mnc -1,mn"
  # 2008-06-26: Let any data before <key xxx part yyy>, including any ':' be part of trace group field. In this RNC_EXCEPTION trace and cell-traces can also be handled
  if (($line =~ /($dateTimeFormat)\s+(\S+)\s+(\S+)\s+(.+)\<key\s*([-]{0,1}\d+)\s*part\s*(\d+)\>\"(.*)[^\"]$/) ||
      ($line =~ /($dateTimeFormat)\s+(\S+)\s+(.+)\<key\s*([-]{0,1}\d+)\s*part\s*(\d+)\>\"(.*)[^\"]$/))
  {
    while (my $nextLine = <>)
    {
      # Increase line number on input file
      $$lineNoRef++;
      
      # Ready if $nextLine is a new trace line. Return $nextLine from parseLine so that it can be re-parsed
      if ($line =~ /($dateTimeFormat)/)
      {
        $returnLineForReparsing = $nextLine;
        last;
      }
      
      # Append $nextLine to $line
      $line = $line . "\\n" . $nextLine;

      # Ready when $nextLine ends with citation mark
      last if ($nextLine =~ /.*\"$/);
    }
  }

  # Example line
  # [2004-10-13 13:29:35.892] 001800/rnhRnsapRoHndlC ../src/RlibTraceTraceD.cpp:720 INFO:< Segmenting traces for key 1209948936, 11 parts will follow >
  if ($line =~ /($dateTimeFormat)\s+(\S+)\s+(\S+)\s+(\S+):.*\<\s*Segmenting traces for key ([-]{0,1}\d+), (\d+) parts will follow\s*\>/)
  {
    # New segmented trace starts here
    my $dateTime = $1;
    my $traceObj = $2;
    my $keyVal = $5;
    my $noOfParts = $6;
    
    if ($traceObj =~ /^([0-9]{6})\/(\S+)/)
    {
      my $boardId = $1;
      $keyVal = $boardId . "_" . $keyVal;
    }
  
    # Create new object
    createNewObject($objHashRef, $dateTime, $keyVal, $noOfParts, $moshell_board);
  }
  # Example line
  # [2004-10-13 13:29:35.892] ../src/RlibTraceTraceD.cpp:720 INFO:< Segmenting traces for key 1209948936, 11 parts will follow >
  elsif ($line =~ /($dateTimeFormat)\s+(\S+)\s+(\S+):.*\<\s*Segmenting traces for key ([-]{0,1}\d+), (\d+) parts will follow\s*\>/)
  {
    # New segmented trace starts here
    my $dateTime = $1;
    my $keyVal = $4;
    my $noOfParts = $5;

    # Create new object
    createNewObject($objHashRef, $dateTime, $keyVal, $noOfParts, $moshell_board);
  }
  # Example line
  # [2004-10-13 13:29:35.892] 001800/rnhRnsapRoHndlC ../src/RlibTraceTraceD.cpp:729 INFO:<key 1209948936 part 1>"rnhRnsapRoHndlC: signal data: RoamIfFroIurLinkRoExtSigGetAttribListRspD{rncModuleId -1,noFro 2,froIdList{size 2,array{RTInteger 0,RTInteger 1}},attribList{size 2,array{RoamIfFroIurLinkRoExtAttribD{mcc 240,mnc 99,mncLength 2,rncId 302,synchTimeout 2,synchRetransmissions 2,aliasPlmnIdentities{size 30,array{RoamIfFroIurLinkRoExtPlmnIdentitiesD{mcc -1,mnc -1,mncLength -1},RoamIfFroIurLinkRoExtPlmnIdentitiesD{mcc -1,mnc -1,mn"
  # 2008-06-26: Let any data before <key xxx part yyy>, including any ':' be part of trace group field. In this RNC_EXCEPTION trace and cell-traces can also be handled
  elsif ($line =~ /($dateTimeFormat)\s+(\S+)\s+(\S+)\s+(.+)\<key\s*([-]{0,1}\d+)\s*part\s*(\d+)\>\"(.*)$/)
  {
    # Another segment for segmented trace, may be the last one.
    my $dateTime = $1;
    my $traceObj = $2;
    my $fileLine = $3;
    my $traceGroup = $4;
    my $keyVal = $5;
    my $segNo = $6;
    my $segment = $7;

    if ($traceObj =~ /^([0-9]{6})\/(\S+)/)
    {
      my $boardId = $1;
      $keyVal = $boardId . "_" . $keyVal;
    }

    # Remove any trailing '"' from segment
    if ($segment =~ /(.*)\"/)
    {
      $segment = $1;
    }

    # Lookup object in container hash and add segment
    addSegmentToObject($lineNoRef, $objHashRef, $dateTime, $traceObj, $fileLine, $traceGroup, $keyVal, $segNo, $segment);
  }
  # Example line
  # [2004-10-13 13:29:35.892] ../src/RlibTraceTraceD.cpp:729 INFO:<key 1209948936 part 1>"rnhRnsapRoHndlC: signal data: RoamIfFroIurLinkRoExtSigGetAttribListRspD{rncModuleId -1,noFro 2,froIdList{size 2,array{RTInteger 0,RTInteger 1}},attribList{size 2,array{RoamIfFroIurLinkRoExtAttribD{mcc 240,mnc 99,mncLength 2,rncId 302,synchTimeout 2,synchRetransmissions 2,aliasPlmnIdentities{size 30,array{RoamIfFroIurLinkRoExtPlmnIdentitiesD{mcc -1,mnc -1,mncLength -1},RoamIfFroIurLinkRoExtPlmnIdentitiesD{mcc -1,mnc -1,mn"
  # 2008-06-26: Let any data before <key xxx part yyy>, including any ':' be part of trace group field. In this RNC_EXCEPTION trace and cell-traces can also be handled
  elsif ($line =~ /($dateTimeFormat)\s+(\S+)\s+(.+)\<key\s*([-]{0,1}\d+)\s*part\s*(\d+)\>\"(.*)$/)
  {
    # Another segment for segmented trace, may be the last one.
    my $dateTime = $1;
    my $traceObj = ""; # No trace object exists with this file format
    my $fileLine = $2;
    my $traceGroup = $3;
    my $keyVal = $4;
    my $segNo = $5;
    my $segment = $6;

    # Remove any trailing '"' from segment
    if ($segment =~ /(.*)\"/)
    {
      $segment = $1;
    }

    # Lookup object in container hash and add segment
    addSegmentToObject($lineNoRef, $objHashRef, $dateTime, $traceObj, $fileLine, $traceGroup, $keyVal, $segNo, $segment);
  }
  else
  {
    # Not a concatenated trace line, just print it.
    print STDOUT "$line\n";
  }
  return $returnLineForReparsing;
}

sub setProgName
{
  $progName = shift @_;
  $progName = $1 if ($progName =~ /.*\/(\S+)/);
}

sub getProgName
{
  return $progName;
}
  
# Main function
sub main
{
  # Store program name
  setProgName($0);

  # Turn on autoflush
  $| = 1;

  # Container to hold all concatenated trace objects
  my $objHashRef = {};

  # Current line number in input file
  my $lineNo = 0;

  # Then concatenate segmented traces.
  while(my $line = <>)
  {
    ++$lineNo;
    my $returnLineForReparsing = parseLine($line, \$lineNo, $objHashRef);
    # If a line to re-parse is returned, take that one first
    # line number has already been incremented
    if ($returnLineForReparsing ne "")
    {
      parseLine($returnLineForReparsing, \$lineNo, $objHashRef);
    }
  }

  # Print any incomplete objects
  foreach my $key (keys(%$objHashRef))
  {
    my $obj = $$objHashRef{$key};
    $obj->printData() if defined($obj);
  }
}

# Simple test sub routine
sub simpleTest
{
  my $obj = TraceData->new(
			   keyVal => 17,
			   noOfParts => 2,
			   dateTime => "[2004-10-13 13:29:35.892]");
  $obj->addSegment(1, "Hej ", 17);
  $obj->addSegment(2, "Svejs", 4711);
  $obj->setTraceObj("001100/process1");
  $obj->setFileLine("file1.cpp:4711");
  $obj->setTraceGroup("TRACE7:");
  $obj->printData;
}

# Run main
main();
