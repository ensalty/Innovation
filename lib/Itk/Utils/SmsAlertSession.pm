#!/usr/bin/perl
##
###
### Author: eqsvimp    Eric Yin Z
### $Id: itkreport 9360 2014-11-04 05:06:20Z eqsvimp $
##
### GSC CHINA Tool & Data Centre Service
###
#
package Itk::Utils::SmsAlertSession;

use Expect;
use MIME::Lite;
use Time::gmtime;

use Exporter 'import';
our @EXPORT=qw(sendSmsWorker);




use constant {
	DEFAULT_LOGIN_TIMEOUT => 1000,  # Default timeout for login phase (seconds)
	DEFAULT_COMMAND_TIMEOUT => 2000,  # Default timeout for normal command handling phase (seconds)
};

use fields (
	    'from',  # Last prompt line seen
            'smtp',  # Last error received
            'contactlist',  # Regex for CLI prompt matching
            'subject',  # Remote hostname or IP address
	    'message',  # Remote port
            'attach',
);


sub new 
{
        my __PACKAGE__ $self = shift;
        $self = fields::new($self) unless (ref $self);
        return $self;
        my $self->{message} = <<_EOF_;
[ENIQ STATUS ALERT]
[SERVER STATUS]: OK
[ETLC LOADING] : OK
[DATA LOADING] : OK
[TIME]: $localtime
[GSC China Tool & Data Center Team, Developer Eric Yin Z]
_EOF_


}


sub setField
{
        my ($self,%options) = @_;
        $self->{from}   = $options{from};
        $self->{smtp}   = $options{smtp};
        $self->{contactlist}= $options{contactlist};   
        $self->{subject}= $options{subject}; 
        $self->{message}= $options{message};
        $self->{attach}= $options{attach};
        
}






sub sendWorker
{
       my $self = shift;
       my $eMail = MIME::Lite->new(
                                   From => $self->{from}, 
                                   To => $self->{contactlist}, 
                                   Subject => $self->{subject}, 
                                   Type => 'text/html', 
                                   Data => $self->{message});
       
       $eMail->attach(Type => 'auto',
                      Path => $self->{attach},
        ) if(defined $self->{attach});
               
                                   $eMail->send('smtp', 'smtp.eapac.ericsson.se', Timeout => 60);
                                   #$eMail->send;
}





1;

