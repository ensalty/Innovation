#!/usr/bin/perl
#
##
## Author: eqsvimp    Eric Yin Z
## $Id: itkreport 9360 2014-11-04 05:06:20Z eqsvimp $
#
## GSC CHINA Tool & Data Centre Service
## 

package Itk::Utils::LoginSession;
use Expect;
use Hash::Util qw(lock_keys);
use Exporter 'import';
our @EXPORT_OK=qw(eca_login node_login);


use constant {
	DEFAULT_LOGIN_TIMEOUT => 1000,  # Default timeout for login phase (seconds)
	DEFAULT_COMMAND_TIMEOUT => 2000,  # Default timeout for normal command handling phase (seconds)
};

use fields (
	'user',  # Last prompt line seen
        'password',  # Last error received
        'cliPrompt',  # Regex for CLI prompt matching
        'host',  # Remote hostname or IP address
        'dmx',
	'port',  # Remote port
	'timeout',  # Current active timeout (seconds), or "0" for no timeout
        'expect',  # If set, errors will be printed
        'command',
	'interact',
        'echo',
	'read'
);	


#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $srv;  #reference to global server object
*srv=\$main::srv;

# Returns the default CLI prompt for this class
# Redefine in derived classes as necessary
sub _defaultCliPrompt { return undef; }

#==============================================================================
# PUBLIC METHODS
#==============================================================================

##------------------------------------------------------------------------------
# new(%options)
# Constructs a new RemoteSession object (abstract class!)
# - %options
# % host  Remote hostname or IP address
# % port  Remote port
# % timeout  Login/command timeout to use initially ('0' for none)
# % printErrors  If set, errors will not be printed
# % prompt  CLI Prompt expression to use for remote node session
# % logfile  Path to logfile to store session log (file will be created)
# RETURNS: RemoteSession object
#------------------------------------------------------------------------------
sub new {
        my __PACKAGE__ $self = shift;
        $self = fields::new($self) unless (ref $self);
        
=pod
        my (%options) = @_;

        $self->{user}     = $options{user};
        $self->{password} = $options{password};
        $self->{cliPrompt}= $options{cliPrompt};   
        $self->{host}     = $options{host} or die __PACKAGE__.": No host provided\n";
        $self->{port}     = $options{port};
        $self->{timeout}  = defined $options{timeout} ? $options{timeout} : DEFAULT_LOGIN_TIMEOUT;

        if ($options{exp}) {
               # $self->{exp} = new FileHandle("> $options{logfile}") or
               #         die __PACKAGE__.": Unable to write to $options{logfile}\n";
               $self->{exp} =  $options{exp};
               print "get expect module process\n";
        }
       # bless($self,%class);

=cut
        return $self;
}



sub setField
{
        my ($self,%options) = @_;
        $self->{user}     = $options{user};
        $self->{password} = $options{password};
        $self->{cliPrompt}= $options{cliPrompt};   
#        $self->{host}     = $options{host} or die __PACKAGE__.": No host provided\n";
 	$self->{host}     = $options{host};
        $self->{dmx}     = $options{dmx};
        $self->{port}     = $options{port};
        $self->{timeout}  = defined $options{timeout} ? $options{timeout} : DEFAULT_LOGIN_TIMEOUT;
        $self->{command}     = $options{command};
        $self->{interact} = $options{interact};
	$self->{echo} = $options{echo};
}



sub setExpect
{
        my ($self,$exp) = @_;
        $self->{expect} = $exp;
}



#==============================================================================
# CLASS METHODS
#==============================================================================
sub close
{
my ($self)       = @_; 
$self->{expect}->hard_close();
}



sub ecaLogin
{

my ($self)       = @_;

if(defined $self->{command}) 
{
 $self->{expect} = Expect->spawn("$self->{command}");
}

else {
$self->{expect} = Expect->spawn("ssh -p $self->{port} $self->{user}\@$self->{host}\r");

}
$self->{expect}->log_stdout(0) if defined $self->{echo};


my $t1 = $self->{timeout};
#$self->{expect}->raw_pty(1);

$self->{expect}->expect($self->{timeout},
[ qr/Are you sure you want to continue connecting \(yes\/no\)\?/, sub {
                my $this=shift;
                $this->send("yes\r");
                print STDERR "EGW added to known hosts\n" unless $quiet;
                exp_continue;
                } ],

        [ qr/word:|CODE:/, sub {
                my $this=shift;
                $this->send($self->{password}."\r");
                exp_continue;
                } ],
        -re, $self->{cliPrompt}
) or die "Expect module terminated at CliPrompt";

if(defined $self->{interact} and $self->{interact} eq "enabled"){$self->{expect}->interact();}

$self->{read} = $self->{expect}->before();
return $self->{expect};

}




sub nodeLogin
{

my ($self) = @_;

if(defined $self->{command})
{
$self->{expect}->send("$self->{command}\r");
}

else {
$self->{expect}->send("ssh $self->{user}\@$self->{host} \r");
}


$self->{expect}->expect($self->{timeout},
[ qr/Are you sure you want to continue connecting \(yes\/no\)\?/, sub {
                my $this=shift;
                $this->send("yes\n");
                print STDERR "EGW added to known hosts\n" unless $quiet;
                exp_continue;
                } ],

        [ qr/word:|CODE:|admin:/, sub {
                my $this=shift;
                $this->send($self->{password}."\r");
                exp_continue;
                } ],
        -re, $self->{cliPrompt}
) or die "Expect module terminated at CliPrompt";

$self->{read} = $self->{expect}->before();
if($self->{interact} eq "enabled"){$self->{expect}->interact();}
return $self->{expect};

}



sub commandCommit
{
   my ($self,$command)  = @_;
   $self->{expect}->log_stdout(0) if defined $self->{echo};
   $self->{expect}->send("$command\r");
   $self->{expect}->expect( $self->{timeout},-re=>qr/$self->{cliPrompt}/ ) or return $self->{expect}->before();
   return $self->{expect}->before();
}

#####################################################################################
#
1; # Modules must return 1
