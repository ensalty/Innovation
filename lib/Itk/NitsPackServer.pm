#!/usr/bin/perl
###----------------------------------------------------------------------------
#
# ServerNitsPack Class. 
# This class provides logging fubctionality in a similiar fashion as to the 
# Server class in a full ITK System. Using this class enables more streamlining 
# and possibilities for code reuse between the nits pack and a full ITK system
#
# == Logging ==
#
# Provides the means to log error, warning, info and debug messages to STDOUT. 
# The actual level of logging performed is determined by DEBUG constants.
#
# (c) Ericsson AB 2006 - All Rights Reserved
#
# No part of this material may be reproduced in any form
# without the written permission of the copyright owner.
# The contents are subject to revision without notice due 
# to continued progress in methodology, design and manufacturing. 
# Ericsson shall have no liability for any error or damage of any
# kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of
# Telefonaktiebolaget LM Ericsson. All other trademarks mentioned
# herein are the property of their respective owners. 
#------------------------------------------------------------------------------
# $Id: NitsPackServer.pm 6230 2012-02-10 04:13:28Z eholmat $

#==============================================================================
# SYSTEM Initialisation
#==============================================================================
# This sets up the debug environment
BEGIN {

	use FindBin qw($Bin);
	# If there is no trace env variable then we just require the trace
	# file. Otherwise we may need to manually override some of the const
	# subs within it, so we will need to handle manually
	my $tfile="$Bin/../etc/trace.conf";
	if (exists $ENV{ITKTRACE}) {
		
		print STDERR "ITKTRACE = $ENV{ITKTRACE}\n";
		
		# First read all constants into hash %packages
		my @trace_groups=qw(DEBUG1 DEBUG2 DEBUG3 DEBUG4 DEBUG5 DEBUG6 DEBUG7 DEBUG8 DEBUG9 ENTER);
		my $n=1;
		my %trace_group_index=map { $_ => $n++ } @trace_groups;
		my %packages;
		my ($fh);
		open $fh,$tfile or die "Could not open $tfile for reading: $!\n";
		while (<$fh>) {
			chomp;
			my ($package, $group, $mask);
			if (($package,$group,$mask) = (/^\s*sub\s*([\w:]+::)(\w+)\s*\(\s*\)\s*{\s*(\d+)\s*}\s*;\s*$/)) {
				if ($group eq 'STACKTRACE') {
					eval $_;
					next;
				}
				next unless exists $trace_group_index{$group};
				$packages{$package}{$group}=$mask;
			}
		}
		close $fh;

		# ITKTRACE can be of the form:
		# [Package_Re=][+-][GROUP1[,GROUP2...][;[Package_Re=][+-][GROUP1[,GROUP2...]...]
		# Package_Re matches a package name
		# + means turn on these trace(s)
		# - means turn off these trace(s)
		# Neither + nor - means turn off all traces except these
		my @criteria;
		foreach my $spec (split ';',$ENV{ITKTRACE}) {
			my ($pkg_match,$oper,$groups) = ($spec =~ /(?:([^=]+)=)?([+-])?(\w+(?:,\w+)*)/);
			die "Invalid trace specifier in ITK_TRACE: $spec\n" unless defined $groups;
			$pkg_match='^::$' if $pkg_match eq 'main';
			my @groups;
			$groups=uc $groups;
			if ($groups eq 'ALL') {
				@groups = @trace_groups;
			} elsif ($groups eq 'NONE') {
				@groups=();
			} else {
				@groups=split ',',$groups;
			}
			foreach (@groups) { 
				$_="DEBUG$_" if /^[\d]$/; # Allow shorthand: 1 = DEBUG1, etc
				die "itktrace: Invalid trace group: $_\n" unless exists $trace_group_index{$_};
			}
			my $pkg_match_re = qr/$pkg_match/i if defined $pkg_match;
			foreach my $pkg (keys %packages) {
				next if (defined $pkg_match && $pkg !~ $pkg_match_re);
				unless ($oper) {
					$packages{$pkg}{$_}=0 foreach @trace_groups;
				}
				foreach my $grp (@groups) {
					$packages{$pkg}{$grp} = ($oper eq '-' ? 0 : 1);
				}
			}
		}
		
		# Now turn the %packages into a set of constants
		# We also calculate the DEBUG constant which is a bitmap of the enabled
		# DEBUGx's
		foreach my $package (keys %packages) {
			my $debug=0;
			foreach $group (reverse @trace_groups) {
				my $value = $packages{$package}{$group};
				$debug = ($debug << 1) + ($value ? 1 : 0);
				my $fullname = "${package}$group";
				*$fullname = sub () { $value };
			}
			$debug=$debug << 3;
			my $fullname = "${package}DEBUG";
			*$fullname = sub() { $debug };
		}
	} else {
		require $tfile;
	}

	# Install die handler. There are two possibilities:
	# 1) Stack trace enabled. 
	# If possible, we use Carp to print the
	# whole stack backtrace. Then, if possible, use logError to report
	# the error and exit. If that is not possible then die instead
	#
	# 2) Stack trace disabled.
	# If possible, use logError to report
	# the error and exit. If that is not possible then die instead
	if (Itk::Server::STACKTRACE()) {
		$SIG{__DIE__}=sub {
			my ($msg)=@_;

			# If stacktrace flag set to 2 then print stack traces if an error occurs during eval
			if (Itk::Server::STACKTRACE()<2) {
				return if defined $^S && $^S; # Just continue if this is within an eval
			} else {
				if (defined $^S && $^S) {
					$msg=$msg."Stacktrace:\n".DB::stacktrace(2);
					$srv->logWarn("Error trapped by eval: $msg");
					return;
				}
			}

			if (defined $^S) {
				$msg=$msg."Stacktrace:\n".DB::stacktrace(2);
				if (defined $::srv) {
					$srv->logError($msg, undef, caller());
					exit ($! != 0 ? $! : 255);
				} else {
					die $msg;
				}
			} else {
				die $msg;
			}
		};
	} else {
		$SIG{__DIE__}=sub {
			my ($msg)=@_;
			return if defined $^S && $^S; # Just continue if this is within an eval
			if (defined $^S && defined $::srv) {
				chomp $msg;
				$srv->logError($msg, undef, caller());
				exit ($! != 0 ? $! : 255);
			} else {
				die $msg;
			}
		};
	}
	# Set log verbosity
	# 4 = full timestamp & origin details
	# 3 = origin details
	# 2 = class & message
	# 1 = message only
	my $vlevel= exists $ENV{ITKLOGVERBOSE} ? $ENV{ITKLOGVERBOSE} : 7;
	*Itk::NitsPackServer::LOGVERBOSE=sub { $vlevel }; 
}

# This sub must be in package DB in order to force caller() to provide the variable
# @DB::args. See perldoc for caller()

package DB;

sub stacktrace { 
	my $start=shift;
	my $trace;
	my $i=$start;
	
	require Data::Dumper;
	import Data::Dumper;
	$Data::Dumper::Terse=1; 
	$Data::Dumper::Indent=0;
	$Data::Dumper::Maxdepth=1;
	$Data::Dumper::Useqq=1;
	
	while (($package, $filename, $line, $sub)=caller($i++)) {
		my @outargs;
		foreach (@args) {
			if (ref $_) {
				push @outargs, "$_";
			} else {
				push @outargs, Dumper($_);
			}
		}
		my $args = join(",",@outargs);
		my $lev = $i-$start;
		$trace .= "\n$lev\t$sub($args)\n\tcalled from $package($filename:$line)\n";
	}
	return $trace;
} 

package Itk::NitsPackServer;
use strict;
use Hash::Util qw(lock_keys);

#==============================================================================
# CONSTANTS
#==============================================================================
use constant {
	LOGLEVEL_ERROR	=> 0,					# Log level for errors
	LOGLEVEL_WARN	=> 1,					# Log level for warnings
	LOGLEVEL_INFO	=> 2,					# Log level for infos
	LOGLEVEL_DEBUG  => 3,					# Starting Log level for debug messages
								# Note levels 3-11 are reserved for debug levels 1-9
	LOGLEVEL_ENTER  => 12,					# Log level for enter messages
	LOGCOLOURS => ["\e[91m","\e[93m","\e[34m"],		# Colours used to highlight errors, warnings and infos
	JOBSTOP_FILE => '/tmp/jobstop',    			# File whose existence indicates whether ITK jobs should be aborted
};
use constant LOGLEVEL_NAMES => qw(ERROR WARN INFO DEBUG1 DEBUG2 DEBUG3 DEBUG4 DEBUG5 DEBUG6 DEBUG7 DEBUG8 DEBUG9 ENTER); # Symbol names for log levels
#==============================================================================
# INSTANCE VARIABLES
#==============================================================================
use fields (
	'defaultNwid',			# Default NWID
	'globalConfig',			# Hash storing global config
	'nitspack_configfile'		# Configuration file specific to nits_pack
);
#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $numInstances=0;
my $uname = `uname`;


##-----------------------------------------------------------------------------
# isLinux()
# Indicates whether this instance is running within a Linux host or not
# RETURNS: True if running in a Linux host
#------------------------------------------------------------------------------
sub isLinux {
      return $uname =~ /Linux/i;
}

##-----------------------------------------------------------------------------
# isSolaris()
# Indicates whether this instance is running within a Solaris/SunOS host or not
# RETURNS: True if running in a Solaris/SunOS host
#------------------------------------------------------------------------------
sub isSolaris {
      return $uname =~ /SunOS/i;
}

#==============================================================================
# CLASS INITIALISATION
#==============================================================================

#==============================================================================
# PRIVATE METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# _logEntry($class, $msg, $nwid, $package, $filename, $line)
# Write an entry to the server log
# - $class		Log Class (See constants above)
# - $msg		Message
# - $nwid		Customer to which the log message applies
# - $package		Package that generated the message (from caller())
# - $filename		File that generated the message (from caller())
# - $line		Line that generated the message (from caller())
# RETURNS: Nothing
#------------------------------------------------------------------------------
sub _logEntry {
	# No logEnter as it might result in recursive calls
	my $self=shift;
	my $class = shift;
        my $msg = shift;
	my $nwid = shift;
	my ($package, $filename, $line) = @_;
	
	# Put the message together. Local server time is used
        my ($ss,$mm,$hh,$dd,$mo,$yy) = localtime(time());
	my $classname = (LOGLEVEL_NAMES)[$class];
	$msg = "$classname: $msg\n";
	my $logout;
	if (LOGVERBOSE==7) {
		$logout=sprintf("[%04d-%02d-%02d %02d:%02d:%02d] $package($filename:$line) $$ $msg", $yy+1900, $mo+1, $dd, $hh, $mm, $ss);
	}
	if (LOGVERBOSE==6) {
		$logout=sprintf("$package($filename:$line) $$ $msg", $yy+1900, $mo+1, $dd, $hh, $mm, $ss);
	}
	if (LOGVERBOSE==5) {
		$logout=sprintf("$package $$ $msg", $yy+1900, $mo+1, $dd, $hh, $mm, $ss);
	}
	if (LOGVERBOSE==4) {
		$logout=sprintf("[%04d-%02d-%02d %02d:%02d:%02d] $package($filename:$line) $msg", $yy+1900, $mo+1, $dd, $hh, $mm, $ss);
	}
	if (LOGVERBOSE==3) {
		$logout=sprintf("$package($filename:$line) $msg", $yy+1900, $mo+1, $dd, $hh, $mm, $ss);
	}
	if (LOGVERBOSE==2) {
		$logout="$package $msg";
	}
	if (LOGVERBOSE==1) {
		$logout=$msg;
	}
		
	# New behaivour:
	# DEBUG/ENTER:
	# - Controlled by constants in /etc/itk/trace.conf
	# - Always output to STDERR
	# - Always prefixed with timestamp & localisation info
	# WARN/ERROR/INFO:
	# - always enabled
	# - if called from an interactive program:
	#        - output to STDERR with no prefix and with colour highlighting
	#        - output to logfile with a prefix
	# - otherwise
	#        - output to STDERR with a prefix
	#        - output to logfile with a prefix
	# Masks no longer used at all
	if ($class >= LOGLEVEL_DEBUG) {
		print STDERR $logout;
	} else {
		if (-t STDERR) {
			my $colourCode=LOGCOLOURS->[$class];
			print STDERR "$colourCode$msg\e[0m";
		} else {
			print STDERR $logout;
		}
	}
}

#==============================================================================
# PUBLIC METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# new(%options)
# Constructor   Constructs a new Nits Pack Server Object. this object is to be used 
#               for debug printouts on a Nits Pack system only.
#------------------------------------------------------------------------------
sub new {
	my ($self,%opt);
	if (ref $_[1] eq 'HASH') {
		$self=$_[0];
		%opt=%{$_[1]};
	} else {
		($self, %opt)=@_;
	}
	lock_keys(%opt, qw(nwid config));
	$self=fields::new($self) unless ref $self;
	if (defined $opt{config}) {
		$self->{nitspack_configfile} = $opt{config};
	}
	if (DEBUG9) {
		$numInstances++;
		my @caller = caller(1);
		my $caller="$caller[3] ($caller[1]:$caller[2])";
		$self->logDebug(9,"NEW $self created by $caller : n=$numInstances");
	}
	return $self;
}



##-----------------------------------------------------------------------------
# logFatal($msg[,$nwid])
# Log an error message, and then die with this message
# - $msg	Message to be logged
# - $nwid	nwid identifies the customer network that the message relates to
# RETURNS: Nothing
#------------------------------------------------------------------------------
sub logFatal {
	my ($self, $msg, $nwid) = @_;
	die "$msg\n";
}

##-----------------------------------------------------------------------------
# logError($msg[,$nwid][,$package][,$filename][,$line])
# Log an error. If package, filename or line are not supplied then they are
# determined from caller() - this is the normal case.
# - $msg	Message to be logged
# - $nwid	nwid identifies the customer network that the message relates to
# - $package	Package that generated the message (from caller())
# - $filename	File that generated the message (from caller())
# - $line	Line that generated the message (from caller())
# RETURNS: True
#------------------------------------------------------------------------------
sub logError {
	# No logEnter as that would be very annoying
	my ($self,$msg,$nwid,@caller)=@_;
	$nwid ||= $self->{defaultNwid};
	@caller=caller() unless @caller;
	$self->_logEntry(LOGLEVEL_ERROR,$msg, $nwid, @caller);
	return 1;
}

##-----------------------------------------------------------------------------
# logWarn($msg[,$nwid])
# Log a warning
# - $msg	Message to be logged
# - $nwid	nwid identifies the customer network that the message relates to
# RETURNS: True
#------------------------------------------------------------------------------
sub logWarn {
	# No logEnter as that would be very annoying
	my $self=shift;
	my $msg=shift;
	my $nwid=shift || $self->{defaultNwid};
	$self->_logEntry(LOGLEVEL_WARN,$msg, $nwid, caller());
	return 1;
}

##-----------------------------------------------------------------------------
# logInfo($msg[,$nwid])
# Log an Info
# - $msg	Message to be logged
# - $nwid	nwid identifies the customer network that the message relates to
# RETURNS: True
#------------------------------------------------------------------------------
sub logInfo {
	# No logEnter as that would be very annoying
	my $self=shift;
	my $msg=shift;
	my $nwid=shift || $self->{defaultNwid};
	$self->_logEntry(LOGLEVEL_INFO,$msg, $nwid, caller());
	return 1;
}

##-----------------------------------------------------------------------------
# logDebug($level, $msg[,$nwid][,@caller])
# Log a debug message. 
# - $level	Debug message level (1-9)
# - $msg	Message to be logged
# - $nwid	nwid identifies the customer network that the message relates to
# - $caller	Caller Info. If not supplied then caller() is used.
# RETURNS: True
#------------------------------------------------------------------------------
sub logDebug {
	# No logEnter as that would be very annoying
	my ($self, $level, $msg, $nwid, @caller)=@_;
	$nwid ||= $self->{defaultNwid};
	@caller = caller() unless @caller;
	# This is an interim measure. Check debug const for caller
	# and if disabled then exit, otherwise proceed
	# In future this will not be necessary since all client code
	# should use DEBUGx constants to compile in/out
	my $ref=$caller[0]."::DEBUG$level";
	{
		no strict 'refs';
		return unless &{$ref}();
	}
	$self->_logEntry(LOGLEVEL_DEBUG+$level-1,$msg, $nwid, @caller);
	return 1;
}

##-----------------------------------------------------------------------------
# logEnter($paramsref[,$nwid])
# Log entry into a subroutine message. Logs the subroutine name and the
# parameter list passed in.
# - $paramsref	Reference to calling parameter list
# - $nwid	nwid identifies the customer network that the message relates to
# RETURNS: True
#------------------------------------------------------------------------------
sub logEnter {
	# No logEnter as that would be recursive and really stupid
	my $self=shift;
	my $p=shift;
	my $nwid=shift || $self->{defaultNwid};
	# See explanation in logDebug for the reason for this
	my $ref=(caller())[0].'::ENTER';
	{
		no strict 'refs';
		return unless &{$ref}();
	}
	my $msg;
	$msg=(caller(1))[3].' Params: <'.join('>,<',@{$p}).'>';
	$self->_logEntry(LOGLEVEL_ENTER,$msg, $nwid, caller());
	return 1;
}


##-----------------------------------------------------------------------------
# setPrintMask()
# Deprecated. Does nothing.
# RETURNS: Nothing
#------------------------------------------------------------------------------
sub setPrintMask {
	$_[0]->logWarn("setPrintMask is deprecated. Use the itktrace command to set tracing");
}

##-----------------------------------------------------------------------------
# getConfig($key[, $nwid])
# Get a specific config item. If a customer specific setting for the key
# exists, then it will be returned, otherwise a global setting will be returned
# - $key	Key
# - $nwid	Customer nwid. Use _global force fetch of global config even
#               if a value might be overridden by a local setting
# RETURNS: $value
# - $value	Value of key. If nwid is supplied, and this key exists in the
#		given customer's namespace, then that value will be returned,
#		otherwise if the key exists in the global namespace, then that
#		value will be returned. If the key does not exist in either
#		namespace then undef is returned.
#		If nwid is not supplied, the default nwid for this server instance
#		will be checked (if any), then the global namespace will be
#		checked.
#------------------------------------------------------------------------------
sub getConfig {
	# No logEnter as it might result in recursive calls
	my $self=shift;
	my $key = shift or die "No Key Supplied";
	##my $nwid = shift || $self->{defaultNwid};
	
	# Check if config already loaded. If not, load it
	$self->_loadConfig() if (not $self->{globalConfig});
	
	# Check if global config is requested specifically
	#if (lc $nwid eq '_global') {
	#	return $self->{globalConfig}{$key} if exists $self->{globalConfig}{$key};
	#	$self->logDebug(1, "Attempt to fetch non-existant key: $key from global itk.conf") if DEBUG;
	#	return undef;
	#}
	
	#$self->_loadConfig($nwid) if ($nwid and not exists $self->{customerConfig}{$nwid});
	
	#return $self->{customerConfig}{$nwid}{$key} if ($nwid and exists $self->{customerConfig}{$nwid}{$key});
	return $self->{globalConfig}{$key} if exists $self->{globalConfig}{$key};
	#$self->logDebug(1, "Attempt to fetch non-existant key: $key from nwid: $nwid") if DEBUG;
	$self->logDebug(1, "Attempt to fetch non-existant key: $key from config file") if DEBUG1;
	return undef;
}
##-----------------------------------------------------------------------------
# _loadConfig([$nwid])
# Loads config data into memory. If possible, loads from cached data, otherwise
# parses config files
# - $nwid	Network ID identifying customer - specific config to be loaded
#		If this is undef, load the global config instead
# RETURNS: Nothing
#------------------------------------------------------------------------------
sub _loadConfig {
	# No logEnter as it might result in recursive calls
	my $self=shift;
	my $nwid=shift;

	##my $etcdir=ITK_ETC;
	##my ($file,$conf_cache_key, $config);
	my ($file, $config);

	# If called with an NWID parameter, then config file will be in ../etc/itk.conf.d/<nwid>.conf
	#if ($nwid) {
	#	$file="$etcdir/itk.conf.d/${nwid}.conf";
	#	$conf_cache_key="config_$nwid";
	#	# Customer specific config stored in self->{customerConfig}{nwid}
	#	$config=$self->{customerConfig}{$nwid}={};
	#} else {
	
		$file=$self->{nitspack_configfile};
		#$conf_cache_key="global_config";
		# Global config stored in self->{customerConfig}{nwid}
		$config=$self->{globalConfig}={};
	#}

	# Config is to be stored in the cache the first time a file is read
	# on subsequent occasions, it is read from the cache. The key used for
	# the cache is 'config/<nwid>' for customer specific config, or
	# 'global_config' for global config.
	# To keep track of wether the cache is up to date or not, when the data
	# is cached the mtime of the file is stored in a separate key with name
	# config_<nwid>_mtime or global_config_mtime
	
	#my $cache=new Cache::FileCache({ namespace => CONFIG_CACHE_NAMESPACE });
	#my $mtime = $cache->get("${conf_cache_key}_mtime");
	#my $newmtime = (stat($file))[9];
	#if ((not defined $mtime) or $mtime < $newmtime) {
		# Config cache was non-existant or older than the last
		# file modification time - [re-]read from the config file
		# and then save in the cache
		_loadConfigFile($file,$config);
	#	$cache->set($conf_cache_key,$config);
	#	$cache->set("${conf_cache_key}_mtime",$newmtime);
	#} else {
		# Cache is up to date. Just load from the cache
	#	if ($nwid) {
	#		$self->{customerConfig}{$nwid} = $cache->get($conf_cache_key);
	#	} else {
	#		$self->{globalConfig} = $cache->get($conf_cache_key);
	#	}
	#}
}
##-----------------------------------------------------------------------------
# _parseConfigFile($file, $config)
# Parses a config file, storing data in the supplied hash
# - $file	Filename of config file
# - $cref	Ref to hash in which it will be stored
# RETURNS:	Nothing.
#------------------------------------------------------------------------------
sub _loadConfigFile {
	# No logEnter as it might result in recursive calls
	my $file = shift;
	my $cref = shift;
	
	die "_loadConfigFile >> nits_pack config file is undefined, perhaps you forgot to pass this parameter at serve creation ??, Aborting"
		unless defined $file;
	
	# If file does not exist, then undef foreach & all!
	return undef unless -e $file;
	
	unless (open FH, $file) {
		die "Could not open config file $file: $!\n";
	}
	
	my ($key, $val, $term);
	while (<FH>) {
		chomp; # Discard line feed at end of line
		next if /^\s*#/; # Skip full-line comments
		$_ =~ s/#.+$//;  # Strip comments from lines
		next if /^\s*$/; # Skip blank lines
		
		# Process multi-line strings. These look like perl 'here' documents
		if (/^\s*([\w\.]+)\s*<<(\w+)$/) { 
			$key=$1;
			$val="";
			my $term=qr/^$2$/; # This expression will match the terminator
					   # on a line by itself
			
			# Read each line, accumulate in $val & terminate when $term is matched
			while (<FH>) {
				last if ($_ =~ $term);
				$val .= $_;
			}
			die "Unterminated multiline string in config file $file on line $.\n" if eof(FH) and $_ !~ $term;
			$cref->{$key}=$val; # Store result
			next;
		}
		
		# Process 'eval' declaration. These look like this:
		# keyname	eval {
		#               .. perl expression..
		# }
		# The perl expression is read in from file, eval'd and the result
		# is stored. Useful for nested data structures
		# Paring similar to block above
		if (/^\s*([\w\.]+)\s*eval\s*{$/) {
			$key=$1;
			$val="";
			while (<FH>) {
				last if (/^}$/);
				$val .= $_;
			}
			die "Unterminated eval string in config file $file on line $.\n" if eof(FH);
			$cref->{$key}=eval "$val";
			die "Error evaluating expression in config file $file on line $.\n$@\n" if $@;
			next;
		}

		# Process 'sub' declaration. These look like this:
		# keyname	sub {
		#               .. perl subrouting..
		# }
		# The perl subrouting is read in from file, eval'd (compiled) and the result
		# is stored. Useful for subs to be defined in config
		# Note: At present Code values cannot be cached! These shall not be
		# used until that is overcome
		# Paring similar to blocks above
#		if (/^\s*(\w+)\s*sub\s*{\s*$/) {
#			$key=$1;
#			$val="";
#			while (<FH>) {
#				last if (/^}$/);
#				$val .= $_;
#			}
#			die "Unterminated sub declaration string in config file $file on line $.\n" if eof(FH);
#			$cref->{$key}=eval "sub { $val }";
#			die "Error compiling sub declaration in config file $file on line $.\n$@\n" if $@;
#			next;
#		}

		($key,$val)=split ' ',$_,2;
		if ($val =~ /^\s*(['"])(.+)\1\s*$/) { 	# Quoted String
			$val=$2;
		} elsif ($val =~ /^\s*([+-]?\d+(?:\.\d+)?)\s*$/) {	# Decimal Number
			$val=$1;
		} elsif ($val =~ /^\s*0[bx]\d+\s*$/) {
			$val = eval ($val);
			if ($@) {
				my ($err)=($@ =~ /(.+)\sat\s\(eval/);
				die "$err in config file $file line $.\n";
			}
		} elsif ($val =~ /^\s*(\w+)\s*$/) {	# Bareword
			$val=$1;
		} else {
			die "Syntax error in config file $file line $.\n";
		}
		$cref->{$key}=$val;
	}
	
	close FH;
}

##-----------------------------------------------------------------------------
# jobstopFileExists(%options)
# Checks for the presence of a file in a predefined location on the server.
# The presence of this file indicates that current jobs should be stopped as
# soon as is convenient, and new jobs should not be started.
# - %options		Currently unused
# RETURNS: boolean value indicating whether file is present
#------------------------------------------------------------------------------
sub jobstopFileExists {
	my ($self, %options) = @_;
	if (-f JOBSTOP_FILE) {
		$self->logInfo('Abort file found! ('.JOBSTOP_FILE.')');
		return 1;
	}
	return 0;
}

##-----------------------------------------------------------------------------
# DESTROY()
# Destructor
#------------------------------------------------------------------------------
sub DESTROY {
	if (DEBUG9) {
		$numInstances--;
		$_[0]->logDebug(9, "DESTROY $_[0] : n=$numInstances");
	}
}
#------------------------------------------------------------------------------
1; # Modules always return 1!


