#!/usr/bin/perl
###----------------------------------------------------------------------------
# Parser/Analyser for Cello Availability Log files
#
# (c) Ericsson AB 2007 - All Rights Reserved
#
# No part of this material may be reproduced in any form
# without the written permission of the copyright owner.
# The contents are subject to revision without notice due 
# to continued progress in methodology, design and manufacturing. 
# Ericsson shall have no liability for any error or damage of any
# kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of
# Telefonaktiebolaget LM Ericsson. All other trademarks mentioned
# herein are the property of their respective owners. 
# 
#------------------------------------------------------------------------------
# Author: Mario Landreville
# $Id$ lmcmarl

package Itk::EventData::ECDAParser;
use strict;
use Itk::EventData::EventRec;
use Time::Local qw(timegm_nocheck);
use Hash::Util qw(lock_keys);


BEGIN {
	if (DEBUG6) {
		require Data::Dumper;
		import Data::Dumper;
	}
}

#==============================================================================
# CONSTANTS
#==============================================================================
use constant {
	OK => 1,			# Operation succeeded
	PARSE_FAILED => -1,		# Parsing failed
	OK_MISSEDDATA => 2,		# Operation succeeded but there was some missing data
	
};
#==============================================================================
# INSTANCE VARIABLES
#==============================================================================
use fields (
	'nodename',	# Name of currently processed node
	'avlogparser',	# Instance of av log parser
	'dumpdir',	# Directory into which completed ecda's should be placed
);
#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $srv;
*srv=\$main::srv;
#==============================================================================
# CLASS METHODS
#==============================================================================

#==============================================================================
# PRIVATE METHODS
#==============================================================================
##-----------------------------------------------------------------------------
# _warn($msg)
# Issue warning, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _warn {
	$srv->logWarn("($_[0]->{nodename}) $_[1]");
}

##-----------------------------------------------------------------------------
# _error($msg)
# Issue Error, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _error {
	$srv->logError("($_[0]->{nodename}) $_[1]");
}

#==============================================================================
# PUBLIC METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# new(%opts)
# Constructor
# % nodename	Name of node
# % dumpdir	Directory into which ECDA dumps will be moved after parsing
# % avlogparser Instance of AV Log Parser, if present then it will be used to
#               determine the running sw package and cv at the time of restart
#------------------------------------------------------------------------------
sub new {
	$srv->logEnter(\@_) if ENTER;
	my ($self, %opts)=@_;
	lock_keys(%opts, qw(nodename dumpdir avlogparser));
	$self=fields::new($self) unless ref $self;
	$self->{nodename}=$opts{nodename};
	$self->{dumpdir}=$opts{dumpdir};
	$self->{avlogparser}=$opts{avlogparser};
	return $self;
}

##-----------------------------------------------------------------------------
# _parseBinData ($fh, %opts)
# Parse a section of binary data in the input file
# - $fh		Filehandle
# % filter	If equal to 'printable' then only printable characters will be
#		included in output
# RETURNS: $data
# - $data	Parsed data as a scalar
#------------------------------------------------------------------------------
sub _parseBinData {
	my ($self, $fh, %opts) = @_;
	lock_keys(%opts, qw(filter));
	
	my $printableFilter = ($opts{filter} eq 'printable');
	
	# Binary Data
	# -----------
	# 
	# Hex Addr  0001 0203 0405 0607 0809 0a0b 0c0d 0e0f
	# --------  ---- ---- ---- ---- ---- ---- ---- ----
	#        0  0000 0000 0000 0001 0000 0000 0000 0000
	#       10  0002 0000 5546 5049 4420 616c 7265 6164
	# ...
	
	# First look for heading lines
	my $found;
	while (<$fh>) {
		chomp;
		$srv->logDebug(6,"BinData Hdr  : $_") if DEBUG6;
		$found=1 and next if /^Binary Data/;
		next if /^--------/;
		$found=2 and last if $found==1 && /^Hex Addr/;
	}
	unless ($found==2) {
		$self->_warn("Did not find expected binary data block");
		return undef;
	}
	$_=<$fh>; # Skip over useless line of ----
	chomp;
	$srv->logDebug(6,"BinData Hdre : $_") if DEBUG6;
	
	my ($addr, $data, @lines);
	while (<$fh>) {
		chomp;
		$srv->logDebug(6,"BinData Body : $_") if DEBUG6;
		last unless (($addr,$data) = (/^\s*([0-9A-Fa-f])+\s+((:?[0-9A-Fa-f]{4}\s*){8})$/));
		$data =~ s/\s//g; # Remove spaces between hex values
		$data = pack("H*",$data); # Convert hex values to binary
		$data =~ s/[^[:print:]]//g if $printableFilter; # remove any nonprintable chars
		push @lines, $data;
	}
	
	$data = join('', @lines);
	
	return $data;
}


##-----------------------------------------------------------------------------
# parse($file, $callback)
# Parses the indicated avlog file
# - $file	Filename of syslog file to parse
# - $callback	Call back sub to recieve results
# RETURNS: $resultcode
# $resultcode	Result of parsing
#------------------------------------------------------------------------------
sub parse {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $file, $callback)=@_;
	
	$srv->logDebug(4, "Start parsing $file") if DEBUG4;
	
	my (
		$line,
		$ts,
	);
	
	# open the file
	my $fh;
	
	if ($file =~ /(.*)\.gz/) {
		open $fh, "gzip -dc $file |";
		if (! $fh || eof($fh)) {
			$self->_error("Failed to open '$file' (gunzipped): $!");
			return PARSE_FAILED;
		}
	} else {
		open $fh, $file or do {
			$self->_error("Failed to open '$file': $!");
			return PARSE_FAILED;
		};
	}
	
	my $ecdaRecord = {};
	
	# keep the file name
	$ecdaRecord->{file} = $file;
	
	# Slot information should be available in the filename
	# Format: TGWA_c_logfiles_dspdumps_00_09_ECDAdump03-83.gz.txt
	my ($subrack,$slotnr);
	if (($subrack,$slotnr) = ($file =~ /(\d\d)_(\d\d)_ECDAdump/)) {
		$ecdaRecord->{slot} = "${subrack}${slotnr}00";
	} else {
		$ecdaRecord->{slot} = "UNKNOWN";
	}
	
	# loop in the file
	while (<$fh>) {

		my $line = $_;
				
		# get the record
		if ($line =~ /^LM Product Number\s*:\s*(.*)/) {
			$ecdaRecord->{lm_product} = $1;
			next;
		}

		# get revision
		if ($line =~ /^LM Product Revision\s*:\s*(.*)/) {
			$ecdaRecord->{lm_revision} = $1;
			next;
		}
		
		# get date and time
		if ($line =~ /^\w*Date and time\s*:\s*(\d+)\s*(\d+)/) {
			$ecdaRecord->{date} = $1;
			$ecdaRecord->{ts} = $2;
			next;
		}
		
		# get third mailbox
		if ($line =~ /^Third mailbox contents\s*:/) {
			# We expect this to be a block of binary data
			my $binData = $self->_parseBinData($fh);
			
			# Expected Format: RBCI_CriticalFaultIndS
			
			my (	$errMsg,  
				$lineNo,
				$fileNamePos,
				$msgPos,
				$binDataPos,
				$binDataLen,
				$paddingWord,
				$paddingByte,
				$group,
				$data,
			) = unpack("x[20] Z[96] x[2] n n n n n n c c a*", $binData);
			
			my $fileName = unpack("Z*", substr($data, $fileNamePos));
			
			my $msg = unpack("Z*", substr($data, $msgPos));
			
			my @errInfo=( $msg );

			# Sometimes fileName contains unprintable garbage instead of a filename
			if ($fileName =~ /^[[:print:]]+$/) {
				push @errInfo, "($fileName:$lineNo)";
			}

			# Sometimes msg is same as errMsg - if so discard it, if not append it
			if ($msg ne $errMsg) {
				push @errInfo, $msg;
			}
			$ecdaRecord->{errorinfo} = join(' ', @errInfo);
			next;
		}
		
		# get fatalError
		if ($line =~ /^Fatal Error\s*:\s*(.*)/) {
			$ecdaRecord->{fatalerror} = $1;
			next;
		}
		
		
		# get Error code
		if (my ($code,$value) = ($line =~ /^Error Code Number\s*:\s+(0x[0-9A-Fa-f]+)\s*\((.*)\)$/)) {
			if ($value =~ /unknown/i) {
				$ecdaRecord->{errorcode} = $code;
			} else {
				$ecdaRecord->{errorcode} = $value;
			}
			next;
		}
		
		# get error code description
		if ($line =~ /^Error Code Desc\.\s*:\s+(.*$)/) {
			$ecdaRecord->{errordesc} = $1;
			next;
		}
		
		# get error function
		if ($line =~ /^Error Function\s*:\s+(.+)$/) {
			$ecdaRecord->{errorfunction} = $1;
			next;
		}
		
		# get current pid
		# Current Pid       : 0 (VERIF_IDLE_proc)
		if (my ($pid, $pname) = ($line =~ /^Current Pid\s*:\s+(\d+)\s*\((.*)\)$/)) {
			if ($pname =~ /unknown/i) {
				$ecdaRecord->{process} = $pid;
			} else {
				$ecdaRecord->{process} = $pname;
			}
			next;
		}
		
		# Get SNID
		if ($line =~ /^DSP SNID:\s*(\d+)/) {
			$ecdaRecord->{snid}=$1;
		}
		
		# Get caller
		# Caller            : KERNEL
		if ($line =~ /^Caller\s+:\s*(.+)$/) {
			$ecdaRecord->{caller}=$1;
		}
		
	}
	
	close $fh;

	my $rec = undef;
	if ($rec = $self->_validateRecord ($ecdaRecord)) {
		&$callback($rec);
	} else {
		$srv->logDebug(4, "Result of parsing $file : FAILED ") if DEBUG4;
		return PARSE_FAILED;	
	}					
		
	$srv->logDebug(4, "Result of parsing $file : OK") if DEBUG4;
 
 
	return OK;
}

#-----------------------------------------------------------------------------
# _validateRecord($eventrecord)
# Analyses the given events, calls the callback if the 
# RETURNS: $outrec
# - $outrec return the output record
#------------------------------------------------------------------------------
sub _validateRecord () {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $eventrecord)=@_;
		
	# fix me: 
	# put some validation here to discard record that we don't want.

	# get the file name. We store the basename of the file
	# (Without directories)
    	my ($file) = ($eventrecord->{file} =~ /([^\/]+)$/);
    
	# build date with date & time 
	my ($h,$m,$s,$yy,$mm,$dd);
	
	# parse the date
	# Date and time: 090407 105833
	# Format:        yymmdd hhmmss
	unless (($yy,$mm,$dd) = ($eventrecord->{date} =~ /(\d{2})(\d{2})(\d{2})/) ) {
		$srv->logDebug(4, "Invalid Date".$eventrecord->{date}) if DEBUG4;
		$self->_error (" date '$eventrecord->{date}' is invalid in file: $file ");		
		return undef;
	}
	 
	# parse the time
	unless (($h,$m,$s) = ($eventrecord->{ts} =~ /(\d{2})(\d{2})(\d{2})/)) {
		$srv->logDebug(4, "Invalid time".$eventrecord->{ts}) if DEBUG4;
		$self->_error (" time '$eventrecord->{ts}' is invalid in file: $file ");
		return undef;
	}
	
	#get date & time
	# In case out of range do this in eval
	my $ts;
	eval {
		$ts=timegm_nocheck($s,$m,$h,$dd,$mm-1,$yy);
	};
	if ($@) {
		$self->_error (" Event Timestamp '$eventrecord->{date} $eventrecord->{ts} is invalid in file: $file");
		return undef;
	}
	
	# Prepare extrainfo string, which will contain whichever of errorfunction, fatalerror and mailbox are present
	my $infotext = join(',', map "$_=$eventrecord->{$_}",grep $eventrecord->{$_}, qw(snid errorfunction caller errordesc fatalerror errorinfo));
	
	my ($up, $cv);
	if ($self->{avlogparser}) {
		$srv->logDebug(4, " up ($up) and cv ($cv) taken from avglogparser " ) if DEBUG4;
		($up, $cv) = $self->{avlogparser}->getRunningUp($ts);
	} 
	
	##
	# if  $up or $cv not defined 
	# take it from history from the module directly.
	##
	if (!defined ($up) || !defined ($cv)) {
		my $sdata = $self->{avlogparser}->getStateData ();
		$cv = $sdata->{currentCv};
		$up = $sdata->{currentUp};
		$srv->logDebug(4, " up ($up) and cv ($cv) taken from history " ) if DEBUG4;	
	}
	

	# build the record
	my $rec = new Itk::EventData::EventRec::Restart(
		time		=> $ts,
		nodename	=> $self->{nodename},
		#nodetype	=> 
		#rncparent	=> 
		restarttype	=> 'auto(dsp)',
		#reason		=> 
		infotext	=> $infotext,
		scope		=> "DSP",
		slot		=> $eventrecord->{slot},
		#piutype	=> 
		pmdid		=> $file,
		process		=> $eventrecord->{process},
		errorno		=> $eventrecord->{errorcode},
		#extra		=> 
		block		=> "$eventrecord->{lm_product}_$eventrecord->{lm_revision}",
		#stacktrace	=> 
		#productnumber	=> 
		#productrevision=> 
		#productdate	=> 
		#serialnumber	=> 
		sw		=> $up,
		cv		=> $cv,
		#newsw		=> 
		#newcv		=> 
		#cellodt	=> 
		#appdt		=> 
	);
    
	#return the record       
	return $rec;
}	
	

##-----------------------------------------------------------------------------
# parseFiles($fileList, $callback, %opts)
# Parses one or more avlog files, returning event records to the callback sub as
# they are found. The last processed record is stored separately for each file
# - $fileList	Ref to array containing local filenames of syslog files
# - $callback 	Ref to callback sub
# % pmddelivery	Specifies method for PMD delivery. If false or not present then
#           legacy delivery of PMDs applies. If true then they are moved into the
# 			dumpdir and the callback is called with the name as the first argument.
# RETURNS: $resultcode
# - $resultCode	  Result Code - see constants for definitions
#------------------------------------------------------------------------------
sub parseFiles {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $fileList, $callback, %opts)=@_;

	my (
		$rcode,		# Result code
	);


	#
	# parse all the file
	foreach my $file (@{$fileList}) {
		
		$srv->logDebug(4, "Parsing file : $file ");

		my $destFile;		
		if ($self->{dumpdir}) {
			($destFile) = ($file =~ /([^\/]+)$/);
			unless ($destFile) {
				$srv->logError("Could not derive ECDA file name");
				next;
			}
			$destFile="$self->{dumpdir}/$self->{nodename}_$destFile";
			rename $file, $destFile or do {
				$srv->logWarn("Problem renaming '$file' to '$destFile': $!");
				next;
			};
			
			&$callback($destFile) if $opts{pmddelivery};
		} else {
			$destFile=$file;
		}
		
		my ($parseRcode) = $self->parse($destFile, $callback);
				
		if ($parseRcode != OK) {
			$self->_warn("Failed parsing file $file, rcode=$rcode");
			$rcode=PARSE_FAILED;
			next;
		}
				
	}
		
	$rcode = OK unless defined $rcode;

	$srv->logDebug(4, "Result of file parsing: rcode=$rcode") if DEBUG4;
	
	return $rcode;
}


##-----------------------------------------------------------------------------
# DESTROY()
# Destructor
#------------------------------------------------------------------------------
sub DESTROY {
	my $self=shift;
	# Do destruction!!
}
#------------------------------------------------------------------------------
1; # Modules always return 1!

