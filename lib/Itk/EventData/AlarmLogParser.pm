#!/usr/bin/perl
###----------------------------------------------------------------------------
# Parser/Analyser for Cello Availability Log files
#
# (c) Ericsson AB 2007 - All Rights Reserved
#
# No part of this material may be reproduced in any form
# without the written permission of the copyright owner.
# The contents are subject to revision without notice due 
# to continued progress in methodology, design and manufacturing. 
# Ericsson shall have no liability for any error or damage of any
# kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of
# Telefonaktiebolaget LM Ericsson. All other trademarks mentioned
# herein are the property of their respective owners. 
# 
#------------------------------------------------------------------------------
# Author: Mario Landreville

package Itk::EventData::AlarmLogParser;
use strict;
use Itk::EventData::EventRec;
use Time::Local qw(timelocal timegm_nocheck);
#use Time::Local;
use Hash::Util qw(lock_keys);
use Data::Dumper;

BEGIN {
	if (DEBUG6) {
		require Data::Dumper;
		import Data::Dumper;
	}
}

#==============================================================================
# CONSTANTS
#==============================================================================
use constant {
	OK => 1,			# Operation succeeded
	PARSE_FAILED => -1,		# Parsing failed
	OK_MISSEDDATA => 2,		# Operation succeeded but there was some missing data
	
	
};
#==============================================================================
# INSTANCE VARIABLES
#==============================================================================
use fields (
	'collHist',		# Ref to collection history object
	'nodename',
	'nodetype',
	'startdate',		# Timestamp (unix epoch) of start time filter
	'alarms',
	'fdnmap'
);
#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $srv;
*srv=\$main::srv;
#==============================================================================
# CLASS METHODS
#==============================================================================

#==============================================================================
# PRIVATE METHODS
#==============================================================================
##-----------------------------------------------------------------------------
# _warn($msg)
# Issue warning, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _warn {
	$srv->logWarn("($_[0]->{nodename}) $_[1]",(caller(0)));
}

##-----------------------------------------------------------------------------
# _error($msg)
# Issue Error, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _error {
	$srv->logError("($_[0]->{nodename}) $_[1]", (caller(0)));
}

#==============================================================================
# PUBLIC METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# new($collHist, %opts)
# Constructor
# - $collHist	Ref to instance of collection history
# % nodename	Name of node
# % nodetype	Type of node
# % startdate 	Starting date as a unix epoch timestamp; PMD files earlier than this will not be fetched
#------------------------------------------------------------------------------
sub new {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $collHist, %opts)=@_;
	$self=fields::new($self) unless ref $self;
	$self->{collHist}=$collHist;
		
	$self->{nodename}=$opts{nodename};
	$self->{nodetype}=$opts{nodetype};
	$self->{startdate}=$opts{startdate};
	$self->{fdnmap}=$opts{fdnmap};
	$self->{alarms}=[];
	return $self;
}

##-----------------------------------------------------------------------------
# parse($file, $callback, $lastrecord, %opts)
# Parses the indicated avlog file
# - $file	Filename of syslog file to parse
# - $callback	Call back sub to recieve results
# - $lastrecord	Record ID of the last processed record. If this is not undef,
#		then we skip forward through the file until we find a matching
#		record; otherwise we just process all records in the file
#
# RETURNS: $resultcode, $lastrecord, $missingRecs
# - $resultCode	  Result Code - see constants for definitions
# - $lastrecord   Record ID of the last processed record
# - $missingRecs  Flag: if true, we could not find lastrecord so some records have been missed.
# - $startTime    Starting time for the time window covered by this parse (epoch secs)
# - $endTime      Ending time for the time window covered by this parse (epoch secs)
#------------------------------------------------------------------------------
sub parse {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $file, $callback, $lastrecord)=@_;
	
	my $parseErr = sub {
		$self->_error("Parse Error: $file:$. $_[0]");
	};

	# local variables	
	my (
		%alarmRecordsH, # hash to store the alarm
		$fdn,           #fdn
		@alarmRecords,  # alarm records Array
		$maxlogrec,	    # Ending log record number processed
		$minlogrec,		# Starting log record number found
		$startTime,		# Start time for this parse (either time of last processed, or first record found if none)
		$firstTime,		# Timestamp of earliest record in the file
		$endTime,		# Time of last record found in the file
		$ts,        	# time stamp variable
		$logRec,    	# log record
		@LogRecord     
	) = undef;
	
#	my $maxProcessedRecordId = $params{lastRecoredIdStored};

	my $initialRun = 1 unless defined ($lastrecord); 
	$" = ';'; # change the $LIST_SEPERATOR from space to ';' for use in generating the CSV report.
	
	# get the date
	my ($minutes,$hours,$days, $months, $years) = (localtime)[1,2,3,4,5];
	my $reportDate = sprintf("%04d-%02d-%02d_%02d%02d", $years+1900, $months+1, $days, $hours, $minutes);
	
	unless (open(LOGFILE, $file)) {
		warn "Could not open logfile '$file', Error: $! : proceeding with next log file\n";
		return;
	}
	
	my ($fdn,$moInstance); 	
	LOGRECORD:	while (<LOGFILE>) {
			if (/<LogRecord number\s+=\s+"(\d+)">/) {
				my $logRec = $1;
				
				$srv->logDebug(6, "processing record  $logRec") if DEBUG6;
				
				########################################################################
				# We only want to process new alarm events (if it is not an initial run
				########################################################################
				next LOGRECORD if ( (defined ($lastrecord) && ($logRec < $lastrecord))); # process if undef

				$minlogrec = $logRec if !defined ($minlogrec);
				$maxlogrec = $logRec if $logRec > $maxlogrec;  # maxprocessId can be set many times
				push @LogRecord, $logRec;
				
				my ($date,$time);
				while (<LOGFILE>) {	
					
					if (/<TimeStamp>/) {
						my ($year,$mon,$day,$hour,$min,$sec);

				        $srv->logDebug(6, "parsing TimeStamp : $_ ") if DEBUG6;

						TIMESTAMPS: while (<LOGFILE>) {
							if (/<year>\s+(\d+)\s+<\/year>\s+<month>\s+(\d+)\s+<\/month>\s+<day>\s+(\d+)\s+<\/day>/) {
								$year  = $1;
								$mon   = sprintf("%02d", $2);
								$day   = sprintf("%02d",$3);
							        $date     = "$year-$mon-$day";
								chomp $date;

							    $srv->logDebug(6, "date is: $date ") if DEBUG6;

								next TIMESTAMPS;
							} elsif (/<hour>\s+(\d+)\s+<\/hour>\s+<minute>\s+(\d+)\s+<\/minute>\s+<second>\s+(\d+)\s+<\/second>/) {
								$hour  = sprintf("%02d", $1);
								$min   = sprintf("%02d", $2);
								$sec   = sprintf("%02d", $3);
								$time  = "$hour:$min:$sec\n";
								chomp $time;

							    $srv->logDebug(6, "time is: $time ") if DEBUG6;


								next TIMESTAMPS;
							} elsif (/<\/TimeStamp>/) {
								
								    #initialise last time
									if (!defined ($startTime)) {
										$startTime = timegm_nocheck($sec,$min,$hour,$day,$mon-1,$year);
 								        $srv->logDebug(6, "start time is: $startTime with  $sec,$min,$hour,$day,$mon-1,$year ") if DEBUG6;
									} else {
										$srv->logDebug(6, "start time is: $startTime ") if DEBUG6;
										
									}
						
									# initialise last time
									$endTime = timegm_nocheck($sec,$min,$hour,$day,$mon-1,$year);


								    $srv->logDebug(6, "end time is: $endTime with  $sec,$min,$hour,$day,$mon-1,$year ") if DEBUG6;

									last;
									
									
							} else {
								  $srv->logDebug(6, "line ignored:  $_ ") if DEBUG6;
							}
						}
					} elsif  (/<Record/) {  # note <RecordContent>: abbreviated due to possibility of malformed xml file
					
						
						while(<LOGFILE>) {
							########################################################################
							# Note that sometimes the xml is not well formed, hence abbreviated here
							########################################################################
							#last if (/<\/Reco/);
							next LOGRECORD if (/<\/Rec/); # note </RecordContent>: abbreviated due to possibility of malformed xml file
							my @tmpArray = split /;/;
							my $alarmEvType        =  $tmpArray[1];
							my $extended           =  $tmpArray[2];
							($fdn,$moInstance)     =  ($tmpArray[4] =~ /^(SubNetwork=.*),(ManagedElement.*)$/);

							## If a FDN map file was specified, use it to override any already existing FDN. 
							## This was needed because of the way FDN mapfiles are used to override any FDN on the nodes at import.
							## TODO: Put this functionality into the final import method. 
							if( defined $self->{fdnmap} ){
								$fdn = $self->{fdnmap};
								$srv->logDebug(6, "fdn mapped is $fdn") if DEBUG6;
							}
							
							## If no FDN could be loaded from the node data, use the node name and hope that eventimport can import it.	
							if( !defined $fdn ) {
								#print "FDN could not be derived for node '$params{nodename}' with (fdn,mo) instance '$tmpArray[4]'\n";
								#next LOGRECORD; # Decision to not process any nodes not connected to OSS and hence have NO FDN : give indication in logfile							
								$fdn = $self->{nodename};								
							}
									
							my $notificationId     =  $tmpArray[5];
							my $severity           =  $tmpArray[9];
							my $problem            =  $tmpArray[10];
							#######################################################################################################################
							# We still parse the record to this point if the recId = $params{lastRecoredIdStored}. This is because for the case no
							# new records are seen, then we still need to parse at least one record so we can get the fdn for the integrity report
							#######################################################################################################################
						
							if ($logRec == $lastrecord) {
								$srv->logDebug(6, "-- lastrecord = $lastrecord ignore record because logrec $logRec is last record processed") if DEBUG6;
								next LOGRECORD; 							
							}
											
							if ($initialRun == 1) {
								my $dateDiff;
								eval {
									$dateDiff = dateDiff($date,$reportDate);
								};
								if ($@) {
									$srv->logInfo(6, "parseAlarmLog >> file $file has crazy dates, aborting record insert, '$@' ") if DEBUG6;
									next LOGRECORD;
								}
							
								if ($dateDiff <= 7) {
									$srv->logDebug(6, "ignore record $logRec because datediff ( <= 7)") if DEBUG6;								
									next LOGRECORD;
								} 
														
							}
							########################################################################################################
							# We are using the severity field for determining if the alarm is a new alarm or a clearing alarm event 
							# (clearing = 6, all others are new
							########################################################################################################
							if ($severity != 6) {
								my $issuingDatetime = "$date $time";
								
									$srv->logDebug(6, " Severity not equal 6 issuingDatetime : $issuingDatetime") if DEBUG6;	
								
								if (defined $alarmRecordsH{$moInstance}{$notificationId}) {
									########################################################################################
									# This will catch cases of wrap around in the log file definitions, hence the ceasing
									# alarm may be read by this utility before the the issuing alarm
									#########################################################################################
									if ( ${$alarmRecordsH{$moInstance}{$notificationId}}[5] == 6 ) {
																				 
										##################################################################################################
										# Here we know the alarm is already defined, and it is a cleaing alarm  hence we check that the 
										# timestamp of the issuing alarm is greater than the clearing case. This could happen for example 
										# if the logs are overwritten, and we have aleary issued a clear in the logs
										##################################################################################################
										my ($pre_yyyy, $prev_mon, $prev_dd, $prev_hh, $prev_mm, $prev_ss) = ( 
											${$alarmRecordsH{$moInstance}{$notificationId}}[7] =~ /(\d\d\d\d)-(\d\d)-(\d\d)\s+(\d\d):(\d\d):(\d\d)/);
										
										my ($cur_yyyy, $cur_mon, $cur_dd, $cur_hh, $cur_mm, $cur_ss) = (
											$issuingDatetime =~ /(\d\d\d\d)-(\d\d)-(\d\d)\s+(\d\d):(\d\d):(\d\d)/);

										if (timelocal($prev_ss,$prev_mm,$prev_hh,$prev_dd,$prev_mon-1,$pre_yyyy-1900) > timelocal($cur_ss,$cur_mm,$cur_hh,$cur_dd,$cur_mon-1,$cur_yyyy-1900) ) {
											# Valid case, previous > current timestamp, hence write to report
											${$alarmRecordsH{$moInstance}{$notificationId}}[0] = $issuingDatetime;
											push @alarmRecords, $alarmRecordsH{$moInstance}{$notificationId};
											$srv->logDebug(6, "writing record $logRec @{$alarmRecordsH{$moInstance}{$notificationId}}") if DEBUG6;											
											delete $alarmRecordsH{$moInstance}{$notificationId};
										} else {
											#########################################################################################################
											# We have a case were there was already a clearing alarm, and the issuing was newer, we cannot have an 
											# alarm being cleared before it is raised in timescale;
											#########################################################################################################
											@{$alarmRecordsH{$moInstance}{$notificationId}} = ($issuingDatetime,$fdn,$alarmEvType,$moInstance,$notificationId,$severity,$problem,undef);
											$srv->logDebug(6, "keeping record $logRec (Already cleared) @{$alarmRecordsH{$moInstance}{$notificationId}}") if DEBUG6;											
											next LOGRECORD;
										
										}
									}
									####################################################################################################################################
									# If we fall through to here it means that we have found our second (or more) raising alarm, hence we do nothing to preserve the 
									# timestamp of the initially raised alrm
									####################################################################################################################################
								} else {
									###########################################################################
									# Here this is the first instance of this alarm being seen (raising alarm)
									###########################################################################
									@{$alarmRecordsH{$moInstance}{$notificationId}} = ($issuingDatetime,$fdn,$alarmEvType,$moInstance,$notificationId,$severity,$problem,undef);
									$srv->logDebug(6, "keeping record $logRec (First Instance) $logRec @{$alarmRecordsH{$moInstance}{$notificationId}}") if DEBUG6;											
									
									next LOGRECORD;
								}
							} else {
								############################################################################################################################
								# It is a clearing alarm event. This means that there must either be an issuing alarm event already stored in alarmRecordsH 
								# hash, or the issuing alarm must have happened in the previous days report, so the EITS will attempt lookup, or the file 
								# has gone circular, meaning that as we read the file, a ceasing alarm may be seen before an issuing alarm.
								############################################################################################################################
								my $ceasingDateTime = "$date $time";
								
								$srv->logDebug(6, " Severity equal 6 ceasingDateTime: $ceasingDateTime") if DEBUG6;
								if (defined $alarmRecordsH{$moInstance}{$notificationId}) {
									${$alarmRecordsH{$moInstance}{$notificationId}}[7] = $ceasingDateTime;
									push @alarmRecords, $alarmRecordsH{$moInstance}{$notificationId};
									$srv->logDebug(6, "writing record $logRec @{$alarmRecordsH{$moInstance}{$notificationId}}") if DEBUG6;																				
									delete $alarmRecordsH{$moInstance}{$notificationId};
								} else {
									
									@{$alarmRecordsH{$moInstance}{$notificationId}} = (undef,$fdn,$alarmEvType,$moInstance,$notificationId,$severity,$problem,$ceasingDateTime);
									$srv->logDebug(6, "keeping  record $logRec @{$alarmRecordsH{$moInstance}{$notificationId}}") if DEBUG6;									
								}
								next LOGRECORD; 
							}
							
						}
					}  elsif (/<\/LogR/) { # note </LogRecord>: abbreviated due to possibility of malformed xml file
						# close logRecord while loop. Note should not trigger here under normal circumstances, as the above statement #next LOGRECORD if (/<\/Reco/);
						# ahould trigger before this is ever needed. Added just in case due to the malformed xml randomess
						$srv->logDebug(6, "finish process $logRec ") if DEBUG6;
						next LOGRECORD; 
					} 
				}  				
			}
			
	} # while loop - no more records to process from this alarm log file 
	close (LOGFILE);
	
	########################################################################################################################################
	# if there is any alarms with no issuing (or ceasing) timestamps, then dump them to the report Itk will have to attaempt to match these
	########################################################################################################################################
	foreach my $moInstance (keys %alarmRecordsH) {
		foreach my $notificationId ( keys %{$alarmRecordsH{$moInstance}}) {
			if (defined @{$alarmRecordsH{$moInstance}{$notificationId}} ) {
				push @alarmRecords, $alarmRecordsH{$moInstance}{$notificationId};
				$srv->logDebug(6, " Writing record (for each) --> @{$alarmRecordsH{$moInstance}{$notificationId}}") if DEBUG6;
			}
		} 
	}
    
	# build the alarm record and call the call back
	foreach my $record (@alarmRecords) {
		my $alarmRec = _getRecord ($self->{nodetype}, $self->{nodename}, $record);
        if (defined ($alarmRec) )  {
            &$callback($alarmRec);
        }
	}
	
	$srv->logDebug(4, "Result of parsing"." $file: lastrecno = $lastrecord minrec = $minlogrec maxrec = $maxlogrec") if DEBUG4;
	return OK, $maxlogrec, $startTime, $endTime;
}

####################################################################################
# dateDiff($date1, $date2)
# Return the number of days between two dates
# - $date1		Start Date
# - $date2		End Date
# RETURNS: Number of whole days between the dates.
####################################################################################
sub dateDiff {  
	my ($date1,$date2)=@_;    
	my ($y1,$m1,$d1)=($date1 =~ /^(\d\d\d\d)-?(\d\d)-?(\d\d)/);  
	my ($y2,$m2,$d2)=($date2 =~ /^(\d\d\d\d)-?(\d\d)-?(\d\d)/);    
	my $diff_t = timelocal(0,0,0,$d2,--$m2,$y2) - timelocal(0,0,0,$d1,--$m1,$y1);    
	return int($diff_t/86400); 
} 

##-----------------------------------------------------------------------------
# _getRecord ($time, $nodeName, $nodeType, $mo, $action, $attributeName, $attributeType, $value)
# Get a record instance with the values.
# - $time		Ref to the time
# - $nodeName 	Ref to the node name
# - $nodetype   Ref to the node type

# RETURNS: $resultcode
# - $rec		ref on the record
#------------------------------------------------------------------------------

sub _getRecord {
	my ($nodeType, $nodeName, $record) = @_;
		
	my $converTime = undef;	
    if (defined ($record->[0])) {
	   # convert time to seconds since the main module expect the 
	   my ($yy, $mm, $dd, $h, $m, $ss) = ($record->[0] =~ /(\d\d\d\d)-(\d\d)-(\d\d)\s+(\d\d):(\d\d):(\d\d)/);
	   $converTime = timegm_nocheck($ss,$m,$h,$dd,$mm-1,$yy);
    }
						
	my $rec = new Itk::EventData::EventRec::Alarm(
		nodetype => $nodeType,
		time => $converTime,
		nodename => $nodeName,
		alarmeventtype => $record->[2],
		moinstance  => $record->[3],           
		notificationid => $record->[4], 
       	severity => $record->[5],
        problemdesc => $record->[6],
        ceasedatetime => $record->[7]);
	return $rec;
}

##-----------------------------------------------------------------------------
# parseFiles($fileList, $callback)
# Parses one or more avlog files, returning event records to the callback sub as
# they are found. The last processed record is stored separately for each file
# - $fileList	    Ref to array containing local filenames of syslog files
# - $callback 	    Ref to callback sub
#
# RETURNS: $resultcode
# - $resultCode	  Result Code - see constants for definitions
# - $startTime    Starting time for the time window covered by this run (epoch secs)
# - $endTime      Ending time for the time window covered by this run (epoch secs)
#------------------------------------------------------------------------------
sub parseFiles {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $fileList, $callback)=@_;

	my (
		$startTime,	# Start of time window. Each run should provide a window of time for which we believe we have captured all events
		$endTime,	# End of time window
		$rcode,		# Result code
	);
	
	# last_record is a hash of file => last record#
	# In case the software is rolled back and then forward we might see cases where more than one of the
	# possible files has changes
	
	my $lastRecTable=$self->{collHist}->get('last_record');
			
	my $removeLastRecordKey = 0;
	
	# for each alarm file 
	foreach my $file (@{$fileList}) {
		
		my ($baseFileName) = ($file =~ /([^\/]+)$/);
		
		$srv->logDebug(4, "parsing file : $baseFileName ") if DEBUG4;
		
		my $lastrecord = $lastRecTable->{$baseFileName};

		$srv->logDebug(4, "$lastRecTable".Dumper ($lastrecord)) if DEBUG4;

		if (!defined ($lastrecord)) {
			$lastrecord = $self->{collHist}->get('itkFetchAlarmLastRun',"Itk::EventData::CollectionController");
			if (defined ($lastrecord)) {
				$self->{collHist}->rm ('itkFetchAlarmLastRun',"Itk::EventData::CollectionController");
				$self->{collHist}->commit();
			} 
	  		$lastrecord = 0 if (!defined $lastrecord);
		}	
	
		$srv->logDebug(4, "last Record is $lastrecord ") if DEBUG4;

		my ($parseRcode, $lastRecInFile, $fileStartTime, $fileEndTime) = $self->parse($file, 
			$callback,
			$lastrecord,
		);
		
		if (defined $lastRecInFile) {
			$lastRecTable->{$baseFileName} = $lastRecInFile;
		}

		$startTime = $fileStartTime unless (defined $startTime and $startTime < $fileStartTime);
		$endTime = $fileEndTime unless (defined $endTime and $endTime > $fileEndTime);
		
			
		if ($parseRcode != OK) {
			$self->_warn("Failed parsing file $file, rcode=$rcode");
			$rcode=PARSE_FAILED;
			next;
		}

		
		
		$srv->logDebug(4, "Result of file parsing: rcode=$rcode, starttime=$startTime, endtime=$endTime") if DEBUG4;
			
	}	
	
	$self->{collHist}->put('last_record', $lastRecTable);
	
	$rcode = OK unless defined $rcode;

	return $rcode, $startTime, $endTime;
}
##-----------------------------------------------------------------------------
# DESTROY()
# Destructor
#------------------------------------------------------------------------------
sub DESTROY {
	my $self=shift;
	# Do destruction!!
}



#------------------------------------------------------------------------------
1; # Modules always return 1!

