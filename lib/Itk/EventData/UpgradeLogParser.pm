#!/usr/bin/perl
###----------------------------------------------------------------------------
# Parser/Analyser for upgrade log files
#
# (c) Ericsson AB 2007 - All Rights Reserved
#
# No part of this material may be reproduced in any form
# without the written permission of the copyright owner.
# The contents are subject to revision without notice due 
# to continued progress in methodology, design and manufacturing. 
# Ericsson shall have no liability for any error or damage of any
# kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of
# Telefonaktiebolaget LM Ericsson. All other trademarks mentioned
# herein are the property of their respective owners. 
# 
#------------------------------------------------------------------------------
#
# Author: EHARMIC
# $Id: UpgradeLogParser.pm 5081 2011-08-18 12:23:47Z eanzmark $

package Itk::EventData::UpgradeLogParser;
use strict;
use Itk::EventData::EventRec;
use Digest::MD5 qw(md5_base64);
use Time::Local qw(timegm_nocheck);
use Hash::Util qw(lock_keys);

BEGIN {
	if (DEBUG5) {
		require Data::Dumper;
		import Data::Dumper;
	}
}
#==============================================================================
# CONSTANTS
#==============================================================================
use constant {
	# Result Codes
	OK => 1,			# Operation succeeded
	OK_MISSEDDATA => 2,		# Operation succeeded but there was some missing data
	PARSE_FAILED => -1,		# Parsing failed
	# Parser States
	IDLE => 0,
	INSTALLING => 10,
	UPGRADING => 20,
	# Result values for output records
	R_OK => 'OK',
	R_CANCEL => 'CANC',
	R_FAIL => 'FAIL',
	R_TIMEOUT => 'TOUT',
	R_UNKNOWN => 'UNKN',
	# Tuning constants
	BADSTATE_COUNTDOWN => 10,	# After seeing a bad state transition, the number of records we parse before giving up on the current action
};

#==============================================================================
# INSTANCE VARIABLES
#==============================================================================
use fields (
	'collHist',		# Ref to collection history object
	'nodename',
	'nodetype',
	'startdate',		# Timestamp (unix epoch) of start time filter
	'state',		# Parser state
	'nextstate',		# Aux parser state for next event after this one
);
#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $srv;
*srv=\$main::srv;
#==============================================================================
# CLASS METHODS
#==============================================================================

#==============================================================================
# PRIVATE METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# _warn($msg)
# Issue warning, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _warn {
	$srv->logWarn("($_[0]->{nodename}) $_[1]");
}

##-----------------------------------------------------------------------------
# _error($msg)
# Issue Error, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _error {
	$srv->logError("($_[0]->{nodename}) $_[1]");
}

#==============================================================================
# PUBLIC METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# new($collHist, %opts)
# Constructor
# - $collHist	Ref to instance of collection history
# % nodename	Name of node
# % nodetype	Type of node
# % startdate 	Starting date as a unix epoch timestamp; events earlier than this will be discarded
#------------------------------------------------------------------------------
sub new {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $collHist, %opts)=@_;
	$self=fields::new($self) unless ref $self;
	$self->{collHist}=$collHist;
	$self->{nodename}=$opts{nodename};
	$self->{nodetype}=$opts{nodetype};
	$self->{startdate}=$opts{startdate};
	return $self;
}

##-----------------------------------------------------------------------------
# parse($file, $callback, $lastrecord)
# Parses the indicated syslog file
# - $file	Filename of syslog file to parse
# - $callback	Call back sub to recieve results
# % lastrecord	Record ID of the last processed record. If this is not undef,
#		then we skip forward through the file until we find a matching
#		record; otherwise we just process all records in the file
# % avlogparser Instance of AvlogParser which is used to find downtimes and
#		running sw package at time of install or upgrade
# RETURNS: $resultcode, $lastrecord, $missingRecs, $startTime, $endTime, $state
# - $resultCode	 Result Code - see constants for definitions
# - $lastrecord  Record ID of the last processed record
# - $missingRecs Flag: if true, we could not find lastrecord so some records have been missed.
# - $startTime   Starting time for the time window covered by this parse (epoch secs)
# - $endTime     Ending time for the time window covered by this parse (epoch secs)
#------------------------------------------------------------------------------
sub parse {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $file, $callback, %opts)=@_;
	lock_keys(%opts, qw(lastrecord avlogparser));
	my $nodename=$self->{nodename};
	my $nodetype=$self->{nodetype};
	my $startDate=$self->{startdate};
	my $lastrecord = $opts{lastrecord};
	my $avlogp = $opts{avlogparser};
	
	my $fh;
	open $fh, $file or do {
		$self->_error("Failed to open '$file': $!");
		return PARSE_FAILED;
	};

	# Local sub to report an error, including line and file details.
	my $err = sub {
		$self->_error("File $file Line $.: $_[0]");
	};
	
	# Local sub to output a record
	my $outrec = sub {
		my ($s, $ts)=@_;

		my $rec;
		if ($s->{state}==INSTALLING) {
			my $fromstate = defined $avlogp ? $avlogp->getRunningUp($s->{starttime}) : undef;
			$rec = new Itk::EventData::EventRec::Install(
				time => $s->{starttime},
				nodename => $nodename,
				nodetype => $nodetype,
				action => $s->{action},
				result => $s->{result},
				info => $s->{info},
				addinfo => $s->{addinfo},
				fromstate => $fromstate,
				tostate => $s->{tostate},
				progresstotal => $s->{progresstotal},
				progresscount => $s->{progresscount},
				numfilesdownload => $s->{install_numloadmodules},
				ftpserver => $s->{install_ftpserver},
				installtime => $ts - $s->{starttime},
			);
		} elsif ($s->{state}==UPGRADING) {
			my ($numrestarts, $celloDt, $appDt, $fromstate);
			($numrestarts, $celloDt, $appDt, $fromstate) = $avlogp->calcDownTime($s->{starttime}, $ts);
			
			# If result was OK, do not keep the last header or step
			if ($s->{result} eq R_OK) {
				$s->{lastheader}=undef;
				$s->{upgrade_laststep}=undef;
			}
			
			# Calc upgrade times - from restartcollector:
			# "ConfirmTime" is the time where the upgrade was idle, waiting 
			# for the operator or OSS to confirm the upgrade. 
			# "UpgradeTime" is the total upgrade time minus "ConfirmTime".
			my $confirm_time = $s->{upgrade_confirmtime}-$s->{upgrade_finishtime};
			my $upgrade_time = ($ts - $s->{starttime}) - $confirm_time;
			
			$rec = new Itk::EventData::EventRec::Upgrade(
				time => $s->{starttime},
				nodename => $nodename,
				nodetype => $nodetype,
				action => $s->{action},
				result => $s->{result},
				info => $s->{info},
				addinfo => $s->{addinfo},
				fromstate => $fromstate, 
				tostate => $s->{tostate},
				numofrestart => $numrestarts,
				appdowntime =>  $appDt,
				cellodowntime => $celloDt,
				upgradetime =>  $upgrade_time,
				confirmtime =>  $confirm_time,
				lastpgheader =>  $s->{lastheader},
				laststepexec =>  $s->{upgrade_laststep},
			);
		} else {
			&$err("Outrec called from incorrect state: s=$s->{state}");
			return;
		}
		$srv->logDebug(5,"Record Output. Line #=$.\n".$rec->sprint()) if DEBUG5;
		
		&$callback($rec);

		%{$s} = ( state => IDLE ); # Reset parser state
	};

	my $missingrecs=0;	# Flag: any missing records?
	my (
		$skipping,	# Flag: Are we skipping forward to last known record?
		$startTime,	# Earliest record processed
		$endTime,	# Latest time processed
		$ts,		# Timestamp of current record
		$s,		# State of parser
		$next_s,	# Next state
		$recid		# Record ID of current record
	);
	
	$skipping=1 if defined $lastrecord;
	$s=$self->{state} || {
		state => IDLE,
	};
	$next_s=$self->{nextstate};
	if (DEBUG5) {
		$srv->logDebug(5,"Starting State:\n".Dumper($s,$next_s));
	}
	
RECORD:
	while (<$fh>) {
		chomp;
		
		my ($yy,$mo,$dd,$hh,$mm,$ss,$ms);
		if (($yy,$mo,$dd,$hh,$mm,$ss,$ms) = (/^Date:\t(\d{4})-(\d{2})-(\d{2}),\s+Time:\s+(\d+):(\d+):(\d+)\.(\d+)/)) {
			
			my (	$class,		# Class of currently record
				$method, 	# Method "   "       "
				@log,		# Lines of log message
				$error,		# Flag: Was this an error record
			);
			
			eval {
				$ts=timegm_nocheck($ss,$mm,$hh,$dd,$mo-1,$yy);
			};
			if ($@) {
				&$err("Invalid time: $yy-$mo-$dd $hh:$mm:$ss - record is discarded");
				next RECORD;
			}
			
			$_=<$fh>; chomp;
			unless (($class)=(/^Class:\t(.+)$/)) {
				&$err("Expected 'Class:', got: $_");
				next RECORD;
			}
			
			$_=<$fh>; chomp;
			unless (($method)=(/^Method:\s(.+)$/)) {
				&$err("Expected 'Method:', got: $_");
				next RECORD;
			}
			$_=<$fh>; chomp;

			# The file does not contain a natural record ID
			# We assume that the same class/method will not be called twice in the one timestamp
			$recid=md5_base64("$yy-$mo-$dd $hh:$mm:$ss.$ms $class $method");

			# Skip this record if before the start date filter
			if (defined $startDate and $startDate > $ts) {
				$srv->logDebug(6, "Discard record id=$recid ts=$ts because it is earlier than startdate") if DEBUG6;
				next RECORD;
			}
			
			# If we are skipping and the record number of this record is != the last 
			# processed record then we skip this record
			if ($skipping) {
				if ($recid ne $lastrecord) {
					next RECORD;
				} else {
					# If this record is the same as the lastprocessed then
					# we have found the point where we left off last time
					$srv->logDebug(5,"Found last processed record with id=$recid") if DEBUG5;
					$skipping=0;
					$startTime=$ts;
					next RECORD;
				}
			} else {
				$startTime=$ts unless defined $startTime;
			}
			
			unless ((/^(?:Log:|I N F O:|E R R O R:|W A R N I N G:)\s+(.+)$/)) {
				&$err("Expected 'Log:|I N F O:|E R R O R:|W A R N I N G:', got: $_");
				next RECORD;
			}
			push @log,$1;
			if (/^E R R O R/) {
				$error=1;
			} else {
				$error=0;
			}
			
			while (<$fh>) {
				chomp;
				last unless $_; # Log message terminates on an empty line
				s/^\t//; # Remove leading tab if any
				push @log, $_;
			}
			
			# Now we have the whole record!
			if (DEBUG6) {
				$srv->logDebug(6, "State=$s->{state} $file:$. Record:\trecid=$recid ts=$ts\n\tClass:\t$class\n\tMethod: $method\nLog:\t".join("\n\t",@log)."\n");
			}
			
			# If a countdown is in progress then we step down the countdown.
			# When it reaches zero then we output the current record. This is used to catch cases
			# were eg. we never see the message to indicate that an install or upgrade
			# was successful or not.
			if ($s->{state} != IDLE and $s->{countdown}) {
				unless (--$s->{countdown}) {
					$s->{result} ||= R_UNKNOWN; # Only sets to unknown if not previously set. If we saw an error then this would already be set
					&$outrec($s, $ts);
				}
			}

			# Unhandled exceptions come out as E R R O R in the log
			if ($error) {
				my $exception;
				if ($log[0] =~ /Exception caught/) {
					foreach (reverse @log) {
						if (/The exception data is.+TAG :"([^"]+)"/) {
							$exception="Exception: $class $method $1";
							last;
						}
					}
				} else {
					$exception=$log[0];
					foreach (reverse @log) {
						if (/Exception info:\s*(.+)$/) {
							$s->{addinfo}=$1;
						}
					}
				}
				$s->{info}=$exception || "Unknown Error Occurred";
				$s->{result}=R_FAIL; # We pre-set this here, in case we never find the end of this action
						     # In that case we would have to assume it failed
				next;
			}
			
			if ($class eq 'UpgradePackageMoImpl') {
				if ($method =~ /^setProgressCountValue/) {
					($s->{progresscount})=($log[0]=~/(?:progressCount|ProgressCounter):\s+(\d+)/) or do {
						&$err("setProgressCountValue: could not find ProgressCounter: $log[0]");
					};
					next RECORD;
					
				} elsif ($method =~ /^setProgressHeaderValue/) {
					
					my ($progressheader) = ($log[0]=~/progressHeader: (.+)$/);

					if ($s->{state} == INSTALLING) {
						
						# Sometimes and install ends and no other indication is given
						# We try to detect this by seeing that the progress header
						# sequence is not what we expect
						
						# BUT: we ignore header 'Execution Failed' because we would expect that
						# in a normal failure case - it is taken care of by setActionResult parsing
											
						if ( $progressheader ne 'Execution failed' and
							($s->{lastheader} eq 'Downloading files' and $progressheader ne 'Saving configuration') or
							($s->{lastheader} eq 'Saving configuration' and $progressheader ne 'Idle') ) {
						
							# Normal state transition has not occurred. In some cases we still might
							# get exception info, etc so rather than stop now, we set a countdown timer. If we 
							# have not seen the end of the process by the time this counter reaches 0, then we abort
							# and write the record (see bottom of loop for that).
							$s->{countdown}=BADSTATE_COUNTDOWN;
						}
					} elsif ($s->{state} == UPGRADING) {
						# This is the signal that the upgrade was completed OK
						# Log:	Sending AVC Event - progressHeader: Upgrade successfully executed
						if ($progressheader =~ /Upgrade success/) {
							$s->{result}=R_OK;
							&$outrec($s, $ts);
						
						# This is the signal that the upgrade failed
						# Log:	Sending AVC Event - progressHeader: Execution of upgrade failed
						} elsif ($progressheader =~ /upgrade failed/) {
							$s->{result}=R_FAIL;
							&$outrec($s, $ts);
						}
					}
					$s->{lastheader} = $s->{progressheader};
					$s->{progressheader} = $progressheader;
					next RECORD;
					
				} elsif ($method =~ /^setProgressTotalValue/) {
					($s->{progresstotal})=($log[0]=~/ProgressTotal:\s+(\d+)/i) or do {
						&$err("setProgressTotalValue: could not find ProgressTotal: $log[0]");
					};
					next RECORD;
				
				#----------------------Enter state awaiting confirm  --------------------------------- 
				# Date:	2007-11-19, Time: 13:39:12.810,  SU_SW: 5.1 LSV21 - R51EL48
				# Class:	UpgradePackageMoImpl
				# Method: changeToState ( int aToState, boolean aSendNotification )
				# Log:	Sending AVC Event - state: Awaiting confirm
				} elsif ($method =~ /changeToState/ and $log[0] =~ /Awaiting confirm/) {
					$s->{upgrade_finishtime}=$ts;
					next RECORD;
				
				#----------------------Start of an Upgrade or Install---------------------------------
				#Date:	2007-09-14, Time: 3:43:05.862,  SU_SW: 5.1 LSV21 - R51JJ01
				#Class:	UpgradePackageMoImpl
				#Method: actionNonBlockingInstall ( Coordinator aCoordinator )
				#Log:	Start Action - Non Blocking Install
				#	Product number: CXP9011610/1
				#	Product revision: R11AF/5
				#
				#Date:	2007-11-21, Time: 18:19:00.715,  SU_SW: 5.1 LSV21 - R51EL39
				#Class:	UpgradePackageMoImpl
				#Method: actionRebootNodeUpgrade ( Coordinator c )
				#Log:	Start Action - Reboot Node Upgrade
				#	Product number: CXP9011610/1
				#	Product revision: R12C/3
				#
				#Date:   2008-06-24, Time: 5:13:37.282,  SU_SW: 5.1 LSV21 - R51SA01
				#Class:  UpgradePackageMoImpl
				#Method: actionInstall ( Coordinator aCoordinator )
				#Log:    Start Action - Install
				#        Product number: CXP9011610/1
				#        Product revision: R12G/1
				
				} elsif ($method =~ /^action((?!Cancel|Verify|Confirm)\w*(?:Install|Upgrade))/) {
					my $actionType = $1;
					if ($log[0] =~ /^Start Action/) {
						my ($prod,$rev);
						($prod) = ($log[1] =~ /Product number:\s+(\S+)/) or do {
							&$err("Could not find Product Number");
							$prod="Unknown";
						};
						
						($rev) = ($log[2] =~ /Product revision:\s+(\S+)/) or do {
							&$err("Could not find Revision Number");
							$rev="???";
						};
						
						# We do not immediately set the state, because it might be that
						# the action is rejected - for example if the package is already installed
						# or if another action is already ongoing. Instead we get the next state
						# ready in $next_s and switch it in when we see that initiation was successful
						
						if ($actionType =~ /Install$/) {
							$next_s = {
								tostate => "${prod}_$rev",
								starttime => $ts,
								install_numloadmodules => 0,
								state => INSTALLING,
								install_ftpserver => $s->{install_ftpserver},
								action => $actionType
							};
							if ($method =~ /^actionInstall/) { # Blocking operation: "End Action" comes at the end 
								$s = $next_s;
								$next_s = undef;
							}
						} else {
							$next_s = {
								tostate => "${prod}_$rev",
								starttime => $ts,
								state => UPGRADING,
								action => $actionType
							};
						}
						
						next RECORD;
					
					# ------------------------ Asynch Action Initiated ----------------------- 
					# Date:	2007-09-14, Time: 3:43:06.962,  SU_SW: 5.1 LSV21 - R51JJ01
					# Class:	UpgradePackageMoImpl
					# Method: actionNonBlockingInstall ( Coordinator aCoordinator )
					# Log:	End Action - Non Blocking Install, install initiated (Action ID: 34306912)
					} elsif (defined $next_s and $log[0] =~ /^End Action -.+initiated \(Action ID: (\d+)\)/) {
						if ($s->{state} != IDLE) {
							$srv->logDebug(4,"$actionType ordered when action already ongoing") if DEBUG4;
							# Must have been a failed case where we did not see the end
							# (despite all counter measures!)
							$s->{result}=R_FAIL;
							&$outrec($s, $ts);
						}
						$s = $next_s;
						$next_s = undef;
						$s->{actionid}=$1;
						next RECORD;
						
					}
				
					
				} elsif ($method =~ /^actionCancelInstall/ and $log[0] =~ /Start Action/) {
					if ($s->{state} != INSTALLING) {
						&$err("Install Cancel Action without install ongoing: state=$s->{state}");
						next RECORD;
					}
					$s->{result}=R_CANCEL;
					&$outrec($s, $ts);
					next RECORD;

				} elsif ($method =~ /^actionConfirmUpgrade/ and $log[0] =~ /Start Action/) {
					if ($s->{state} != UPGRADING) {
						&$err("UPgrade Confirm action without upgrade ongoing: state=$s->{state}");
						next RECORD;
					}
					$s->{upgrade_confirmtime}=$ts;
					next RECORD;
					
				# ------------------------ Install Completed -----------------------
				# Date:	2007-08-06, Time: 7:27:12.671,  SU_SW: 5.1 LSV21 - R51JJ01
				# Class:	UpgradePackageMoImpl
				# Method: installResponse(InstallRejectedEvent anInstallRejectedEvent)
				# Log:	Install failed.
					
				} elsif ($method =~ /^installResponse/) {
					if ($s->{state} != INSTALLING) {
						&$err("installResponse without install ongoing: state=$s->{state}");
						next RECORD;
					}
					if ($log[0] =~ /success/) {
						$s->{result}=R_OK;
					} elsif ($log[0] =~ /failed/) {
						$s->{result}=R_FAIL;
					}
					&$outrec($s, $ts);
					next RECORD;
					
				}
				
			# Count the number of MOs that are installed during installation
			} elsif ($class eq 'InstallCoordinator' and $method =~ /^loadModuleInstalled/) {
				if ($s->{state} != INSTALLING) {
					&$err("Load Module Install without install ongoing: state=$s->{state}");
					next RECORD;
				}
				$s->{install_numloadmodules}++;
				next RECORD;

			# Keep the upgrade step labels
			} elsif ($class eq 'UpgradeController' and $method =~ /^doExecuteSequence/ and $log[0] =~ /Execution of step label '([^']+)' is starting/) {
				if ($s->{state} != UPGRADING) {
					&$err("UpgradeController::doExecuteSequence without install ongoing: state=$s->{state}");
					next RECORD;
				}
				$s->{upgrade_laststep}=$1;
				next RECORD;
				
			# Stash the address of the FTP server being used. This is present when the Upgrade MO is completed rather than when install is ordered
			# WARNING: May not be accurate if the MO is created, then another MO is created, then install ordered on the original one
			} elsif ($class eq 'UpgradePackageCreator' and $method =~ /^getUpgradeControlFileFromFTP_Server/) {
				my $t;
				foreach (@log) {
					$t=$1 and last if (/IP address of FTP Server: (\d+\.\d+\.\d+\.\d+)/);
				}
				unless ($t) {
					&$err("IP Address not found in getUpgradeControlFileFromFTP_Server method");
					next RECORD;
				}
				$s->{install_ftpserver}=$t;
				next RECORD;

			# Detect SW level from created CV. Only needed if the action to
			# start the upgrade was missing in log. Bug#1103
			# Date:	  2008-10-26, Time: 14:21:49.211,  SU_SW: 6.0 LSV14 - R60MN02
			# Class:  ConfigurationVersionMO_Handler
			# Method: createCV_AndSetStartableCont(String aReason, String aProductNumber, String aProductRevision, int aType, String aComment)
			# Log:	  CV 'SU_CXP9012123_R10H%15_081026_1421' has been created.
			} elsif ($class eq 'ConfigurationVersionMO_Handler' and $method =~ /^createCV/) {
				if ($s->{state} == UPGRADING && (! defined $s->{tostate}) && ($log[0] =~/^CV\s+'SU_([^_]+_[^_]+)_/)) {
						$s->{tostate}=$1;
						$s->{tostate} =~ s/%/\//;
				}
				next RECORD;
#				$s->{install_fromstate}=$1 if ($log[0] =~ /^CV\s+'Si_([^_]+_[^_]+)_\d+_\d+' has been created/);
#				# FIXME: Determine fromstate from AvLog
#				next RECORD;
				
			# Errors, exceptions etc come out in logs for the setActionResult method
			# Typically there are 2 or 3 results. The first is a high level error and the second seems to contain the most
			# relevant data. We keep the last one and insert it into the output record because if an action fails
			# then it is most likely that the most recent action result gives the reason why
			#
			# Date:	2007-08-06, Time: 7:27:12.365,  SU_SW: 5.1 LSV21 - R51JJ01
			# Class:	ActionResultHandler
			# Method: setActionResult ( InstallRejectedReasonEvent anInstallRejectedReasonEvent, InstallClientInterface anInstallClientInterface, ActionResultDataTypeOfInvokedActionValue anActionResultDataTypeOfInvokedActionValue )
			# I N F O: WARNING - Result 2 of invoked action:
			# 	
			# 	Time Stamp: Date: 2007-08-06, Time: 7:27:12.011
			# 	Action Id: 72648179
			# 	Type Of Invoked Action: Install
			# 	Information: The required disk space for load modules to be installed is insufficient
			# 	Additional Information: There is NOT enough space on disk for download on /c
			} elsif ($class eq 'ActionResultHandler' and $method =~ /^setActionResult/ && $log[0] =~ /WARNING - Result 2 of invoked action/) {
				
				my ($actionid) = ($log[3]=~/Action Id: (\d+)$/);
				my ($atype) = ($log[4]=~/Type Of Invoked Action: (.+)$/);
				my ($info) = ($log[5]=~/Information: (.+)$/);
				my ($addinfo) = ($log[6]=~/Additional Information: (.+)$/);
				
				next RECORD if ($s->{state} == IDLE or $actionid != $s->{actionid});
				
				if ($s->{state} == INSTALLING and $atype ne 'Install') {
					&$err("Action result for type of '$atype' while state is INSTALLING");
					next RECORD;
				}
				
				$s->{info}=$info;
				$s->{addinfo}=$addinfo;
				next RECORD;
			} elsif  ($class eq 'UpgradeSupervisionTimerHandler' && $log[0] =~ /^The upgrade supervision timer is set/ && $s->{state}!=UPGRADING) {
				# Bug#1103. JVM restart has resulted in some log messages going missing.
				# See also http://clearddts.rnd.ki.sw.ericsson.se/ddts/ddts_main?id=WRNae36786
				# We have to assume that an upgrade has been requested.
				if ($s->{state} != IDLE) {
					$srv->logDebug(4, "Detected upgrade started while state not idle (state=$s->{state}) - assuming WRNae36786 and upgrade has started");
					&$outrec($s, $ts);
				};
				
				$s = {
					tostate => undef,
					starttime => $ts,
					state => UPGRADING,
					action => 'upgrade',
				};
				next RECORD;
			}


		}
	}

	# Got to the end of the file. If $skipping is true that means we did not
	# find the last processed record; so we must have missed some data.
	if ($skipping) {
		$self->_warn("Could not find current record. This means we have missed some data.");
		$skipping=0;
		$missingrecs=1;
		seek($fh, 0,0); # Rewind file back to beginning
		goto RECORD; # start again, this time reading all records
	}
	
	$endTime=$ts;
	
	close $fh;
	
	# Save state of parser so next file can pick it up
	$self->{state}=$s;
	$self->{nextstate}=$next_s;
	
	return (
		OK,		# Result Code - see constants for definitions
		$recid,         # Record ID of the last processed record
		$missingrecs,   # Flag: if true, we could not find lastrecord so some records have been missed.
		$startTime,     # Starting time for the time window covered by this parse (epoch secs)
		$endTime        # Ending time for the time window covered by this parse (epoch secs)
		);
}

##-----------------------------------------------------------------------------
# parseFiles($fileList, $callback)
# Parses a batch of upgrade log files, returning event records to the callback sub as
# they are found.
# - $fileList	Ref to array containing local filenames of log files
# - $callback 	Ref to callback sub
# % avlogparser Instance of AvlogParser which is used to find downtimes and
#		running sw package at time of install or upgrade
# RETURNS: $resultcode, $startTime, $endTime
# - $resultCode	  Result Code - see constants for definitions
# - $startTime    Starting time for the time window covered by this run (epoch secs)
# - $endTime      Ending time for the time window covered by this run (epoch secs)
#------------------------------------------------------------------------------
sub parseFiles {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $fileList, $callback, %opts)=@_;
	lock_keys(%opts, qw(avlogparser));
	
	my (
		$startTime,	# Start of time window. Each run should provide a window of time for which we believe we have captured all events
		$endTime,	# End of time window
		$rcode,		# Result code
		@sortedfilelist,# List of files in processing order
		$newLastRecord, # Tracks the max record number processed through all files
	);
	
	# Persistent history. Last file is the last file we read from on this node
	# lastrecord is the last record in that file. When we start up we expect to find the last read
	# record in this file, otherwise we have missed data
	
	my $lastrecord=$self->{collHist}->get('last_record');
	
	# We also save the parser state between runs. This is to cater for the case
	# where we are parsing the file and it ends in the middle of an event
	# The next time we should pick up the rest of the event
	$self->{state}=$self->{collHist}->get('state');
	$self->{nextstate}=$self->{collHist}->get('nextstate');

	# We should either have a single file (Trace.log) or 2 files (Trace.log and Trace.log_old)
	# in the latter case it means they have rotated and we should process both, with _old first
	foreach (@{$fileList}) {
		if (/Trace\.log_old$/) {
			unshift @sortedfilelist, $_; # Put _old files on the front of the list
		} elsif (/Trace\.log$/) {
			push @sortedfilelist, $_; # Put newer files on the end of the list
		}
	}

	# Process files in order
	foreach my $file (@sortedfilelist) {

		$srv->logDebug(4, "Parsing file: $file") if DEBUG4;
		
		my ($parseRcode, $lastRecInFile, $missedRec, $fileStartTime, $fileEndTime) = $self->parse($file,$callback, lastrecord => $lastrecord, %opts);
		
		if (defined $lastRecInFile) {
			undef $lastrecord;
			$newLastRecord=$lastRecInFile;
		}
		
		$startTime = $fileStartTime unless (defined $startTime and $startTime < $fileStartTime);
		$endTime = $fileEndTime unless (defined $endTime and $endTime > $fileEndTime);
		
		if ($parseRcode != OK) {
			$self->_warn("Failed parsing file $file, rcode=$rcode");
			$rcode = PARSE_FAILED;
			next;
		}
		
		$rcode = OK_MISSEDDATA if (! defined $rcode and $missedRec);
		
	}
	
	# At the end of this, $lastRec contains the id of last record read
	# Only save this value if we did process some files!
	if (@sortedfilelist && defined $newLastRecord) {
		$self->{collHist}->put('last_record', $newLastRecord);
		$self->{collHist}->put('state',$self->{state});
		$self->{collHist}->put('nextstate',$self->{nextstate});
	}
	
	$rcode = OK unless defined $rcode;
	
	return $rcode, $startTime, $endTime;
}

##-----------------------------------------------------------------------------
# DESTROY()
# Destructor
#------------------------------------------------------------------------------
sub DESTROY {
	my $self=shift;
	# Do destruction!!
}
#------------------------------------------------------------------------------
1; # Modules always return 1!

